@extends('layouts.mail')

@section('content')
<div class="container">
    <div class="card p-5">
    <div class="card-header">
    <img src="{{ url('photo/material/logo.png')}}" style="max-width: 20%; margin-left: 40%;" class="logo-img"> 
    </div>

    <div class="card-body">

    
    <h3 class="text-center">Your Email Verification  :
    
            @if ($msg != null)

        
             {{ $msg }}



            @endif
        </h3>

    <p class="text-center mt-5">
    <a href="{{url('/login')}}"><button class="btn btn-primary">Login Here</button></a>
    </p>


    </div>
</div>
@endsection
