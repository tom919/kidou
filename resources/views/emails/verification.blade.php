@extends('layouts.mail')

@section('content')
<div class="container">
    <div class="card p-5">
        <div class="card-header">
        <img src="{{ url('photo/material/logo.png')}}" style="max-width: 20%; margin-left: 40%;" class="logo-img"> 
        </div>

        <div class="card-body">
            <p>
                Hi, {{ $name }}
            </p>

            <p>
                Please verify your email : <a href="{{ url('verifyemail', $token)}}" target="_blank"><button class="btn btn-primary">click here</button></a>
            </p>

            <p>
            Regrads,



            Kidou
            </p>

        </div>
    </div>
</div>
@endsection
