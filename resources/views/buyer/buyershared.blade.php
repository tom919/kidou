@extends('layouts.sharedbuyer')

@section('content')
<div class="section-hero-copy">
<img src="{{url('photo/company')}}/{{$co->photo_slide}}" class="slide-photo">
  </div>
  <div class="section-hero-content-copy">
    <div class="container-7 w-container">
      <div class="w-layout-grid grid-wrap-hero-content-copy">
        <div class="wrap-content-hero-left buyers">
          <div class="share-save-copy _60-copy">
            <ul role="list" class="nav-grid-2-wrap-copy w-list-unstyled">
              <li class="list-item-6-copy">
                <a href="#" class="nav-logo-link-copy"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60731b10c68543ed70a43511_Kidou_Black%201.png" alt="" class="nav-logo-copy"></a>
              </li>
              <li id="w-node-ba49b9ba-489b-64f4-7e62-90ddfe05df59-53806dc6" class="list-item-4-copy">
                <a data-w-id="ba49b9ba-489b-64f4-7e62-90ddfe05df5a" href="#" class="l003-button-copy w-inline-block">
                  <div data-w-id="ba49b9ba-489b-64f4-7e62-90ddfe05df5b" data-animation-type="lottie" data-src="{{url('/maikai')}}/documents/1087-heart.json" data-loop="0" data-direction="1" data-autoplay="0" data-is-ix2-target="1" data-renderer="svg" data-default-duration="3.1" data-duration="0" class="lottie-heart"></div>
                </a>
              </li>
              <li class="list-item-5">
                <a href="{{url('/login')}}" class="nav-link-2">Sign In</a>
              </li>
              <li class="list-item-3">
                <a href="#" class="nav-link-2" data-toggle="modal" data-target="#regModal">Register</a>
              </li>
            </ul>
          </div>
          <h1 class="heading-section-2">{{$ud->first_name}}&nbsp;{{$ud->last_name}}</h1>
         <h3>{{$cown->owner_position}} Of {{$co->company_name}}</h3>
          <div class="w-layout-grid grid-bar-resto-entertainment">
            <div class="text-block-4">{{$cown->owner_position}}r</div>
            <ul role="list" class="list"></ul>
            <ul role="list" class="list"></ul>
          </div>
          <div class="divider full"></div>
          <div class="w-layout-grid grid-bottons-small phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607832eddebae43c801da3ec_icon%20location.png" loading="lazy" width="34" alt="">
            <p id="w-node-ab44bd9a-3252-ee1e-9425-7c9c62ba7eff-53806dc6" class="para-body-2 lighter">{{$co->address}} , {{$co->city}}, {{$co->country}}</p>
          </div>
          <div class="w-layout-grid grid-bottons-small phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607832e33f6827b57ff7fe21_industry%20icon.png" loading="lazy" width="34" alt="">
            <p class="para-body-2 lighter">Industry</p>
            <?php
                $ind = explode(',', $co->industry);
                ?>
            @foreach($ind as $in)
            @if($loop->index < 3 )
            <a href="#" class="button-3 blue w-button">{{$in}}</a>
            @endif
            @endforeach
          </div>
          <div class="w-layout-grid grid-bottons-small phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607d06497c6777c26de4f91b_criteria%20icon.png" loading="lazy" width="34" alt="">
            <p class="para-body-2 lighter">Criteria</p>
            <?php
                $crt = explode(',', $co->b_criteria);
                ?>
            @foreach($crt as $cr)
            @if($loop->index < 3 )
            <a href="#" class="button-3 blue w-button">{{$cr}}</a>
            @endif
            @endforeach
          </div>
          <div class="w-layout-grid grid-big-botton-hero-left-copy">
            <a href="#" class="button-bigger blue w-button">Request a Meeting</a>
          </div>
        </div>
        <div class="wrap-content-hero-right buyer">
          <div class="share-save _60">
            <ul role="list" class="nav-grid-2-wrap w-list-unstyled">
              <li class="list-item-6-copy">
                <a href="#" class="nav-logo-link-copy"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60731b10c68543ed70a43511_Kidou_Black%201.png" alt="" class="nav-logo-copy"></a>
              </li>
              <li id="w-node-cc78e3ed-f2d4-662d-0b39-1233b7ddcb32-53806dc6" class="list-item-4">
                <a data-w-id="cc78e3ed-f2d4-662d-0b39-1233b7ddcb33" href="#" class="l003-button w-inline-block">
                  <div data-w-id="cc78e3ed-f2d4-662d-0b39-1233b7ddcb34" data-animation-type="lottie" data-src="{{url('/maikai')}}/documents/1087-heart.json" data-loop="0" data-direction="1" data-autoplay="0" data-is-ix2-target="1" data-renderer="svg" data-default-duration="3.1" data-duration="0" class="lottie-heart"></div>
                </a>
              </li>
              <li id="w-node-cc78e3ed-f2d4-662d-0b39-1233b7ddcb35-53806dc6" class="list-item-5">
                <a href="{{url('/login')}}" class="nav-link-2">Sign In</a>
              </li>
              <li id="w-node-cc78e3ed-f2d4-662d-0b39-1233b7ddcb38-53806dc6" class="list-item-3">
                <a href="#" class="nav-link-2" data-toggle="modal" data-target="#regModal">Register</a>
              </li>
            </ul>
          </div>
          <div class="wrap-image-content-hero">
          <img src="{{url('photo/company')}}/{{$co->photo_main}}" loading="lazy" width="595" alt="" class="image-15">
          </div>
          <div class="columns-2 w-row">
            <div class="column-11 w-col w-col-5">
              <h5 class="heading-section-2 center">
              <?php 
                    $f = new NumberFormatter("en", NumberFormatter::CURRENCY);
                    $inv_size_start = number_format($co->b_investment_size_start, 0);
                    $inv_size_end = number_format($co->b_investment_size_end, 0);
            ?>
          $ {{$inv_size_start}} - {{$inv_size_end}}
              </h5>
              <p class="para-body-2 center">Investment Size</p>
            </div>
            <div class="column-12 w-col w-col-7">
              <a href="#" class="button-bigger buyer smaller w-button">Send Proposal</a>
              <a href="#" class="button-bigger buyer phone-copy w-button">Send Proposal</a>
            </div>
          </div>
          <div class="w-layout-grid grid-33">
            <div id="w-node-ce1c5747-d6bd-a38b-5f7a-2d3a6166717a-53806dc6">
              <h2 class="heading-section-4 center" style="font-weight: bold;"> $ {{$inv_size_start}} - {{$inv_size_end}}</h2>
              <p class="para-body-2 center">Investment Size</p>
            </div>
            <div id="w-node-_64c9ef13-f9ed-01dc-09d1-0c3ed9c0dabd-53806dc6" class="div-block-33">
              <a href="#" class="button-bigger buyer smaller w-button">Send Proposal</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section-body-content-copy">
    <div class="w-container">
      <div class="w-layout-grid grid-7-copy">
        <div class="wrap-profile-img-copy">
          <img src="{{url('photo/user')}}/{{$ud->photo}}" loading="lazy" sizes="(max-width: 479px) 92vw, 100vw" alt="" class="cust-pp">
          </div>
        <div id="w-node-_66706f93-7d26-e661-98cd-15d8c46b957a-53806dc6" class="wrap-para-body-bigger">
          <p class="para-body-bigger">
          {!! \Illuminate\Support\Str::limit($co->intro, 255, $end='...')  !!}
         </p>
        </div>
      </div>
    </div>
  </div>
  <div class="section-company">
    <div class="w-container">
      <div class="w-row">
        <div class="coumn-about w-col w-col-8">
          <div class="wrap-content-copy">
            <h2 class="heading-section-2 about">Company</h2>
            <p class="para-body-2 left">
            {!! \Illuminate\Support\Str::limit($co->about, 255, $end='...') !!}
            </p>
            <div class="wrap-accordian-item">
              <div data-hover="" data-delay="0" class="dropdown-trigger w-dropdown">
                <div class="dropdown-toggle-4 w-dropdown-toggle">
                  <div class="accordian-icon w-icon-dropdown-toggle"></div>
                  <div>Show More </div>
                </div>
                <nav class="dropdown-lists-buyer w-dropdown-list">
                  <p class="para-body-2">
                  <?php
                    $about_rest= substr($co->about,  -(( strlen($co->about)-1) - 100));
                  ?>
                  {!! $about_rest !!}</p>
                 </p>
                </nav>
              </div>
            </div>
            <div class="divider-copy"></div>
            <div class="w-layout-grid grid-para-header"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607678da0a294bf99072e2e9_expertise%20icon.png" loading="lazy" alt="">
              <h3 class="para-header">Areas of Expertise</h3>
            </div>
            <div class="w-layout-grid grid-bottons-small">
            <?php
                $exp = explode(',', $co->b_expertise);
                ?>
              @foreach($exp as $ex)
              @if($loop->index < 3 )
              <a href="#" class="button-3 blue w-button">{{ $ex }}</a>
              @endif
              @endforeach
            </div>

            <div class="w-layout-grid grid-para-header"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607678ddcd23936085515f01_founding%20icon.png" loading="lazy" alt="">
              <h3 class="para-header">Founding Source</h3>
            </div>
            <div class="w-layout-grid grid-bottons-small">
            <?php
                $bfs = explode(',', $co->b_fund_source);
                ?>
              @foreach($bfs as $bf)
              @if($loop->index < 3 )
              <a href="#" class="button-3 blue-darker w-button">{{ $bf }}</a>
              @endif
              @endforeach
            </div>
            <div class="divider-copy"></div>
            <h2 class="heading-section-2">Transaction</h2>
            <div class="wrap-sub-heading-company-facts">
              <p class="para-bold-22">Preferred Transaction Details</p>
            </div>
            <div class="wrap-table buyers">
            <?php 
              $pftLength = count($pft);
          ?>
          @foreach($pft as $pf)
            
              <div class="w-layout-grid grid-table buyers">
                <div>
                  <p class="para-body-2">{{$pf->title}}<br></p>
                  <p class="para-light-gray">{{$pf->sub_title}}</p>
                </div>
                <a id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a3e7-53806dc6" href="#" class="w-inline-block"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/6076765738c1fa25bd4eb57a_mini%20checbox.png" loading="lazy" alt=""></a>
                <p id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a3e9-53806dc6" class="para-body-2 table right">{{$pf->note}}</p>
              </div>

              <div class="divider-table"></div>
          @endforeach
              
            </div>
          </div>
          <div class="divider-copy _60"></div>
        </div>
        <div class="column-2 w-col w-col-4">
          <div class="wrap-owners-box-copy buyer">
            <div class="wrap-img-box-right-copy">
              <a href="#" class="link-block-3 w-inline-block">
              <img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607856167d1f210851261a5b_heart%20icon.png" loading="lazy" width="35" alt="" class="image-11"></a>
              <img src="{{url('photo/company')}}/{{$co->photo}}" loading="lazy" height="200" alt="" class="image-10">
              <h3 class="para-header white">{{$co->company_name}}</h3>
            </div>
            <div class="wrap-content-right-box-copy">
              <div class="w-layout-grid grid-bottons-small right-box phone">
                <h4 class="heading-36px">
                <?php 
                  echo floor((time() - strtotime($co->establish_date)) / 31556926);
                ?>
                </h4>
                <p class="para-body-2 white">Years Operational</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box phone">

                @if($co->b_mna_exp == 'Yes')
                <i class="material-icons" style="color: #ffff;">check</i>
                @else 
                <i class="material-icons" style="color: #ffff;">clear</i>
                @endif
                <p class="para-body-2 white">M&amp;A Experience</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/606d345c43ccdd77c5203dcf_Vector%20(5).png" loading="lazy" width="25" alt="">
                <p class="para-bold-22">Industry</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box padd-20">
                <?php
                  $ind = explode(',', $co->industry);
                ?>
                @foreach($ind as $in)
                @if($loop->index < 3 )
                  <a href="#" class="button-3 blue w-button">{{$in}}</a>
                  @endif
                @endforeach
              </div>
              <div class="w-layout-grid grid-bottons-small right-box"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607d004e40edff0df0a9ee43_criteria%20icon.png" loading="lazy" width="41" alt="">
                <p class="para-bold-22">Criteria</p>
              </div>
              <?php
                $crt = explode(',', $co->b_criteria);
                ?>
              <div class="w-layout-grid grid-bottons-small right-box padd-20">
              @foreach($crt as $cr)
              @if($loop->index < 3 )  
              <a href="#" class="button-3 blue w-button">{{$cr}}</a>
              @endif
              @endforeach  
            </div>
              <div class="w-layout-grid grid-bottons-small right-box"></div>
            </div>
            <div class="divider-copy buyers"></div>
            <div class="div-block-2-copy">
              <div class="w-layout-grid grid-big-botton-hero-right inside--box-right">
                <h3 class="heading-section-4 inside-box buyers" style="color: #ffff;">
                <?php 
                    $f = new NumberFormatter("en", NumberFormatter::CURRENCY);
                    $inv_size_start = number_format($co->b_investment_size_start, 0);
                    $inv_size_end = number_format($co->b_investment_size_end, 0);
            ?>
          $ {{$inv_size_start}} - {{$inv_size_end}}
                </h3>
                <p class="para-body-2 white invest-size">Investment Size</p>
                <a href="#" class="button-bigger place-an-offer w-button">Send Proposal</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="w-container">
      <div class="columns-6 w-row">
        <div class="column-18 w-col w-col-8">
          <div class="w-layout-grid grid-big-botton-hero-right owner-profile">
            <h1 class="heading-section-2">Buyer Profile</h1>
            <a id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a421-53806dc6" href="#" class="button-bigger contact-owner w-button">Contact Owner</a>
          </div>
          <div class="buyers">
            <div class="divider-copy"></div>
            <div class="w-layout-grid grid-buyer-profile">
              <div class="wrap-img-buyer-profile"><img src="images/profile-buyer-min.jpg" loading="lazy" sizes="(max-width: 479px) 92vw, 100vw" srcset="images/profile-buyer-min-p-500.jpeg 500w, images/profile-buyer-min-p-1080.jpeg 1080w, images/profile-buyer-min-p-1600.jpeg 1600w, images/profile-buyer-min-p-2000.jpeg 2000w, images/profile-buyer-min-p-2600.jpeg 2600w, images/profile-buyer-min-p-3200.jpeg 3200w, images/profile-buyer-min.jpg 4096w" alt="" class="image-23"></div>
              <div>
                <div class="w-layout-grid grid-22">
                  <div id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a427-53806dc6" class="div-block-20">
                    <p class="para-bold-22 black">{{$ud->first_name}}&nbsp;{{$ud->last_name}}</p>
                    <p class="para-light-gray">{{$cown->owner_position}}</p>
                    <div class="w-layout-grid grid-bottons-small-copy owner-profile">
                      <a href="#" class="button-3 blue smaller button-4 w-button">{{ ucfirst($cown->status)}}</a>
                    </div>
                  </div>
                </div>
                <p class="para-bold-2 para-bold-16-24">{{$ud->first_name}}&nbsp;{{$ud->last_name}} – {{$cown->owner_position}} - {{$co->address}}, {{$co->city}}, {{$co->country}} </p>
              </div>
            </div>
            <p class="para-body-2 dark-grey">
            <?php
                $storyLength = strlen($ud->story);
                $storypart1 = substr($ud->story, 0, 500);
                $storypart2 = substr($ud->story, 501, 999);
            ?>
            {!! $storypart1 !!} ...
            </p>
           
            <div data-hover="" data-delay="0" class="dropdown-trigger w-dropdown">
            @if(strlen($storypart2) != 0)
                <div class="dropdown-toggle-4 w-dropdown-toggle">
                  <div class="accordian-icon w-icon-dropdown-toggle"></div>
                  <div>Show More </div>
                </div>
              @endif
                <nav class="dropdown-lists w-dropdown-list">
                  <p class="para-body-2">{!! $storypart2 !!}</p>
                </nav>
            </div>
          </div>
          <div class="wrap-media-buyers">
            <h2 class="heading-section-2">Media</h2>
            <div class="w-layout-grid grid-media-buyers"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751aa9f9a78565f9c34b6c_web%20icon.png" loading="lazy" id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a440-53806dc6" alt="" class="image-24">
              <p id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a441-53806dc6" class="para-bold-16-24 media">Website</p>
              <div id="w-node-a07b7bb7-d3dc-1466-1fd9-a5341681d0ec-53806dc6">
                <p class="para-body-2 dark-grey media">{{$co->facebook}}</p>
              </div>
              <p id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a449-53806dc6" class="para-bold-16-24 media">LinkedIn</p>
              <p id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a44b-53806dc6" class="para-bold-16-24 media">Facebook</p><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751ab25bed464c1202d2a1_linkdIn%20icon.png" loading="lazy" id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a44d-53806dc6" alt="" class="image-25"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751abba6f68b3e95d25d1d_facebook%20icon.png" loading="lazy" id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a44e-53806dc6" alt="" class="image-26">
              <p id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a445-53806dc6" class="para-body-2 dark-grey media">{{$co->linkedin}}</p>
              <p id="w-node-_45a17b7f-da03-2c1c-6071-adf71a90a443-53806dc6" class="para-body-2 dark-grey media">{{$co->website}}</p>
            </div>
          </div>
        </div>
        <div class="column-19 w-col w-col-4"></div>
      </div>
    </div>
  </div>
  <div class="wrap-footer">
    <div class="w-container">
      <div class="columns-3 w-row">
        <div class="column-15 w-col w-col-7">
          <div class="w-layout-grid grid-big-botton-hero-left">
            <p class="para-body-2 footer">Corporate Services</p>
            <p class="para-body-2 white">Terms &amp; Conditions</p>
            <p class="para-body-2 white">Privacy Policy</p>
          </div>
        </div>
        <div class="column-14 w-col w-col-5">
          <div class="w-layout-grid grid-footer-right">
            <a href="#" class="link para-body whtie">Twitter</a>
            <a href="#" class="link para-body whtie">Facebook</a>
          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- modal register -->
    <div class="modal mt-5" id="regModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
  
      <div class="modal-body">
       <div class="row">
          <div class="col-md-6 text-center">
            <a href="{{url('/registersseller')}}" target="_blank"><h3>Register As Seller</h3></a>
          </div>
          <div class="col-md-6 text-center">
          <a href="{{url('/registersbuyer')}}" target="_blank"><h3>Register As Buyer</h3></a>
          </div>
       </div>
      </div>
    </div>
  </div>
</div>

  <!-- modal register -->
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=604dff40fd2aff5154978dc0" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  @endsection