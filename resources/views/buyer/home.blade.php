@extends('layouts.user')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" style="background-color: #ffff !important;">
                <div class="card-header card-header-primary">
                <h4 class="card-title"><h2>Dashboard</h2></h4>
                </div>
            </div>

                @if ($message = Session::get('success'))

                <div >

                    <p>{{ $message }}</p>

                </div>

                @endif


                @if ($message = Session::get('warning'))

                <div >

                    <p>{{ $message }}</p>

                </div>

                @endif
            
                    <!-- card group -->
                    <div class="col-md-12">
                        <div class="row justify-content-center align-self-center">
                            <!-- impression -->
                                <div class="card col-md-4 mx-2">
                                    <div class="card-body">
                                        <h3>Impressions</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                            @if(isset($visitoramount))
                                                <h1>{{$visitoramount}}</h1>
                                                    @else
                                                <h1>0</h1>
                                            @endif
                                                 Visitor
                                            </div>
                                            <div class="col-md-6">
                                            <h1>102</h1>
                                            Saved by Investor
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- impression -->
                            <!-- profile setup -->
                                <div class="card col-md-6 mx-2">
                                    <div class="card-body">
                                        <h3>Profile Setup</h3>
                                        <div class="row">
                                            <div class="col-md-5">
                                            @if($percentage == 100)
                                                Excelent , Your profile is complete
                                            @else
                                                Complete your business Profile Information to attract more investors.
                                            @endif
                                            <br>
                                            <a href="{{url('buyerprofile')}}" class="my-2"><button class="btn btn-primary btn-sm">Complete My Profile</button></a>
                                            </div>
                                            <div class="col-md-6">
                                            <h1 class="profile-percentage mx-2">
                                            @if(isset($percentage))
                                            {{round($percentage, 0)}} %
                                            @else
                                            0 %
                                            @endif
                                            </h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- profile setup -->
                        </div>
                        <!-- row 2 -->
                        <div class="row justify-content-center align-self-center">
                            <!-- notification -->
                                <div class="card col-md-6 mx-2">
                                    <div class="card-body">
                                        <h3>Notifications</h3>
                                        Choose type of notifications you want to see
                                  
                                        <div class="row my-3 mx-2">
                                        <div class="col-md-6">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                    New Messages
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                                    Visitors
                                            </div>
                                            <div class="col-md-6">
                                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                                                New Offer
                                            </div>
                                        </div>
                                        <button type="submit" value="submit" class="btn btn-primary btn-sm">Save Preferences</buton>
                                    </div>
                                </div>
                            <!-- notification -->
                            <!-- offers -->
                                <div class="card col-md-4 mx-2">
                                    <div class="card-body">
                                        <h3>Offers</h3>
                                        <div class="row">
                                            <div class="col-md-6">
                                            Most recent offers
                                            </div>
    
                                        </div>
                                    </div>
                                </div>
                            <!-- offers -->
                        </div>
                        <!-- row 2 -->
                    </div>
                    <!-- card group -->
        
    </div>
</div>
@endsection
