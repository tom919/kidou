@extends('layouts.user')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" style="background-color: #ffff !important;">
                <div class="card-header card-header-primary">
                <h4 class="card-title"><h2>Account Details</h2></h4>
                </div>
            </div>


      <!-- card group -->
      <div class="col-md-12">
                        <div class="row justify-content-center align-self-center">
                          <!-- side 1 -->
                            <div class="col-md-6 mx-1">
                                  <!-- seller information -->
                                  <div class="card">
                                    <div class="card-body">
                                        <h5>Account Information</h5>
                                        <div class="row">
                                            <div class="col-md-6">
                                              <div class="card-photo">
                                              @if($ud->photo != null)
                                                <img src="{{url('photo/user')}}/{{ $ud->photo}}" >
                                                @else
                                                <img src="{{url('photo/material/')}}/no_image.jpg" style="" >
                                                @endif
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                            <div class="card-account-detail">
                                            <label>Full Name</label>
                                            <br>
                                            {{ $ud->first_name }}   {{ $ud->last_name }}
                                            </div>
                                          
                                            <div class="card-account-detail">
                                            <label>Birth</label>
                                            <br>
                                            {{ $ud->pob }}  {{ $ud->dob }}
                                            
                                              </div>
                                          <div class="card-account-detail">
                                            <label>Member Since</label>
                                            <br>
                                            {{ $ud->member_since }} 
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- seller information -->
                              <!-- seller story -->
                              <div class="card">
                                    <div class="card-body">
                                        <h5>About</h5>
                                        {!! \Illuminate\Support\Str::limit($ud->story, 255, $end='') !!}
                                        <span id="dots">...</span>
                                        <span id="more">
                                        <?php
                                            echo substr($ud->story,  -(( strlen($ud->story)-1) - 100));
                                          ?>
                                        </span>
                                        <br>
                                        <button onclick="myFunction()" id="myBtn" class="btn btn-primary btn-sm">Read more</button>
                                    </div>
                                </div>
                            <!-- seller story -->
                            </div>
                            <!-- side 2 -->
                            <div class="col-md-5 mx-0">
                                 <!-- contact information -->
                                 <div class="card">
                                    <div class="card-body">
                                        <h5>Contact Information</h5>
                                        <div>
                                        <div class="card-account-detail">
                                          <label>Address</label>
                                          <br>
                                          {{ $ud->address }}  {{ $ud->city }}   {{ $ud->country }}
                                        </div>
                                        <div class="card-account-detail">
                                        <label>Phone</label>
                                        <br>
                                        {{ $ud->phone }}
                                          </div>
                                          <div class="card-account-detail">
                                        <label>Email</label>
                                        <br>
                                        {{ $ua->email }}
                                        <?php 
                                              $vm = Auth::user()->email_verified_at;
                                        ?>
                                        @if($vm != null)
                                        <i class="material-icons">verified</i> 
                                        @else
                                        <a href="{{url('buyeremailverify')}}"> <i class="material-icons">error_outline</i>&nbsp;Verify Now</a>
                                        @endif
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- contact information -->
                              <!-- update button -->
                              <div class="card">
                                    <div class="card-body">
                                    <a href="{{url('/buyerupdatecredential')}}" class="btn btn-primary btn-round text-md-center"><i class="material-icons">vpn_key</i> &nbsp;Update Credential</a>
                                    <a href="{{url('/buyerupdatedetail')}}" class="btn btn-primary btn-round text-md-center"><i class="material-icons">save</i> &nbsp;Update Detail</a>
                                    </div>
                                </div>
                            <!-- update button -->
                            </div>
                        </div>
      </div>
      <!-- card group -->



    </div>
</div>



@endsection
