@extends('layouts.user')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


                
                @if (isset($errors) && count($errors))

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }} </li>
                    @endforeach
                </ul>

                @endif


                <div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title">Update Investment Section</h3>
                </div>
                <div class="card-body">


                <form id="updatedetail" action="{{ url('/buyercompsectionupdate') }}" method="POST" enctype="multipart/form-data" >
            


            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="cid" value="{{$cd[0]->id}}">
            <input type="hidden" name="sid" value="{{$sid}}">
            @switch($sid)
                         @case(1)
                         <p>
                            <h5>Company Name:</h5> 
                            <input class="form-control" type="text" name="company_name" placeholder="Company Name..." value="{{$cd[0]->company_name}}" >
                        </p>
                        <p><h5>About :</h5>
                             <textarea class="ckeditor form-control" name="about" placeholder="About..." >{{$cd[0]->about}}</textarea>
                        </p>
                            @break
                        @case(2)

                        <p>
                            <h5>Establish Date :</h5>
                            <input id="estdate" class="form-control" type="date" data-date-format="yyyy-mm-dd" pattern="\d{4}-\d{2}-\d{2}" name="establish" placeholder="Establish Date..." value="{{$cd[0]->establish_date}}">
                        </p>
                        <p>
                            <h5>Merger and Acqusition Experience :</h5>
                            <select class="form-control selectpicker" id="selMnaexp" name="b_mna_exp" >
                                        <option value="{{ $cd[0]->b_mna_exp }}" selected>{{$cd[0]->b_mna_exp}}</option>
                                        <option value="Yes">Yes</option>
                                        <option value="No">No</option>
                            </select>
                            </p>
                        </p>
                        <p>
                            <h5>Investment Type</h5>
                            <select class="form-control selectpicker" id="selCategory" name="b_inv_type" >
                                        <option value="{{ $cd[0]->b_inv_type }}" selected>{{$cd[0]->b_inv_type}}</option>
                                        <option value="Personal">Personal</option>
                                        <option value="Corporate">Corporate</option>
                            </select>
                            </p>
                        </p>
                        <p>
                            <h5>Investment Size :</h5>
                            <br>
                            <label>Start From :</label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="b_investment_size_start" placeholder="Start From..." value="{{$cd[0]->b_investment_size_start}}">
                            <br>
                            <label>Until</label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="b_investment_size_end" placeholder="Until ..." value="{{$cd[0]->b_investment_size_end}}">
                        </p>
                            @break
                        @case(4)
                                <p>
                                    <h5>Industry</h5>
                                @php( $industry = \App\Industry::all())
                                <select class="form-control selectpicker" id="selIndustry" name="industry[]" multiple="multiple" >
                                <option value="{{ $cd[0]->industry }}" selected>{{$cd[0]->industry}}</option>
                                        @foreach($industry as $ind)
                                            <option value="{{ $ind->text }}">{{ $ind->text }}</option>
                                        @endforeach  
                                    </select>
                                </p>
                                <p>
                                    <h5>Criteria</h5>
                                @php( $criteria = \App\Criteria::all())
                                <select class="form-control selectpicker" id="selCriteria" name="b_criteria[]" multiple="multiple" >
                                <option value="{{ $cd[0]->b_criteria }}" selected>{{$cd[0]->b_criteria}}</option>
                                        @foreach($criteria as $crt)
                                            <option value="{{ $crt->text }}">{{ $crt->text }}</option>
                                        @endforeach  
                                </select>
                                </p>

                                <p>
                                    <h5>Expertise</h5>
                                @php( $expertise = \App\Expertise::all())
                                <select class="form-control selectpicker" id="selExpertise" name="b_expertise[]" multiple="multiple" >
                                <option value="{{ $cd[0]->b_expertise }}" selected>{{$cd[0]->b_expertise}}</option>
                                        @foreach($expertise as $exp)
                                            <option value="{{ $exp->text }}">{{ $exp->text }}</option>
                                        @endforeach  
                                </select>
                                </p>

                                <p>
                                    <h5>Funding Source</h5>
                                @php( $fund_source = \App\FundSource::all())
                                <select class="form-control selectpicker" id="selFundSource" name="b_fund_source[]" multiple="multiple" >
                                <option value="{{ $cd[0]->b_fund_source }}" selected>{{$cd[0]->b_fund_source}}</option>
                                        @foreach($fund_source as $fsr)
                                            <option value="{{ $fsr->text }}">{{ $fsr->text }}</option>
                                        @endforeach  
                                </select>
                                </p>



                            @break
                @case(5)
                <p><h5>Owner Introduction :</h5>
                <textarea class="ckeditor form-control" name="intro" placeholder="Owner Introduction..." >{{$cd[0]->intro}}</textarea></p>
                            @break
                @case(6)
                <p>
                    <h5>Address :</h5>
                    <textarea class="form-control" name="address" placeholder="Address..." >{{$cd[0]->address}}</textarea>
                </p>
                <p>
                    <h5>City :</h5>
                    <input class="form-control" type="text" name="city" placeholder="City..." value="{{$cd[0]->city}}" >
                </p>
                <p>
                    <h5>Postal Code :</h5>
                    <input class="form-control" type="text" name="postal_code" placeholder="Postal Code..." value="{{$cd[0]->postal_code}}">
                </p>
                <p>
                    <h5>Country : </h5>    
                    <input id="myCountry" class="form-control" type="text" name="country" placeholder="Country..." value="{{$cd[0]->country}}" autocomplete="off" >
                </p>
                <p>
                    <h5>Phone :</h5>
                    <input class="form-control" type="text" name="phone" placeholder="Phone..." value="{{$cd[0]->phone}}">
                </p>
                <p>
                    <h5>Email :</h5>
                    <input class="form-control" type="text" name="email" placeholder="Email..." value="{{$cd[0]->email}}">
                </p>
                <p>
                    <h5>Website :</h5>
                    <input class="form-control" type="text" name="website" placeholder="Website..." value="{{$cd[0]->website}}" >
                </p>
                <p>
                    <h5>LinkedIn :</h5>
                    <input class="form-control" type="text" name="linkedin" placeholder="Linkedin..." value="{{$cd[0]->linkedin}}">
                </p>
                <p>
                    <h5>Instagram :</h5>
                    <input class="form-control" type="text" name="instagram" placeholder="Instagram..." value="{{$cd[0]->instagram}}" >
                </p>
                <p>
                    <h5>Facebook : </h5>
                    <input class="form-control" type="phone" name="facebook" placeholder="Facebook..." value="{{$cd[0]->facebook}}">
                </p>

                    @break








            @endswitch            
            <p><button  type="submit" name="register" class="btn btn-primary pull-right">Save &nbsp;<i class="material-icons">save</i></button></p>
    






            </form>

            </div>
        </div>
          
        </div>

</div>
<script>
// date time 
$(function () {
        $('#estdate').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            time:false,
        });
    });

// date time
</script>

@endsection