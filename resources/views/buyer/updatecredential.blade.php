@extends('layouts.user')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card text-center">

                @if (isset($errors) && count($errors))

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }} </li>
    @endforeach
</ul>

@endif


<!-- test form -->

<div class="card">
                <div class="card-header card-header-primary">
                  <h3 class="card-title">Update Credential</h3>
                </div>
                <div class="card-body">
      <form  method="POST" action="{{ url('/buyersavecredential') }}" method="POST">

      @csrf

        <div class="form-group row">
          <h5>Email</h5>
          <input type="email" name="email" class="form-control" value="{{$uc->email}}">
        </div>
        
        <div class="form-group row">
          <h5>Password</h5>
          <input type="password" name="password" class="form-control" >
        </div>

        <div class="form-group row">
          <h5>Confirm Password</h5>
          <input type="password" name="repassword" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary"> <i class="material-icons">save</i>&nbsp;Update</button>

      </form>
</div>
</div>


         

                </div>
            </div>
        </div>
    </div>
</div>
@endsection