@extends('layouts.user')

@section('content')
<div class="container">



   
<div class="card" style="background-color: #ffff !important;">
                <div class="card-header card-header-primary">
                  <h2 class="card-title">My Investment</h2>
                </div>

        </div>



        @if($cd == null)
            <div class="text-md-center">
            <a href="{{url('/buyercompanycreate')}}"><button class="btn btn-light btn-lg"> <i class="material-icons">add_circle_outline</i> &nbsp; Add Investment Profile </button></a>
            </div>
        @else

            <?php 
                    $inv_size_start = number_format($cd[0]->b_investment_size_start, 0);
                    $inv_size_end = number_format($cd[0]->b_investment_size_end, 0);
            ?>
            
                           <!-- card group -->
      <div class="col-md-12">
                        <div class="row ">
                          <!-- side 1 -->
                            <div class="col-md-7 mx-0">
                                  <!-- company information -->
                                            <div class="card">
                                    <div class="card-body">
                                  
                                        <div class="row justify-content-center">
                                        <div class="col-md-12 text-center">
                                            <h4 class="text-uppercase" style="color: #00008b; font-size: large; font-weight: 700;"> {{$cd[0]->company_name}} </h4>
                                          </div>
                                            <div class="col-md-6 text-center">
                                            
                                              <div class="card-photo">
                                            
                                              @if($cd[0]->photo != null)
                                                <img src="{{url('photo/company')}}/{{ $cd[0]->photo}}">
                                                @else
                                                <img src="{{url('photo/material/')}}/no_image.jpg" style="width: 100%;" >
                                                @endif
                                              </div>
                                              <a href="{{ url('buyercompidentityedit') }}" class="mx-5"><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- company information -->
                            <!--  caption  -->

                            <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                         
                                          <div class="p-3">
                                          {!! \Illuminate\Support\Str::limit($cd[0]->about, 255, $end='') !!}
                                          <span id="dots">...</span>
                                        <span id="more">
                                        <?php
                                            echo substr($cd[0]->about,  -(( strlen($cd[0]->about)-1) - 100));
                                          ?>
                                        </span>
                                        <br>
                                        <button onclick="myFunction()" id="myBtn" class="btn btn-primary btn-sm">Read more</button>
                                            <div class="pull-right"> 
                                              <a href="{{ url('buyercompsectionedit/1') }}" ><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                          </div>
                                          </div>
                                        </div>
                              </div>
                            <!-- caption  -->

                                      <!--  owner intro  -->

                                      <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                          <div class="col-md-12 text-center">
                                            <h4 class="text-uppercase" style="color: #00008b; font-size: large; font-weight: 700;">Owner Intro</h4>
                                          </div>
                                          <div class="p-3">
                                          {!! \Illuminate\Support\Str::limit($cd[0]->intro, 255, $end='') !!}
                                          <span id="dotsintro">...</span>
                                        <span id="moreintro">
                                        <?php
                                            echo substr($cd[0]->intro, -(( strlen($cd[0]->intro)-1) - 100));
                                          ?>
                                        </span>
                                        <br>
                                        <button onclick="myFunctionintro()" id="myBtnintro" class="btn btn-primary btn-sm">Read more</button>
                                            <div class="pull-right"> 
                                              <a href="{{ url('buyercompsectionedit/5') }}" ><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                          </div>
                                          </div>
                                        </div>
                                        </div>
                                      <!-- owner intro  -->

                          <!-- contact  -->

                            <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="p-1 col-md-12">
                                                  <h5 class="text-left p-2"> Contact</h5>
                                                      <div class="row my-3">
                                                              <div class="col-md-6">
                                                                <p><label class="mx-2"><i class="material-icons">location_on</i></label> 
                                                                  &nbsp;
                                                                  {{$cd[0]->address}} <br>{{$cd[0]->city}} {{$cd[0]->country}}
                                                                </p>
                                                                <p><label class="mx-1"><i class="material-icons">mail</i></label>
                                                                  &nbsp;{{$cd[0]->email}}</p> 
                                                                <p><label class="mx-2"><i class="material-icons">phone</i></label>
                                                                  &nbsp;{{$cd[0]->phone}}</p>
                                                              </div>

                                                              <div class="col-md-6">
                                                                <p><label class="mx-1">Website</label>
                                                                  <br>{{$cd[0]->website}}</p>
                                                                <p><label class="mx-1">Linkedin</label> 
                                                                  <br>{{$cd[0]->linkedin}}</p>
                                                                <p><label class="mx-1">Instagram</label>
                                                                  <br>&nbsp;{{$cd[0]->instagram}}</p>
                                                                <p><label class="mx-1">Facebook</label>
                                                                  <br>&nbsp;{{$cd[0]->facebook}}</p>
                                                              </div>
                                                          </div>
                                                    <a href="{{ url('sellercompsectionedit/6') }}" class="pull-right" ><i class="material-icons">edit</i> &nbsp;Update</a>
                                              </div>
                                          </div>
                                        </div>
                              </div>
                            <!-- contact  -->

                            </div>
                              <!-- side 2 -->
                              <div class="col-md-4 mx-0">

                              <!-- shared -->
                              <div class="card">
                                    <div class="card-body text-center">
                                    <a href="{{url('buyershared/')}}/{{$cd[0]->id}}" target="_blank"><button class="btn btn-primary btn-sm"><i class="material-icons">share</i> &nbsp;Preview My Business</button></a>
                                    </div>
                              </div>
                              <!-- shared -->
                                  <!-- contact information -->
                                  <div class="card">
                                    <div class="card-body">
                                        <h5>General Information</h5>
                                        <div>
                                        <div class="card-account-detail">
                                          <label>Investment Type</label>
                                          <br>
                                          {{$cd[0]->b_inv_type}}
                                        </div>
                                        <div class="card-account-detail">
                                        <label>M&A Experience</label>
                                        <br>
                                        {{$cd[0]->b_mna_exp}}
                                          </div>
                                        <div class="card-account-detail">
                                        <label> Registered date</label>
                                        <br>
                                        {{$cd[0]->registered_since}} 
                                          </div>
                                        <div class="card-account-detail">
                                        <label>Establish date</label>
                                        <br>
                                        {{$cd[0]->establish_date}}
                                          </div>
                                          <div class="card-account-detail">
                                        <label>Investment Size</label>
                                        <br>
                                        $ {{$inv_size_start}} - {{$inv_size_end}}
                                          </div>
                                        </div>
                                        <div class="mt-2">
                                        <a href="{{ url('buyercompsectionedit/2') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update</a>
                                        </div>
                                    </div>
                                </div>
                            <!-- contact information -->
                                  <!-- main photo  -->
                                    <div class="card">
                                            <div class="card-body">
                                                <h5>Main Photo</h5>
                                                        @if($cd[0]->photo_main != null)
                                                        <img src="{{url('photo/company')}}/{{ $cd[0]->photo_main}}" style="width: 100%;" class="mx-0" >
                                                        @else
                                                        <img src="{{url('photo/material/')}}/no_image.jpg" style="width: 100%;" >
                                                        @endif
                                                        <a href="{{ url('buyercompmainphotoedit') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update Main Photo</a>
                                            </div>
                                    </div>
                                    <!-- main photo -->
                                    <!-- categorial information -->
                                  <div class="card">
                                    <div class="card-body">
                                       
                                            <h5>Categorial Information</h5>
                                      
                                        <div class="card-account-detail">
                                          <label>Industry</label>
                                          <br>
                                          {{$cd[0]->industry}}
                                        </div>

                                        <div class="card-account-detail">
                                            <label>Criteria</label>
                                            <br>
                                            {{$cd[0]->b_criteria}}
                                        </div>

                                        <div class="card-account-detail">
                                          <label> Expertise</label>
                                          <br>
                                          {{$cd[0]->b_expertise}}
                                        </div>

                                        <div class="card-account-detail">
                                            <label>Fund Source</label>
                                            <br>
                                            {{$cd[0]->b_fund_source}}
                                        </div>

                                        <div class="mt-2">
                                        <a href="{{ url('buyercompsectionedit/4') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update</a>
                                        </div>
                                        
                                    </div>
                                </div>
            
                            <!-- categorial information -->
                                    <!-- slide photo  -->
                                    <div class="card">
                                    <div class="card-body">
                                        <h5>Background Photo</h5>
                                                @if($cd[0]->photo_slide != null)
                                                <img src="{{url('photo/company')}}/{{ $cd[0]->photo_slide}}" style="width: 100%;" class="mx-0" >
                                                @else
                                                <img src="{{url('photo/material/')}}/no_image.jpg" style="width: 100%;" >
                                                @endif
                                                <a href="{{ url('buyercompslidephotoedit') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update Background Photo</a>
                                    </div>
                                    </div>
                                    <!-- slide  photo -->
                                                 <!-- other feature  -->
                                                <div class="card">
                                                <div class="card-body">
                                                    <h5>Other Feature</h5>
                                                    <div class="card-account-detail">
                                                      <a href="{{url('/buyerpreftrans')}}" >Prefered Transaction</a>
                                                    </div>
                                                </div>
                            </div>
                            <!-- other feature -->
                              </div>
                        </div>
        </div>

        @endif


</div>
            


</div>

@endsection