@extends('layouts.user')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


                
                @if (isset($errors) && count($errors))

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }} </li>
                    @endforeach
                </ul>

                @endif


                <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Update Investment Identity</h4>
                </div>
                <div class="card-body">


                <form id="updatedetail" action="{{ url('/buyercompidentityupdate') }}" method="POST" enctype="multipart/form-data" >
            


            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="cid" value="{{$cd[0]->id}}">
    
            <p>
                <h5>Investment Name</h5>
                <input type="text" name="company_name" class="form-control" value="{{ $cd[0]->company_name}}">
            </p>


            <p>
                <h5>Investment Logo</h5>
                <br>
                @if($cd[0]->photo != null)
                <img src="{{url('photo/company')}}/{{ $cd[0]->photo}}" style="max-width: 50%;"  class="shadow-lg rounded">
                @else
                <img src="{{url('photo/material/')}}/no_image.jpg" style="max-width: 50%;" class="shadow-lg rounded">
                @endif
                  <input class="form-control" type="file" name="photo" >

            </p>
            <p><button  type="submit" name="register" class="btn btn-primary pull-right">Save &nbsp;<i class="material-icons">save</i></button></p>
    






            </form>

            </div>
        </div>
          
        </div>
    </div>
</div>


@endsection