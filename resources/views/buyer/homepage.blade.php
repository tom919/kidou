@extends('layouts.homepage')

@section('content')
<body class="body-7">
<div class="section-hero-kidou-home">
    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar-3 w-nav">
      <div class="container-13 w-container">
        <a href="{{url('/buyer')}}" class="brand w-nav-brand"><img src="{{url('/')}}/maikai/images/output-onlinepngtools.png" loading="lazy" width="161" sizes="161px" srcset="{{url('/')}}/maikai/images/output-onlinepngtools-p-500.png 500w, {{url('/')}}/maikai/images/output-onlinepngtools-p-800.png 800w, {{url('/')}}/maikai/images/output-onlinepngtools-p-1080.png 1080w, {{url('/')}}/maikai/images/output-onlinepngtools.png 1252w" alt=""></a>
        <div class="w-layout-grid grid-34">
          <nav role="navigation" class="nav-left w-nav-menu">
            <a href="#" class="nav-link-12 w-nav-link">Pricing</a>
            <a href="#" class="nav-link-11 w-nav-link">Pricing</a>
          </nav>
          <nav role="navigation" class="nav-right w-nav-menu">
            <a href="#" class="nav-link-13 w-nav-link">Help</a>
            <a href="{{url('/login')}}" class="nav-link-14 w-nav-link"><strong>Log in</strong></a>
            <a href="{{url('/registersbuyer')}}" class="button-home-page-nav w-button">Create a Profile</a>
          </nav>
        </div>
        <div class="w-nav-button">
          <div class="icon-8 w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
    <div class="w-layout-grid grid-sec-hero">
      <div id="w-node-_2201c7e4-6494-02a8-7cf1-88ec84bd0a87-eba9d9d8" class="wrap-hero-content"></div>
      <div id="w-node-_4b4206b0-708d-99c2-6de2-07360b91ef21-eba9d9d8" class="wrap-hero-img">
        <div class="w-layout-grid grid-float">
          <div id="w-node-b0a9cd39-1513-e0c0-a332-7208ac9cedc5-eba9d9d8" class="wrap-cont-hero-cont">
            <div class="container-15 w-container">
              <div class="w-layout-grid grid-content-hero">
                <div id="w-node-c75cca47-7b47-838b-1232-b5546476f7eb-eba9d9d8" class="wrap-content-hero-center">
                  <h1 class="h1-home-page">Find the perfect small business for you</h1>
                  <div class="div-block-48">
                    <p class="para-body-home-page white">Are you ready to start your own business? KIDOU is the go-to marketplace for business buyers like you. With our unique matching capabilities, we want to give you the best opportunities to find the perfect small business for you. Start making the world a better place now with KIDOU</p>
                  </div>
                  <a href="{{url('/registersbuyer')}}" class="button-home-page w-button">Create a Profile</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="w-node-_7ced9af7-33ff-74ad-878f-9bec24a7fbc2-eba9d9d8" class="wrap-hero-content"></div>
    </div>
    <div class="container-14 w-container">
      <div class="w-layout-grid the-points">
        <div id="w-node-def8f7ed-7695-fcda-4d8f-4f114bcfb3f2-eba9d9d8" class="div-block-52"><img src="{{url('/')}}/maikai/images/pc5o78d9i-1.png" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#" class="nav-link-bold">Safe and Secure</a>
        </div>
        <div id="w-node-def8f7ed-7695-fcda-4d8f-4f114bcfb3f6-eba9d9d8" class="div-block-51"><img src="{{url('/')}}/maikai/images/pc5o78d9i-1.png" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#" class="nav-link-bold">Successful Business</a>
        </div>
        <div id="w-node-def8f7ed-7695-fcda-4d8f-4f114bcfb3fa-eba9d9d8" class="div-block-53"><img src="{{url('/')}}/maikai/images/pc5o78d9i-1.png" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#" class="nav-link-bold">Easy Set-Up</a>
        </div>
        <div id="w-node-_7cdaf552-2e24-1807-70bd-1b555cd9342e-eba9d9d8" class="div-block-54"><img src="{{url('/')}}/maikai/images/pc5o78d9i-1.png" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#" class="nav-link-bold">24/7 Support</a>
        </div>
      </div>
    </div>
  </div>
  <div class="section-13">
    <div class="w-container">
      <h1 class="heading-35">Are you ready to start your own business? KIDOU is the go-to marketplace for business buyers like you. With our unique matching capabilities, we want to give you the best opportunities to find the perfect small business for you. Start making the world a better place now with KIDOU</h1>
      <h1 class="_3456">3456+</h1>
      <p class="para-bold">Joined already</p>
      <a href="{{url('/registersbuyer')}}" class="button-home-page-nav w-button">Create a Profile</a>
    </div>
  </div>
  <div class="section-15">
    <div class="w-container">
      <p class="para-body-home-page _14">EASY SET UP</p>
      <div class="w-layout-grid grid-40">
        <div id="w-node-_4280dc1b-c71f-d939-03f2-d00b958a8327-eba9d9d8">
          <h2 class="sub-heading h2">Starting a business has never been so easy</h2>
        </div>
        <div id="w-node-ba46db08-0398-d8da-55ce-e6861f6429c0-eba9d9d8" class="div-block-39">
          <div class="w-layout-grid grid-41"><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_76660855-e835-360d-d646-94f327044877-eba9d9d8" alt="">
            <div>
              <div class="para-bold">Easy set-up</div>
              <p class="para-body-home-page">With Kidou, you’re only a few taps away from finding the perfect business for you.</p>
            </div>
            <div>
              <div class="para-bold"><strong>High pay-offs</strong></div>
              <p class="para-body-home-page">Turn risks into opportunities and start trading within days.</p>
            </div>
            <div>
              <div class="para-bold"><strong>Quick and simple</strong></div>
              <p class="para-body-home-page">Don’t spend months trying to find the perfect fit. Kidou effectively matches you with businesses that fit your needs.</p>
              <a href="#" class="button-home-page box w-button">Create a Profile</a>
            </div><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-a892d8e2-4f3c-3899-bbec-c0d629172c5d-eba9d9d8" alt=""><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-b81f12ea-24cb-2675-fffd-ec5ecf4a146a-eba9d9d8" alt="">
          </div>
        </div>
      </div>
    </div>
    <div class="w-layout-grid grid-42-buyer">
      <div id="w-node-a4e1e8bb-b68b-bc8e-6e41-4c229627b3f0-eba9d9d8" class="div-block-40 find-clients">
        <div class="div-block-42">
          <h1 class="_3456">3456+</h1>
          <p class="para-bold">something</p>
        </div>
      </div>
    </div>
  </div>
  <div class="section-16">
    <div class="w-container">
      <p class="para-body-home-page _14">SUCCESSFUL BUSINESS</p>
      <div class="w-layout-grid grid-43">
        <div id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544a5-eba9d9d8" class="div-block-39">
          <h1 class="sub-heading h2">Run a successful business</h1>
          <div class="w-layout-grid grid-41">
            <div>
              <div class="para-bold">Become a successful entrepreneur</div>
              <p class="para-body-home-page">With Kidou, you can easily find a business you love that’s already doing well.</p>
            </div>
            <div>
              <div class="para-bold"><strong>Low risk</strong></div>
              <p class="para-body-home-page">Start a business in an industry you are passionate about, building on existing foundations with low risk.</p>
            </div>
            <div id="w-node-_4fc5fc02-1b7b-eee5-1a1d-8ce647e4dcb9-eba9d9d8">
              <div class="para-bold"><strong>Your Business, Your Journey</strong></div>
              <p class="para-body-home-page">Start trading as soon as you buy your new business and make your new business truly you.</p>
              <a href="#" class="button-home-page box w-button">Create a Profile</a>
            </div><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544b9-eba9d9d8" alt=""><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544ba-eba9d9d8" alt=""><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_10c4944c-4c4c-bc45-7b2e-e423722907cd-eba9d9d8" alt="">
          </div>
        </div>
        <div class="div-block-43">
          <div class="cards-grid-container-4-copy">
            <div id="w-node-_56f76b3d-450d-14bb-0ae5-541a4aa6e563-eba9d9d8" class="div-block-41">
              <div class="cards-image-mask"></div>
              <h3 class="sub-heading">3456+</h3>
              <p class="para-bold">something</p>
            </div>
            <div id="w-node-_56f76b3d-450d-14bb-0ae5-541a4aa6e56a-eba9d9d8">
              <div class="cards-image-mask right"></div>
              <h3 id="w-node-_56f76b3d-450d-14bb-0ae5-541a4aa6e56d-eba9d9d8" class="sub-heading">3456+</h3>
              <p class="para-bold">something</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section-15-copy">
    <div class="w-container">
      <p class="para-body-home-page _14">SAFE AND SECURE</p>
      <div class="w-layout-grid grid-40">
        <div>
          <h1 class="sub-heading h2">Safe and secure</h1>
        </div>
        <div class="div-block-39">
          <div class="w-layout-grid grid-41"><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040cb-eba9d9d8" alt="">
            <div>
              <div class="para-bold"><strong>Anonymous</strong></div>
              <p class="para-body-home-page">Your profile will only be visible to business sellers you match with through our safe and secure platform.</p>
            </div>
            <div>
              <div class="para-bold"><strong>You’re in control</strong></div>
              <p class="para-body-home-page">Choose what information you want to share with potential sellers.</p>
            </div>
            <div>
              <div class="para-bold"><strong>No commitments</strong></div>
              <p class="para-body-home-page">Spend as much time as you need browsing businesses for sale until you find the perfect fit.</p>
              <a href="#" class="button-home-page box w-button">Create a Profile</a>
            </div><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040dd-eba9d9d8" alt=""><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040de-eba9d9d8" alt="">
          </div>
        </div>
      </div>
    </div>
    <div class="w-layout-grid grid-42 buyer-2">
      <div id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040e0-eba9d9d8" class="div-block-40">
        <div class="div-block-42">
          <h1 class="_3456 right">3456+</h1>
          <p class="para-bold">something</p>
        </div>
      </div>
    </div>
  </div>
  <div class="section-17">
    <div class="w-container">
      <h4 class="heading-34">Take your entrepreneurial journey to the next level and buy a small business with Kidou. Find the perfect business for you with our safe and secure platform, and start a business using the foundations already in place by another business owner. Low risk, high pay-off.</h4>
    </div>
  </div>
  <div class="section-18">
    <div class="w-container">
      <p class="para-body-home-page _14">LATEST MATERIALS</p>
      <div class="w-layout-grid grid-44">
        <h1 id="w-node-_903a629b-782b-5802-1a29-80c0c4017c31-eba9d9d8" class="sub-heading">Lorem Ipsum is simply dummy text</h1>
      </div>
      <div class="w-layout-grid grid-45">
        <div id="w-node-_625a3637-a012-21fd-50f7-ff19d0b48180-eba9d9d8" class="wrap-img-galery _1"></div>
        <div id="w-node-ec3a5bf1-f293-1972-8a6b-fe734640633d-eba9d9d8" class="wrap-img-galery _2"></div>
        <div id="w-node-_115950ed-e33b-3782-0cc9-d40ddaccdb94-eba9d9d8" class="wrap-img-galery _3"></div>
        <div id="w-node-_3b5bd80e-4ebd-88c6-f8ad-6a1caddd38e7-eba9d9d8" class="wrap-img-galery _4"></div>
      </div>
    </div>
  </div>
  <div class="section-20">
    <div class="w-container">
      <p class="para-body-home-page _14">HOW TO START</p>
      <h1 class="sub-heading">HOW TO START</h1>
      <ol role="list" class="list-3-buyer">
        <li class="how-to-start">Sign up to KIDOU</li>
        <li class="how-to-start">Browse thousands of small businesses</li>
        <li class="how-to-start">Buy the right business for you</li>
      </ol>
      <div class="w-layout-grid grid-46">
        <div id="w-node-f624a73d-23be-9124-5214-2d626f1920ef-eba9d9d8" class="div-block-46">
          <h1 class="sub-heading white">Lorem Ipsum is simply dummy text</h1>
          <a href="{{url('/registersbuyer')}}" class="button-home-page box white w-button">Create a Profile</a>
        </div>
      </div>
    </div>
  </div>
  <div class="section-21">
    <div class="w-container">
      <div class="w-layout-grid grid-47">
        <div>
          <div class="w-layout-grid grid-48">
            <div class="div-block-47"><img src="{{url('/')}}/maikai/images/Kidou-Visual-Branding-v1_page-0002-icon.jpg" loading="lazy" width="204" sizes="204px" srcset="{{url('/')}}/maikai/images/Kidou-Visual-Branding-v1_page-0002-icon-p-500.jpeg 500w, {{url('/')}}/maikai/images/Kidou-Visual-Branding-v1_page-0002-icon-p-800.jpeg 800w, {{url('/')}}/maikai/images/Kidou-Visual-Branding-v1_page-0002-icon-p-1080.jpeg 1080w, {{url('/')}}/maikai/images/Kidou-Visual-Branding-v1_page-0002-icon.jpg 1200w" alt=""></div>
            <div>
              <div class="w-layout-grid grid-49">
                <div>
                  <p class="para-body-home-page">Terms &amp; Conditions</p>
                </div>
                <div>
                  <p class="para-body-home-page">Privacy &amp; Policy</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="w-node-_600839cd-ed87-37f4-84ce-78fcbebe7feb-eba9d9d8">
          <div>
            <div class="w-layout-grid grid-49-copy">
              <div id="w-node-_600839cd-ed87-37f4-84ce-78fcbebe7ff1-eba9d9d8">
                <p class="para-body-home-page">Instagram</p>
              </div>
              <div id="w-node-_79d50865-a004-93e5-e522-b0901371288b-eba9d9d8">
                <p class="para-body-home-page">Linkedn</p>
              </div>
              <div id="w-node-_600839cd-ed87-37f4-84ce-78fcbebe7ff4-eba9d9d8">
                <p class="para-body-home-page">Facebook</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection