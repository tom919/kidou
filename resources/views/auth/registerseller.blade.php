@extends('layouts.register')

@section('content')

    <div class="row content-row">
    <div class="col-md-6 login-section">
      <img src="{{url('/photo/material')}}/city.jpg" class="login-img pull-left" alt="kidou">
    </div>
        <div class="col-md-6 mt-3 w-50">

                                      <div class="regform">
                                              <div class="text-center mb-1">
                                            <img src="{{ url('photo/material/logo.png')}}" class="logo-img " style="width: 35%">&nbsp;
                                            </div>
                                              <div class="regHeader">
                                                <h1>Create Seller Account</h1>
                                             Let's start with some basic details before getting to know you better
                                              </div>

                                      


                                              @if (isset($errors) && count($errors))

                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }} </li>
                                                @endforeach
                                            </ul>

                                            @endif

                                                  <form id="regForm" method="POST" action="{{ route('register') }}">

                                                  @csrf
                                                        <div class="tab">
                                                          <p>
                                                              <input class="form-control" type="email" name="email" placeholder="E-mail..." >
                                                          </p>
                                                          <input type="hidden" name="user_type" value="3">
                                                        </div>


                                                        <div class="tab">
                                                              <p><input class="form-control" name="password" type="password" placeholder="Password..." ></p>
                                                              <p>  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Retype Password..."  required></p>
                                                        </div>

                                                        <div class="tab">
                                                          <p><input class="form-control" name="first_name" placeholder="First name..." ></p>
                                                          <p><input class="form-control" name="last_name" placeholder="Last name..." ></p>
                                                          <p><button  type="submit" name="register" class="btn btn-primary pull-right">Register</button></p>
                                                        </div>


                                                        <div style="overflow:auto;">
                                                          <div style="float:right;">
                                                            <button type="button" class="btn btn-primary" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                                            <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                                          </div>
                                                        </div>


                                                  <div style="text-align:left;margin-top:-5px;">
                                                  <span class="step"></span>
                                                    <span class="step"></span>
                                                    <span class="step"></span>
                                                  </div>

                                                  </form>

                                                  <div class="regFooter">
                                                  <h7>Already have account, <a href="{{url('/')}}/login">Login here</a></h7>
                                                 </div>
                                        </div>

          </div>
      </div>


<script>


// $(function () {
//   $("#datepicker").datepicker({
//         format:'yyyy-mm-dd', 
//         todayHighlight: true
//   });
// });





</script>





  
@endsection
