@extends('layouts.register')

@section('content')

    <div class="row justify-content-center">
    <div class="col-md-6 login-section">
      <img src="{{url('/photo/material')}}/tent.jpg" class="login-img pull-left" alt="kidou">
    </div>
        <div class="col-md-6">

                                      <div class="regform">
                                        
                                        
                                        
                                    
              <div class="text-center">
              <div class="regHeader">
                <h2>Create Account</h2>
            
              </div>
                              <a href="{{url('registersseller')}}">
                                        <button type="button" class="btn btn-primary"><i class="material-icons">storefront</i>&nbsp;
                                          Create Seller Account
                                      </button>
                              </a>

                              <a href="{{url('registersbuyer')}}">
                                      <button type="button" class="btn btn-primary"><i class="material-icons">shopping_cart</i>&nbsp;
                                        Create Buyer Account
                                      </button>
                              </a>

                              <div class="regFooter">
                              <h7>Already have account, <a href="{{url('/')}}/login">Login here</a></h7>
                              </div>
              </div>


                                            
                                        </div>

          </div>
      </div>







  
@endsection
