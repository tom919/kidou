@extends('layouts.register')

@section('content')
<div class="row h-100 content-row">
    <div class="col-md-6 login-section">
      <img src="{{url('/photo/material')}}/citywalk.jpg" class="login-img pull-left" alt="kidou">
    </div>
        <div class="col-md-6 mt-3 w-50">

                                      <div class="regform">
                                        
                                      <div class="text-center mb-5">
                                            <img src="{{ url('photo/material/logo.png')}}" class="logo-img " style="width: 35%">&nbsp;
                                            </div>

                                            <div class="regHeader text-center">
                                              <h2>Reset Password</h2>
                                              </div>


                                              @if (isset($errors) && count($errors))

                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }} </li>
                                                @endforeach
                                            </ul>

                                            @endif
                                    



                                                        <form method="POST" action="{{ route('password.email') }}">
                                                        @csrf

                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <label for="email">{{ __('E-Mail Address') }}</label>
                                                            </div>
                                                            <div class="col-md-8 mx-0">
                                                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                                @error('email')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                @enderror
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="row justify-content-center">
                                                                <div class="col-md-4">
                                                                    <button type="submit" class="btn btn-primary">
                                                                        {{ __('Send Password Reset Link') }}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                  <div class="regFooter">
                                                  <h7>Already have account, <a href="{{url('/')}}/login">Login here</a></h7>
                                                 </div>
                                        </div>

          </div>
      </div>


@endsection
