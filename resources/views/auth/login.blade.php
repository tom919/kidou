@extends('layouts.register')

@section('content')
<div class="row">
    <div class="col-md-6 login-section">
      <img src="{{url('/photo/material')}}/tent.jpg" class="login-img pull-left" alt="kidou">
    </div>

    
        <div class="col-md-6 mt-0 w-50">


                         
                 <div class="regform">
                                            <div class="text-center mb-1">
                                            <img src="{{ url('photo/material/logo.png')}}" class="logo-img " style="width: 45%">&nbsp;
                                            </div>

                                              <div class="regHeader">
                                            @if (isset($errors) && count($errors))
                                              <div  style="color: red;">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }} </li>
                                                @endforeach
                                            </ul>
                                            </div>
                                            @endif

                                            @if (session()->has('notification'))
                                            <div style="color: red;">
                                                {!! session('notification') !!}
                                                <br>
                                            </div>
                                        @endif

                                        @if ($message = Session::get('success'))

                                        <div >

                                            <p>{{ $message }}</p>

                                        </div>

                                    @endif


                                    @if ($message = Session::get('warning'))

                                        <div >

                                            <p>{{ $message }}</p>

                                        </div>

                                    @endif
                                              <h2>Sign In</h2>
                                                Or <a href="{{url('/register')}}">Create Account</a>
                                              </div>
                                        
                                    

               


                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <div>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email ..." value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                           
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password ..." required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                         
                        </div>

                        
                        <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <?php
                                        $permitted_chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                                        $secode=substr(str_shuffle($permitted_chars), 0, 6);
                                ?>
                                <input type="hidden" name="secode" value="{{$secode}}">
                                <div>
                                    <h3 class="text-center">{{$secode}}</h3>
                                </div>
                            </div>
                            <div class="col-md-8 mx-0">
                            <input id="seccode" type="text" class="form-control @error('seccode') is-invalid @enderror" name="seccode" placeholder="Captcha ...">

                                @error('seccode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        </div>

                        <div class="form-group row text-md-center">
                            <div class="col-md-6">
                                    @if (Route::has('password.request'))
                                                <a class="pull-left" href="{{ route('password.request') }}">
                                                    {{ __('Forgot Your Password?') }}
                                                </a>
                                            @endif
                                </div>

                            <div class="col-md-6">
                                     
                            <button type="submit" class="btn btn-primary pull-right"><i class="material-icons">vpn_key</i>&nbsp;
                                {{ __('Login') }}
                            </button>
                            </div>
                        </div>

                 
                    </form>

                                            
                                        </div>

          </div>
      </div>
</div>


  
@endsection
