@extends('layouts.register')

@section('content')

    <div class="row content-row">
    <div class="col-md-6 login-section">
      <img src="{{url('/photo/material')}}/city.jpg" class="login-img pull-left" alt="kidou">
    </div>
        <div class="col-md-6 mt-3 w-50">

                                      <div class="regform">
                                            <div class="text-center mb-1">
                                            <img src="{{ url('photo/material/logo.png')}}" class="logo-img " style="width: 35%">&nbsp;
                                            </div>
                                              <div class="regHeader text-md-center">
                                                <h2>Create Buyer Account</h2>
                                             Let's start with some basic details before getting to know you better
                                              </div>

                                      


                                              @if (isset($errors) && count($errors))

                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>{{ $error }} </li>
                                                @endforeach
                                            </ul>

                                            @endif
                                    



                                                  <form id="regForm" method="POST" action="{{ route('register') }}">

                                                  @csrf
                                                        <div class="tab">
                                                          <p><input class="form-control" type="email" name="email" placeholder="E-mail..." ></p>
                                                          <input type="hidden" name="user_type" value="4">
                                                        </div>


                                                        <div class="tab">
                                                              <p><input class="form-control" name="password" type="password" placeholder="Password..." ></p>
                                                              <p>  <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  placeholder="Retype Password..."  required></p>
                                                        </div>

                                                        <div class="tab">
                                                          <p><input class="form-control" name="first_name" placeholder="First name..." ></p>
                                                          <p><input class="form-control" name="last_name" placeholder="Last name..." ></p>
                                                          <p><button  type="submit" name="register" class="btn btn-primary pull-right">Register</button></p>
                                                        </div>


                                                        <div style="overflow:auto;">
                                                          <div style="float:right;">
                                                            <button type="button" class="btn btn-primary" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                                                            <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next</button>
                                                          </div>
                                                        </div>


                                                  <div style="text-align:center;margin-top:40px;">
                                                  <span class="step"></span>
                                                    <span class="step"></span>
                                                    <span class="step"></span>
                                                  </div>

                                                  </form>

                                                  <div class="regFooter">
                                                  <h7>Already have account, <a href="{{url('/')}}/login">Login here</a></h7>
                                                 </div>
                                        </div>

          </div>
      </div>


<script>


$(function () {
  $("#datepicker").datepicker({
        format:'yyyy-mm-dd', 
        todayHighlight: true
  });
});


var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
// This function will display the specified tab of the form ...
var x = document.getElementsByClassName("tab");
x[n].style.display = "block";
// ... and fix the Previous/Next buttons:
if (n == 0) {
document.getElementById("prevBtn").style.display = "none";
} else {
document.getElementById("prevBtn").style.display = "inline";
}
if (n == (x.length - 1)) {

document.getElementById("nextBtn").style.display = "none";;
} else {
document.getElementById("nextBtn").style.display = "inline";
document.getElementById("nextBtn").innerHTML = "Next &nbsp;<i class='far fa-hand-point-right'></i>";
}
// ... and run a function that displays the correct step indicator:
fixStepIndicator(n)
}

function nextPrev(n) {
// This function will figure out which tab to display
var x = document.getElementsByClassName("tab");
// Exit the function if any field in the current tab is invalid:
if (n == 1 && !validateForm()) return false;
// Hide the current tab:
x[currentTab].style.display = "none";
// Increase or decrease the current tab by 1:
currentTab = currentTab + n;
// if you have reached the end of the form... :
if (currentTab >= x.length) {
//...the form gets submitted:
//   console.log('fire');
// document.getElementById("regForm").submit();
return true;
}
// Otherwise, display the correct tab:
showTab(currentTab);
}

function validateForm() {
// This function deals with validation of the form fields
var x, y, i, valid = true;
x = document.getElementsByClassName("tab");
y = x[currentTab].getElementsByTagName("input");
// A loop that checks every input field in the current tab:
for (i = 0; i < y.length; i++) {
// If a field is empty...
if (y[i].value == "") {
// add an "invalid" class to the field:
// alert("Please Fill Column")
// y[i].className += " invalid";
// and set the current valid status to false:
valid = false;
}
}
// If the valid status is true, mark the step as finished and valid:
if (valid) {
document.getElementsByClassName("step")[currentTab].className += " finish";
}
return valid; // return the valid status
}

function fixStepIndicator(n) {
// This function removes the "active" class of all steps...
var i, x = document.getElementsByClassName("step");
for (i = 0; i < x.length; i++) {
x[i].className = x[i].className.replace(" active", "");
}
//... and adds the "active" class to the current step:
x[n].className += " active";
}




</script>





  
@endsection
