@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Add New Press</h4>
        </div>
              
         <div class="card-body">
               
         <form action="{{ url('/sellercompanypresssave') }}" method="POST" enctype="multipart/form-data" >
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="fact-upload">
            <div class="form-group">
                <input class="form-control" name="media" placeholder="Media Name...">
            </div>
            <div class="form-group">
                <input class="form-control" name="year" placeholder="Year...">
            </div>
            <div class="form-group">
                 <input class="form-control" name="link_title" placeholder="Link Title...">
            </div>
            <div class="form-group">
                 <input class="form-control" name="link" placeholder="Link...">
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-outline-primary"><i class="material-icons">save</i>&nbsp;Press</button>
            </div>
        </form>
    
        </div>

</div>

@endsection