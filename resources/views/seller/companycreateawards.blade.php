@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Add New Company Awards</h4>
        </div>
              
         <div class="card-body">
               
         <form action="{{ url('/sellercompanyawardscreatesave') }}" method="POST" enctype="multipart/form-data" >
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="gallery-upload">
                <input class="form-control" type="file" name="image" >
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-outline-primary"><i class="material-icons">save</i>&nbsp;Save Awards</button>
            </div>
        </form>
    
        </div>

</div>

@endsection