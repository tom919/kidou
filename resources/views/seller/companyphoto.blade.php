@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Company Photo</h4>
                  <p class="card-category"> company photo gallery</p>
        </div>
              
         <div class="card-body">
               
         <div class="row justify-content-center">
                        <a href="{{url('/sellercompanyphotocreate')}}"><button  type="button" class="btn btn-outline-primary pull-right"><i class="material-icons">add_a_photo</i>&nbsp;Add Photo</button></a>
        </div>
               
                <div class="card-description">
                      @foreach ($cd as $photo)
                      <div class="gallery">
                                <a data-fancybox="images" data-caption="<a href='{{ url('/sellercompanyphotodelete')}}/{{$photo->id}}'><button  type='button' class='btn btn-outline-primary');'><i class='material-icons'>delete</i>&nbsp;Delete</button></a>" href="{{url('/photo/company_gallery/')}}/{{$photo->comp_photo}}">
                                    <img class="img-gallery" src="{{url('/photo/company_gallery/')}}/{{$photo->comp_photo}}">
                                </a>
                        </div>
                        @endforeach
                   
                </div>
    
        </div>
                <div class="row justify-content-center">
                        {!! $cd->links('pagination::bootstrap-4') !!}
                </div>





</div>

<div id="myModal" class="modal fullscreen fade">
    <div class="modal-dialog">        
        <div class="modal-content">
        <div class="close-btn" data-dismiss="modal">×</div>
            <div class="modal-body">
                <!--Overlay iframe inserted here-->
            </div>            
        </div>        
    </div>
</div>

@endsection