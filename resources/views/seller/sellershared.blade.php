@extends('layouts.sharedseller')

@section('content')
<div class="section-hero-owners">
<img src="{{url('photo/company')}}/{{$co->photo_slide}}" class="slide-photo">
</div>
  <div class="section-hero-content">
    <div class="container-7 seller w-container">
      <div class="w-layout-grid grid-wrap-hero-content">
        <div class="wrap-content-hero-left">
          <div class="div-block-32">
            <ul role="list" class="nav-grid-2-copy w-list-unstyled">
              <li id="w-node-_50149efa-9451-b977-2e86-9bfd04e2cbb4-e1e27079" class="list-item-6-copy">
                <a href="#" class="nav-logo-link"><img src="{{url('photo/company')}}/{{$co->photo}}" alt="" class="nav-logo"></a>
              </li>
              <li class="listt-item-4-seller">
                <a data-w-id="50149efa-9451-b977-2e86-9bfd04e2cbb8" href="#" class="l003-button-copy w-inline-block">
                  <div data-w-id="50149efa-9451-b977-2e86-9bfd04e2cbb9" data-animation-type="lottie" data-src="{{url('/maikai')}}/documents/1087-heart.json" data-loop="0" data-direction="1" data-autoplay="0" data-is-ix2-target="1" data-renderer="svg" data-default-duration="3.1" data-duration="0" class="lottie-heart"></div>
                </a>
              </li>
              <li id="w-node-_50149efa-9451-b977-2e86-9bfd04e2cbba-e1e27079" class="list-item-5-copy">
                <a href="{{url('/login')}}" class="nav-link-2">Sign In</a>
              </li>
              <li id="w-node-_50149efa-9451-b977-2e86-9bfd04e2cbbd-e1e27079" class="list-item-3-copy">
                <a href="#" class="nav-link-2" data-toggle="modal" data-target="#regModal">Register</a>
              </li>
            </ul>
          </div>
          <h1 class="heading-section-3 h1">{{$co->company_name}}  </h1>
          <?php
                $cat = explode(',', $co->category);
                ?>
          <ul role="list" class="list sp-list">
          @foreach($cat as $ca)
            <li class="list-item">{{$ca}} </li>
            @endforeach
          </ul>
          <?php 
               $f = new NumberFormatter("en", NumberFormatter::CURRENCY);
               $ap = $f->formatCurrency($co->asking_price, "USD");
               $avs = $f->formatCurrency($co->avg_sales, "USD");
               $ar =  $f->formatCurrency($co->annual_revenue, "USD");
               $pr_start = $f->formatCurrency($co->pr_start, "USD");
               $pr_end = $f->formatCurrency($co->pr_end, "USD");
          ?>
          <p class="para-body-2 lighter"><strong>Annual Revenue:</strong>   {{$ar}}</p>
          <div class="divider full owners"></div>
          <div class="w-layout-grid grid-bottons-small phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607832eddebae43c801da3ec_icon%20location.png" loading="lazy" width="34" alt="">
            <p id="w-node-_49c3f7da-4d55-7958-5f16-fa36a2e1dc2c-e1e27079" class="para-body-2 lighter">{{$co->address}}, {{$co->city}}, {{$co->country}}</p>
          </div>
          <div class="w-layout-grid grid-bottons-small phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607832e33f6827b57ff7fe21_industry%20icon.png" loading="lazy" width="34" alt="">
            <p class="para-body-2 lighter">Industry</p>
            <?php
                $ind = explode(',', $co->industry);
                ?>
            @foreach($ind as $in)
            @if($loop->index < 3 )
            <a href="#" class="button-3 blue w-button">{{$in}}</a>
            @endif
            @endforeach
          </div>
          <div class="w-layout-grid grid-bottons-small seller"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607832e6adf8de7774a92ed6_sales%20channel%20icon.png" loading="lazy" width="34" alt="">
            <p class="para-body-2 lighter">Sales Channel</p>
            <?php
                $slc = explode(',', $co->sales_channel);
                ?>
            @foreach($slc as $sc)
            @if($loop->index < 3 )
            <a href="#" class="button-3 blue w-button">{{ $sc }}</a>
            @endif
            @endforeach  
          </div>
          <div class="w-layout-grid grid-bottons-small-copy-2"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607832ea843caa6b51f400ed_prise%20range%20icon.png" loading="lazy" width="41" alt="">
            <p class="para-body-2 lighter">Price Range</p>
            <a href="#" class="button-3 blue w-button">{{ $pr_start }} - {{ $pr_end }}</a>
          </div>
          <div class="divider full owners"></div>
          <div class="w-layout-grid grid-big-botton-hero-left seller">
            <a href="#comp-fact" class="button-bigger w-button">Show Company Facts</a>
          </div>
        </div>
        <div id="w-node-_49c3f7da-4d55-7958-5f16-fa36a2e1dc46-e1e27079" class="wrap-content-hero-right">
          <div class="share-save">
            <ul role="list" class="nav-grid-2-wrap w-list-unstyled">
              <li class="list-item-6-copy">
                <a href="#" class="nav-logo-link-copy"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60731b10c68543ed70a43511_Kidou_Black%201.png" alt="" class="nav-logo-copy"></a>
              </li>
              <li id="w-node-a95c1e12-86b5-89e7-c224-602e0db2f787-e1e27079" class="list-item-4">
                <a data-w-id="a95c1e12-86b5-89e7-c224-602e0db2f788" href="#" class="l003-button w-inline-block">
                  <div data-w-id="a95c1e12-86b5-89e7-c224-602e0db2f789" data-animation-type="lottie" data-src="{{url('/maikai')}}/documents/1087-heart.json" data-loop="0" data-direction="1" data-autoplay="0" data-is-ix2-target="1" data-renderer="svg" data-default-duration="3.1" data-duration="0" class="lottie-heart"></div>
                </a>
              </li>
              <li class="list-item-5">
                <a href="{{url('/login')}}" class="nav-link-2">Sign In</a>
              </li>
              <li class="list-item-3">
                <a href="#" class="nav-link-2" data-toggle="modal" data-target="#regModal">Register</a>
              </li>
            </ul>
          </div>
          <div class="wrap-image-content-hero">
            <!-- main image -->
            <img src="{{url('photo/company')}}/{{$co->photo_main}}" class="main-photo">
          </div>
          <div class="w-layout-grid grid-big-botton-hero-right">
            <h1 class="heading-section-3 price">{{$ap}}</h1>
            <a href="#" class="button-bigger w-button">Place an Offer</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section-body-content">
    <div class="w-container">
      <div class="w-layout-grid grid-7">
        <div class="wrap-profile-img"><img src="{{url('photo/user')}}/{{$ud->photo}}" class="cust-pp"></div>
        <div id="w-node-_6eb4c187-cbf9-cb0a-508d-fdaca934f0fc-e1e27079" class="wrap-para-body-bigger"><img src="images/Vector.png" loading="lazy" width="58" alt="" class="image-18">
          <p class="para-body-bigger"> {!! \Illuminate\Support\Str::limit($co->intro, 255, $end='...')  !!} </p>
        </div>
      </div>
    </div>
  </div>
  <div class="section-about">
    <div class="w-container">
      <div class="columns-5 w-row">
        <div class="coumn-about w-col w-col-8">
          <div class="wrap-content">
            <h2 class="heading-section-3 about">About</h2>
            <p class="para-body-2">{!! \Illuminate\Support\Str::limit($co->about, 255, $end='...') !!}</p>
            <h3 class="para-header business-model">Business Model</h3>
            <p class="para-body-2">{!! \Illuminate\Support\Str::limit($co->bussines_model, 100, $end='...') !!}</p>
            <div class="wrap-accordian-item">
              <div data-hover="" data-delay="0" class="dropdown-trigger w-dropdown">
                <div class="dropdown-toggle-4 w-dropdown-toggle">
                  <div class="accordian-icon w-icon-dropdown-toggle"></div>
                  <div>Show More </div>
                </div>
                <nav class="dropdown-list-4 w-dropdown-list">
                  <p class="para-body-2">  <?php
                    $bm_rest= substr($co->bussines_model,  -(( strlen($co->bussines_model)-1) - 100));
                  ?>
                  {!! $bm_rest !!}</p>
                </nav>
              </div>
            </div>
            <div class="divider-copy"></div>
            <div class="w-layout-grid grid-para-header"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/606d184618a40a7ca04ecec0_Vector%20(3).png" loading="lazy" alt="">
              <h3 class="para-header">Business Specialties</h3>
            </div>
            <div class="w-layout-grid grid-buttons-specialities">
            <?php
                $bsp = explode(',', $co->bussines_specialities);
                ?>
              @foreach($bsp as $bs)
              <a href="#" class="button-3 w-button">{{ $bs }}</a>
              @endforeach
            </div>
            <div class="w-layout-grid grid-para-header"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/606d1866e4ec3c37c8cbdeef_Vector%20(4).png" loading="lazy" alt="">
              <h3 class="para-header">Marketing Channel</h3>
            </div>
            <div class="w-layout-grid grid-bottons-small-marketing-channel applicable">
            <?php
                $mcl = explode(',', $co->marketing_channel);
              ?>
              @foreach($mcl as $mc)
                @if($loop->index < 4 )
                <a href="#" class="button-3 marketing-channel w-button">{{$mc}}</a>
                @endif
              @endforeach
            </div>
            <!-- <div class="w-layout-grid grid-bottons-small applicable">
              <a href="#" class="button-3 marketing-channel phone w-button">Google Ads</a>
              <a id="w-node-eaedf527-8bf7-1eab-cf26-aee1166c0e1e-e1e27079" href="#" class="button-3 marketing-channel w-button">Search Engine Optimization (SEO)</a>
            </div> -->
            <div class="w-layout-grid grid-para-header"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607850f3aecb6551c5b32b30_applicable%20icon.png" loading="lazy" width="27" alt="">
              <h3 class="para-header">Applicable Assets for Selling</h3>
            </div>
            <div class="w-layout-grid grid-bottons-small applicable phone-only">
            <?php
                $aps = explode(',', $co->applicable_assets);
              ?>
              @foreach($aps as $apa)
              <a href="#" class="button-3 blue-darker w-button">{{$apa}}</a>
              @endforeach
            </div>
          
            <div class="divider-copy"></div>
          </div>
        </div>
        <div class="column-2 w-col w-col-4">
          <div class="wrap-buyers-box">
            <div class="wrap-img-box-right-copy-2">
              <a href="#" class="link-block-3 w-inline-block"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607856167d1f210851261a5b_heart%20icon.png" loading="lazy" width="35" alt="" class="image-11"></a>
              <img src="{{url('photo/company')}}/{{$co->photo}}" loading="lazy" height="200" alt="" class="image-10 logo-avatar">
              <h2 class="heeading-2">{{$co->company_name}}</h2>
            </div>
            <div class="wrap-content-right-box">
              <div class="w-layout-grid grid-bottons-small right-box phone">
                <p class="para-bold-22 _24">
                <?php 
                  echo floor((time() - strtotime($co->establish_date)) / 31556926);
                ?>
                </p>
                <p class="para-body-2 white">Years Operational</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box phone">
                <p class="para-bold-22 _24">{{$avs}}</p>
                <p class="para-body-2 white">Avg Sale / Yr</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/606d345c43ccdd77c5203dcf_Vector%20(5).png" loading="lazy" width="27" alt="">
                <p class="para-bold-22">Industry</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box padd-20">
              <?php
                    $ind = explode(',', $co->industry);
                    ?>
                @foreach($ind as $in)
                @if($loop->index < 3 )
                <a href="#" class="button-3 blue w-button">{{$in}}</a>
                @endif
               @endforeach
              </div>
              <div class="w-layout-grid grid-bottons-small right-box phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60785f7d3f6827f2acf92fed_Vector%20(8).png" loading="lazy" width="30" alt="">
                <p class="para-bold-22">Sales Channel</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box padd-20">
                  <?php
                    $slc = explode(',', $co->sales_channel);
                    ?>
                @foreach($slc as $sc)
                @if($loop->index < 3 )
                <a href="#" class="button-3 blue w-button">{{ $sc }}</a>
                @endif
                @endforeach  
              </div>
              <div class="w-layout-grid grid-bottons-small right-box phone"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/607861ad67b962163092617e_price%20range%20blue%20icon.png" loading="lazy" width="39" alt="">
                <p class="para-bold-22">Price Range</p>
              </div>
              <div class="w-layout-grid grid-bottons-small right-box">
                <a href="#" class="button-3 blue w-button">{{ $pr_start }} - {{ $pr_end }}</a>
              </div>
            </div>
            <div class="divider-copy"></div>
            <div class="div-block-2">
              <div class="w-layout-grid grid-big-botton-hero-right inside--box-right">
                <h1 class="heading-section-3 inside-box">{{$ap}}</h1>
                <a href="#" class="button-bigger place-an-offer w-button">Place an Offer</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <?php
    $finL = count($finFact);
    $orgL = count($orgFact);
    $servL = count($servFact);
?>
  
  <div class="section-company-facts">
    <div class="w-container">
      <div class="columns-copy w-row">
        <div class="column-company-facts w-col w-col-8">
          <div class="revenue-div">
            <h2 class="heading-section-3" id="comp-fact">Company Facts</h2>
            <div class="wrap-sub-heading-company-facts">
              <p class="para-bold-22">Finance</p>
            </div>
            <div class="wrap-table owners">
              <!-- finance data loop -->
              
            @foreach($finFact as $fc)
              @if($loop->index < 3 )
              <div class="w-layout-grid grid-table ">
                <div>
                  <p class="para-body-2">{{$fc->title}}<br></p>
                  <p class="para-light-gray">{{$fc->year}}</p>
                </div>
                <p id="w-node-_4d0dfc2f-9bfb-1661-5982-fa6f7e657980-e1e27079" class="para-body-2 table">{{$fc->data}}</p>
              </div>
              <div class="divider-table"></div>
              @endif                
            @endforeach
              <!-- finance data loop -->
              <div class="w-layout-grid grid-show-more table">
                <div data-hover="" data-delay="0" class="w-dropdown">
                  <div class="dropdown-toggle-4 w-dropdown-toggle">
                    <div class="icon-5 w-icon-dropdown-toggle"></div>
                    <div>Show More </div>
                  </div>
                  <nav class="dropdown-list-6 w-dropdown-list">
                  @foreach($finFact as $fc)
                    @if($loop->index >= 3 )
                    <div class="w-layout-grid grid-table">
                      <div id="w-node-d5b105f6-3c13-0117-70e6-1162d7b4443f-e1e27079">
                        <p class="para-body-2">{{$fc->title}}<br></p>
                        <p class="para-light-gray">{{$fc->year}}</p>
                      </div>
                      <p id="w-node-d5b105f6-3c13-0117-70e6-1162d7b44445-e1e27079" class="para-body-2">  {{$fc->data}}</p>
                    </div>
                    @endif
                  @endforeach
                  </nav>
                </div>
              </div>
            </div>
          </div>
          <div class="service-details-div">
            <div class="wrap-sub-heading-company-facts">
              <p class="para-bold-22">Organization</p>
            </div>
            <div class="wrap-table">
            @foreach($orgFact as $fc)
              @if($loop->index < 3 )
              <div class="w-layout-grid grid-table">
                <div>
                  <p class="para-body-2">{{ $fc->title }}<br></p>
                  <p class="para-light-gray">{{ $fc->year }}</p>
                </div>
                <p id="w-node-_4d0dfc2f-9bfb-1661-5982-fa6f7e6579da-e1e27079" class="para-body-2 table">{{$fc->data}}</p>
              </div>
              <div class="divider-table"></div>
              @endif
            @endforeach
            @if($orgL > 3)
            <!-- show more -->
            <div data-hover="" data-delay="0" class="dropdown-trigger table w-dropdown">
                    
                    <div class="dropdown-toggle-4 w-dropdown-toggle">
                      <div class="w-icon-dropdown-toggle"></div>
                      <div class="show-more-table">Show More </div>
                    </div>  

                    <nav class="dropdown-list-5 w-dropdown-list">
                    @foreach($orgFact as $fc)
                    @if($loop->index >= 3 )
                      
                      <div class="w-layout-grid grid-table dropdown-list">

                        <div>
                          <p class="para-body-2">{{$fc->title}}<br></p>
                          <p class="para-light-gray">{{$fc->year}}</p>
                        </div>
                        <p id="w-node-_8baef9ca-532d-e69d-8c95-0512c3364ec0-e1e27079" class="para-body-2 table">
                        {{$fc->data}}
                        </p>
                      </div>
                    @endif
                  @endforeach
                    </nav>
                  
                  </div>
            <!-- show more -->
              @endif

            </div>
          </div>
          <div class="organization-div">
            <div class="wrap-sub-heading-company-facts">
              <p class="para-bold-22">Service Details</p>
            </div>
            <div class="wrap-table">
            @foreach($servFact as $sf)
              @if($loop->index < 3 )
              <div class="w-layout-grid grid-table">
                <div>
                  <p class="para-body-2">{{$sf->title}}<br></p>
                  <p class="para-light-gray">{{$sf->year}}</p>
                </div>
                <p id="w-node-_4d0dfc2f-9bfb-1661-5982-fa6f7e657a06-e1e27079" class="para-body-2 table">
                {{$sf->data}}    
              </p>
              </div>
              <div class="divider-table"></div>
              @endif
            @endforeach
            <!-- show more part -->
            @if($servL > 3)
              <div data-hover="" data-delay="0" class="dropdown-trigger table w-dropdown">
           
                <div class="dropdown-toggle-4 w-dropdown-toggle">
                  <div class="w-icon-dropdown-toggle"></div>
                  <div class="show-more-table">Show More </div>
                </div>
      

                <nav class="dropdown-list-5 w-dropdown-list">
                @foreach($servFact as $sf)
                    @if($loop->index >= 3 )
                  <div class="w-layout-grid grid-table dropdown-list">
                    <div>
                      <p class="para-body-2">{{$sf->title}}<br></p>
                      <p class="para-light-gray">{{$sf->year}}</p>
                    </div>
                    <p id="w-node-_8baef9ca-532d-e69d-8c95-0512c3364ec0-e1e27079" class="para-body-2 table">   {{$sf->data}}</p>
                  </div>
                  @endif
                  @endforeach
                </nav>
              </div>
              @endif
              <!-- show more part -->
            </div>
          </div>
          <div class="divider-copy certificates"></div>
        </div>
        <div class="column-8 w-col w-col-4">
          <div class="div-block-21">
            <div class="w-layout-grid grid-contact-us-here">
              <h2 id="w-node-_4d0dfc2f-9bfb-1661-5982-fa6f7e657a3c-e1e27079" class="heading-32">Looking to franchise <br>or grow <br>your brand?</h2>
              <div id="w-node-_4d0dfc2f-9bfb-1661-5982-fa6f7e657a42-e1e27079" class="div-block">
                <div class="link-block-contact">Contact us Here</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section-certificates">
    <div class="w-container">
      <div class="w-row">
        <div class="column-certificates w-col w-col-8">
          <div class="certificates-div">
            <h2 class="heading-section-3">Certificates</h2>
            <div class="w-layout-grid grid-certificates">
            @foreach($CC as $cs)
              @if($loop->index < 3 )
              <div class="wrap-content-certificates">
                <div class="wrap-img-certificates"><img src="{{url('photo/company_certificates/')}}/{{$cs->image}}" loading="lazy" alt="" class="certificates-img"></div>
                <div class="div-block-22">
                  <p class="para-body-2 certificates">{{$cs->title}}</p>
                </div>
              </div>
              @endif
            @endforeach
      

            </div>
            <div data-hover="" data-delay="0" class="dropdown-4 w-dropdown">

              <div class="dropdown-toggle-4 w-dropdown-toggle">
                <div class="w-icon-dropdown-toggle"></div>
                <div>Show More </div>
              </div>

              <nav class="dropdown-list-7 w-dropdown-list">
                <div class="w-layout-grid grid-certificates">
                @foreach($CC as $cs)
                    @if($loop->index >= 3 )
                  <div class="wrap-content-certificates">
                    <div class="wrap-img-certificates"><img src="{{url('photo/company_certificates/')}}/{{$cs->image}}" loading="lazy" alt="" class="certificates-img"></div>
                    <div class="div-block-22">
                      <p class="para-body-2 certificates">{{$cs->title}}</p>
                    </div>
                  </div>
                  @endif
                 @endforeach
                </div>
              </nav>
            </div>
          </div>
          <div class="divider-copy"></div>
        </div>
        <div class="column-5 w-col w-col-4"></div>
      </div>
    </div>
  </div>
  <div class="section-owner-profile">
    <div class="w-container">
      <div class="columns w-row">
        <div class="column-owner-profile w-col w-col-8">
          <div class="owner-profile">
            <div class="w-layout-grid grid-big-botton-hero-right owner-profile">
              <h1 id="w-node-d89b88a4-68d5-2bed-0d54-248fae165687-e1e27079" class="heading-section-3">Owner Profile</h1>
              <a id="w-node-d89b88a4-68d5-2bed-0d54-248fae165689-e1e27079" href="#media" class="button-bigger contact-owner w-button">Contact Owner</a>
            </div>
            <div class="w-layout-grid grid-buyer-profile">
              <div id="w-node-d89b88a4-68d5-2bed-0d54-248fae16568c-e1e27079" class="wrap-img-owner-profile">
              <img src="{{url('photo/user')}}/{{$ud->photo}}" loading="lazy" sizes="(max-width: 479px) 92vw, 100vw" 
              srcset="{{url('photo/user')}}/{{$ud->photo}} 500w, {{url('photo/user')}}/{{$ud->photo}} 800w, {{url('photo/user')}}/{{$ud->photo}} 1080w, {{url('photo/user')}}/{{$ud->photo}} 1600w, {{url('photo/user')}}/{{$ud->photo}} 4096w"
               alt="" class="image-21"></div>
              <div id="w-node-d89b88a4-68d5-2bed-0d54-248fae16568e-e1e27079">
                <div class="w-layout-grid grid-22">
                  <div id="w-node-d89b88a4-68d5-2bed-0d54-248fae165690-e1e27079" class="div-block-20"><img src="images/Vector.png" loading="lazy" width="37" alt="" class="image-20">
                    <p class="para-bold-22 black owner-name">{{$ud->first_name}}&nbsp;{{$ud->last_name}}</p>
                    <p class="para-light-gray">Over since <?php echo date('Y', strtotime($cown->owned_since)); ?></p>
                    <div class="w-layout-grid grid-bottons-small-copy owner-profile">
                      <a href="#" class="button-3 blue smaller button-4 w-button">{{ ucfirst($cown->status)}}</a>
                    </div>
                  </div>
                </div>
                <p class="para-bold-2 para-bold-16-24">{{$ud->first_name}}&nbsp;{{$ud->last_name}} – {{$cown->owner_position}} {{$co->company_name}}</p>
              </div>
            </div>
            <p class="para-body-2 dark-grey">
            <?php
                $storyLength = strlen($ud->story);
                $storypart1 = substr($ud->story, 0, 500);
                $storypart2 = substr($ud->story, 501, 999);
            ?>
            {!! $storypart1 !!}
            </p>
           
            <div class="wrap-accordian-item">
              <div data-hover="" data-delay="0" class="dropdown-trigger w-dropdown">
              @if(strlen($storypart2) != 0)
                <div class="dropdown-toggle-4 w-dropdown-toggle">
                  <div class="accordian-icon w-icon-dropdown-toggle"></div>
                  <div>Show More </div>
                </div>
              @endif
                <nav class="dropdown-lists w-dropdown-list">
                  <p class="para-body-2">{!! $storypart2 !!}</p>
                </nav>
              </div>
            </div>
          </div>
          <div class="divider-copy certificates"></div>
          <div class="wrap-profile-videos">
            <h2 class="heading-section-3">Business Profile Videos</h2>
            <div class="w-layout-grid grid-content-profile-videos">
            @foreach($cv as $v)
                    @if($loop->index < 3 )
              <div class="wrap-profile-videos-content">
                <div class="w-layout-grid grid-profile-videos-content">
                  <div id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656a9-e1e27079" class="wrap-provile-videos">
                    <div style="padding-top:56.17021276595745%" class="video w-video w-embed">
                    {!! $v->comp_video !!}
                    </div>
                  </div>
                  <div id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656b7-e1e27079" class="content-profile-videos-wraper">
                    <p class="para-body-2 business-profile-vid"><strong>{{$v->title}}<br></strong></p>
                    <!-- <p class="para-light-gray---12px">Duration 1h 15 minutes</p> -->
                  </div>
                </div>
              </div>
              @endif
            @endforeach

            </div>
            <div class="divider-copy"></div>
          </div>
          <div class="wrap-media">
            <h2 class="heading-33" id="media">Media</h2>
            <div class="w-layout-grid grid-media"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751aa9f9a78565f9c34b6c_web%20icon.png" loading="lazy" id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656c4-e1e27079" alt="" class="image-27"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751ac39d8dda1109360e7a_instagram%20icon.png" loading="lazy" id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656c5-e1e27079" alt="">
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656c6-e1e27079" class="para-bold-16-24 media">Website</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656c8-e1e27079" class="para-body-2 dark-grey media">{{$co->website}}</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656ca-e1e27079" class="para-body-2 dark-grey media">{{$co->linkedin}}</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656cc-e1e27079" class="para-body-2 dark-grey media">{{$co->instagram}}</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656ce-e1e27079" class="para-body-2 dark-grey media">{{$co->facebook}}</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656d0-e1e27079" class="para-bold-16-24 media">Instagram</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656d2-e1e27079" class="para-bold-16-24 media">LinkedIn</p>
              <p id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656d4-e1e27079" class="para-bold-16-24 media">Facebook</p><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751ab25bed464c1202d2a1_linkdIn%20icon.png" loading="lazy" id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656d6-e1e27079" alt=""><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60751abba6f68b3e95d25d1d_facebook%20icon.png" loading="lazy" id="w-node-d89b88a4-68d5-2bed-0d54-248fae1656d7-e1e27079" alt="">
            </div>
          </div>
          <div class="divider-copy certificates"></div>
          <?php 
              $cpLength = count($cp);
          ?>
          <h2 class="heading-section-3">Press ({{$cpLength}})</h2>
          <div class="w-layout-grid grid-press">
          @foreach($cp as $press)
              @if($loop->index < 3 )
            <div>
              <p class="para-bold-2"><a href="{{$press->link}}">{{$press->link_title}}</a></p>
              <p class="para-body-2 dark-grey media">{{$press->media}}, {{$press->year}}</p>
            </div>
            @endif
          @endforeach

          </div>
          <div class="wrap-accordian-item">
            <div data-hover="" data-delay="0" class="dropdown-3 w-dropdown">
            @if($cpLength >= 3 )
              <div class="dropdown-toggle-4 w-dropdown-toggle">
                <div class="w-icon-dropdown-toggle"></div>
                <div>Show More </div>
              </div>
            @endif
              <nav class="dropdown-list-3 w-dropdown-list">
                <div class="w-layout-grid grid-press">

                @foreach($cp as $press)
              @if($loop->index >= 3 )
            <div>
              <p class="para-bold-2"><a href="{{$press->link}}" target="_blank">{{$press->link_title}}</a></p>
              <p class="para-body-2 dark-grey media">{{$press->media}}, {{$press->year}}</p>
            </div>
            @endif
          @endforeach

                </div>
              </nav>
            </div>
          </div>
          <div class="wrap-future-business">
            <p class="para-bold-22">Future Business Opportunities</p>
          </div>
          <div class="div-block-24 div-block-25 wrap-table">
          <?php 
              $boLength = count($bo);
          ?>
          @foreach($bo as $bos)
              @if($loop->index >= 3 )
            <div class="w-layout-grid grid-table">
              <div class="wrap-content-future-busines">
                <p class="para-body-2">{{$bos->title}}</p>
              </div>
              <div class="wrap-content-future-busines">
                <p>{!! \Illuminate\Support\Str::limit($bos->note, 200, $end='...') !!}</p>
              </div>
            </div>
            <div class="divider-table"></div>
            @endif
          @endforeach

            <div class="wrap-accordian-item">
              <div data-hover="" data-delay="0" class="dropdown-trigger w-dropdown">
              @if($boLength >= 3 )
                <div class="dropdown-toggle-4 w-dropdown-toggle">
                  <div class="accordian-icon w-icon-dropdown-toggle"></div>
                  <div class="show-more-table">Show More </div>
                </div>
              @endif

                <nav class="dropdown-list-5 w-dropdown-list">
                <div class="divider-table"></div>
                @foreach($bo as $bos)
              @if($loop->index < 3 )
            <div class="w-layout-grid grid-table">
              <div class="wrap-content-future-busines">
                <p class="para-body-2">{{$bos->title}}</p>
              </div>
              <div class="wrap-content-future-busines">
                <p>{!! \Illuminate\Support\Str::limit($bos->note, 200, $end='...') !!}</p>
              </div>
            </div>
            <div class="divider-table"></div>
            @endif
          @endforeach
              </div>
            </div>
          </div>
          <div class="divider-copy"></div>
        </div>
        <div class="column-5 w-col w-col-4"></div>
      </div>
    </div>
  </div>
  <div class="section-footer">
    <div class="w-container">
      <div class="columns-4 w-row">
        <div class="w-col w-col-7">
          <h2 class="heading-section-3">Awards</h2>
          <div class="w-layout-grid grid-32">
          <?php 
              $awLength = count($aw);
          ?>
          @foreach($aw as $awr)
              @if($loop->index < 3 )

            <div  class="wrap-content-awards">
              <div class="wrap-awards-img"><img src="{{url('photo/company_awards/')}}/{{$awr->image}}" loading="lazy" width="143" height="95" alt=""></div>
              <div>
                <p class="para-awards">{{$awr->title}}</p>
              </div>
            </div>
            @endif
          @endforeach
          </div>
        </div>
        <div class="column-5 w-col w-col-5"></div>
      </div>
    </div>
    <div class="wrap-footer">
      <div class="w-container">
        <div class="w-row">
          <div class="column-16 w-col w-col-8">
            <div class="w-layout-grid grid-footer">
              <a href="#" class="link para-body grey">Other WBECS Programs/Brands</a>
              <a href="#" class="link para-body whtie">Terms &amp; Conditions</a>
              <a href="#" class="link para-body whtie">Privacy Policy</a>
            </div>
          </div>
          <div class="grid-footer right w-col w-col-4">
            <div class="w-layout-grid grid-footer-right">
              <a href="#" class="link para-body whtie">Twitter</a>
              <a href="#" class="link para-body whtie">Facebook</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- modal register -->
  <div class="modal mt-5" id="regModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
  
      <div class="modal-body">
       <div class="row">
          <div class="col-md-6 text-center">
            <a href="{{url('/registersseller')}}" target="_blank"><h3>Register As Seller</h3></a>
          </div>
          <div class="col-md-6 text-center">
          <a href="{{url('/registersbuyer')}}" target="_blank"><h3>Register As Buyer</h3></a>
          </div>
       </div>
      </div>
    </div>
  </div>
</div>

  <!-- modal register -->
  @endsection