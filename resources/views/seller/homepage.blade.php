@extends('layouts.homepage')

@section('content')
<body class="body-7">
<div class="section-hero-kidou-home">
    <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar-3 w-nav">
      <div class="container-13 w-container">
        <a href="{{url('/seller')}}" class="brand w-nav-brand"><img src="{{url('maikai/images/output-onlinepngtools.png')}}" loading="lazy" height="" width="176" srcset="{{url('maikai/')}}/images/output-onlinepngtools-p-500.png 500w, {{url('maikai/')}}images/output-onlinepngtools-p-800.png 800w, {{url('maikai/')}}images/output-onlinepngtools-p-1080.png 1080w, {{url('maikai/')}}images/output-onlinepngtools.png 1252w" sizes="176px" alt="" class="image-37"></a>
        <div class="w-layout-grid grid-34">
          <nav role="navigation" class="nav-left w-nav-menu">
            <a href="#" class="nav-link-10 w-nav-link">Pricing</a>
            <a href="#" class="nav-link-6 w-nav-link">Pricing</a>
          </nav>
          <nav role="navigation" id="w-node-_58237696-7897-7f29-a20f-51cb6ae0b617-51fd90cb" class="nav-right w-nav-menu">
            <a href="#" class="nav-link-8 w-nav-link">Help</a>
            <a href="{{url('/login')}}" class="nav-link-7 w-nav-link"><strong>Log in</strong></a>
            <a href="{{url('/registersseller')}}" class="button-home-page-nav w-button">Create a Profile</a>
          </nav>
        </div>
        <div class="menu-button-3 w-nav-button">
          <div class="icon-7 w-icon-nav-menu"></div>
        </div>
      </div>
    </div>
<div class="wrap-hero-img">
      <div class="w-layout-grid grid-float">
        <div id="w-node-b0a9cd39-1513-e0c0-a332-7208ac9cedc5-51fd90cb" class="wrap-cont-hero-cont">
          <div class="container-15 w-container">
            <div class="w-layout-grid grid-content-hero">
              <div id="w-node-c75cca47-7b47-838b-1232-b5546476f7eb-51fd90cb" class="wrap-content-hero-center">
                <h1 class="h1-home-page">Sell your business right</h1>
                <div class="div-block-48">
                  <p class="para-body-home-page hero">Are you looking to sell your business? KIDOU is the go-to marketplace to find a buyer for your small business. <br><br>With KIDOU, you can securely connect with hundreds of potential buyers interested in buying the micro-businesses you have created that make our lives better.</p>
                </div>
                <a href="{{url('/registersseller')}}" class="button-home-page w-button">Create a Profile</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="w-container">
      <div class="w-layout-grid grid-53">
        <div id="w-node-cdc28ffc-06ed-9065-4e7c-3bfe2323c08d-51fd90cb" class="div-block-52"><img src="{{ url('./maikai/images/pc5o78d9i-1.png')}}" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#safe-and-secure" class="nav-link-bold">Safe and Secure</a>
        </div>
        <div id="w-node-f01d52b2-dbcf-d5ec-44fe-70e8f557a69e-51fd90cb" class="div-block-51"><img src="{{ url('./maikai/images/pc5o78d9i-1.png')}}" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#empowering" class="nav-link-bold">Empowering</a>
        </div>
        <div id="w-node-acba371a-64a8-4cf7-d0d6-4d4e79812d7a-51fd90cb" class="div-block-53"><img src="'{{ url('./maikai/images/pc5o78d9i-1.png')}}" loading="lazy" width="31" sizes="31px" srcset="{{url('/')}}/maikai/images/pc5o78d9i-1-p-500.png 500w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-800.png 800w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1080.png 1080w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-1600.png 1600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2000.png 2000w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-2600.png 2600w, {{url('/')}}/maikai/images/pc5o78d9i-1-p-3200.png 3200w, {{url('/')}}/maikai/images/pc5o78d9i-1.png 5000w" alt="" class="image-39">
          <a href="#easy-set-up" class="nav-link-bold">Easy Set-Up</a>
        </div>
      </div>
    </div>
  </div>
  <section id="section-13" class="section-13">
    <div class="w-container">
      <h4 class="heading-35">Exit your startup now without hiring expensive advisors and avoid the pressure to sell to the first buyer. With Kidou, you can easily and safely pick a buyer for your business, and do so in complete anonymity. No personal information is shared until you are matched with the right buyer, giving you the best possible opportunities to sell your business right.</h4>
      <h1 class="_3456">3456+</h1>
      <p class="para-bold">Joined already</p>
      <a href="{{url('/registersseller')}}" class="button-home-page-nav w-button">Create a Profile</a>
    </div>
  </section>
  <section id="easy-set-up" class="section-15">
    <div class="w-container">
      <p class="para-body-home-page _14">EASY SET-UP</p>
      <div class="w-layout-grid grid-40">
        <div id="w-node-_4280dc1b-c71f-d939-03f2-d00b958a8327-51fd90cb">
          <h1 class="sub-heading">Easy Profile Set-Up</h1>
        </div>
        <div id="w-node-ba46db08-0398-d8da-55ce-e6861f6429c0-51fd90cb" class="div-block-39">
          <div class="w-layout-grid grid-41"><img src="{{url('./maikai/images/Ellipse-6.png')}}" loading="lazy" width="20" id="w-node-_76660855-e835-360d-d646-94f327044877-51fd90cb" alt="">
            <div>
              <div class="para-bold">Selling your business has never been so easy</div>
              <p class="para-body-home-page">With Kidou, no need to hire expensive advisor.</p>
            </div>
            <div>
              <div class="para-bold">Sign up, list your business and you&#x27;re good to go!</div>
              <p class="para-body-home-page">With Kidou, you only need a few minutes to set up your business profile and sell your business.</p>
            </div>
            <div>
              <div class="para-bold"><strong>We’re here to help.</strong></div>
              <p class="para-body-home-page">Whether you’re selling your bakery or your online apparel store, we’re here to help you every step of the way</p>
              <a href="{{url('/registersseller')}}" class="button-home-page box w-button">Create a Profile</a>
            </div><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-a892d8e2-4f3c-3899-bbec-c0d629172c5d-51fd90cb" alt=""><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-b81f12ea-24cb-2675-fffd-ec5ecf4a146a-51fd90cb" alt="">
          </div>
        </div>
      </div>
    </div>
    <div class="w-layout-grid grid-42">
      <div id="w-node-a4e1e8bb-b68b-bc8e-6e41-4c229627b3f0-51fd90cb" class="div-block-40 find-clients">
        <div class="div-block-42">
          <h1 class="_3456">3456+</h1>
          <p class="para-bold">something</p>
        </div>
      </div>
    </div>
  </section>
  <section id="safe-and-secure" class="section-16">
    <div class="w-container">
      <p class="para-body-home-page _14">SAFE AND SECURE</p>
      <div class="w-layout-grid grid-43">
        <div id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544a5-51fd90cb" class="div-block-39">
          <h1 class="sub-heading">Safe and Secure, Always</h1>
          <div class="w-layout-grid grid-41">
            <div id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544a8-51fd90cb">
              <div class="para-bold">Guaranteed Anonymity</div>
              <p class="para-body-home-page">Our safe and secure platform guarantees anonymity until you find the perfect buyer for your business.</p>
            </div>
            <div>
              <div class="para-bold">Any Questions?</div>
              <p class="para-body-home-page">Our extensive knowledge base and friendly support team are just one click away if you have any questions.</p>
            </div>
            <div id="w-node-b24f9145-e37b-1f28-3a29-b76869975b82-51fd90cb">
              <div class="para-bold"><strong>Not sure about how it works?</strong></div>
              <p class="para-body-home-page">No problem. Just get in touch with us and we can help you set up</p>
              <a href="#" class="button-home-page box w-button">Create a Profile</a>
            </div><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544b9-51fd90cb" alt=""><img src="{{url('/')}}/maikai/images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_7e018292-b12f-cf7b-8b17-af768d1544ba-51fd90cb" alt=""><img src="images/Ellipse-6.png" loading="lazy" width="20" id="w-node-ec5b7c0d-26cf-e4c6-079c-f99045f36611-51fd90cb" alt="">
          </div>
        </div>
        <div class="div-block-43">
          <div class="cards-grid-container-4-copy">
            <div id="w-node-_56f76b3d-450d-14bb-0ae5-541a4aa6e563-51fd90cb" class="div-block-41">
              <div class="cards-image-mask"></div>
              <h3 class="sub-heading">3456+</h3>
              <p class="para-bold">something</p>
            </div>
            <div id="w-node-_56f76b3d-450d-14bb-0ae5-541a4aa6e56a-51fd90cb">
              <div class="cards-image-mask right"></div>
              <h3 id="w-node-_56f76b3d-450d-14bb-0ae5-541a4aa6e56d-51fd90cb" class="sub-heading">3456+</h3>
              <p class="para-bold">something</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="empowering" class="section-15-copy">
    <div class="w-container">
      <p class="para-body-home-page _14">EMPOWERING</p>
      <div class="w-layout-grid grid-40">
        <div id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040c6-51fd90cb">
          <h1 class="sub-heading">Empower Yourself</h1>
        </div>
        <div id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040c9-51fd90cb" class="div-block-39">
          <div class="w-layout-grid grid-41"><img src="{{ url('maikai/images/Ellipse-6.png')}}" loading="lazy" width="20" id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040cb-51fd90cb" alt="">
            <div>
              <div class="para-bold"><strong>You’re in control</strong></div>
              <p class="para-body-home-page">Sell your business to the right buyer for you and get a big pay-off for your hard work.</p>
            </div>
            <div>
              <div class="para-bold"><strong>Give your business a second life</strong></div>
              <p class="para-body-home-page">Exit your business on a high note and make sure your business gets a second life with the perfect new owner.</p>
            </div>
            <div>
              <div class="para-bold"><strong>The future is yours</strong></div>
              <p class="para-body-home-page">Explore your passions, start a new career, or start another business once you sell your business with Kidou!</p>
              <a href="#" class="button-home-page box w-button">Create a Profile</a>
            </div><img src="images/Ellipse-6.png" loading="lazy" width="20" id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040dd-51fd90cb" alt=""><img src="{{ url('maikai/images/Ellipse-6.png')}}" loading="lazy" width="20" id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040de-51fd90cb" alt="">
          </div>
        </div>
      </div>
    </div>
    <div class="w-layout-grid grid-42 _2">
      <div id="w-node-_613ab5ab-bbcc-3bde-e4e3-a9216f6040e0-51fd90cb" class="div-block-40">
        <div class="div-block-42">
          <h1 class="_3456 right">3456+</h1>
          <p class="para-bold">something</p>
        </div>
      </div>
    </div>
  </section>
  <div class="section-17">
    <div class="w-container">
      <h4 class="heading-34">Exit your startup now without hiring expensive advisors and avoid the pressure to sell to the first buyer. With Kidou, you can easily and safely pick a buyer for your business, and do so in complete anonymity. No personal information is shared until you are matched with the right buyer, giving you the best possible opportunities to sell your business right.</h4>
    </div>
  </div>
  <div class="section-18">
    <div class="w-container">
      <p class="para-body-home-page _14">LATEST MATERIALS</p>
      <div class="w-layout-grid grid-44">
        <h1 id="w-node-_903a629b-782b-5802-1a29-80c0c4017c31-51fd90cb" class="sub-heading">Lorem Ipsum is simply dummy text</h1>
      </div>
      <div class="w-layout-grid grid-45">
        <div id="w-node-_625a3637-a012-21fd-50f7-ff19d0b48180-51fd90cb" class="wrap-img-galery _1"></div>
        <div id="w-node-ec3a5bf1-f293-1972-8a6b-fe734640633d-51fd90cb" class="wrap-img-galery _2"></div>
        <div id="w-node-_115950ed-e33b-3782-0cc9-d40ddaccdb94-51fd90cb" class="wrap-img-galery _3"></div>
        <div id="w-node-_3b5bd80e-4ebd-88c6-f8ad-6a1caddd38e7-51fd90cb" class="wrap-img-galery _4"></div>
      </div>
    </div>
  </div>
  <div class="section-20">
    <div class="w-container">
      <p class="para-body-home-page _14">HOW TO START</p>
      <h1 class="sub-heading">HOW TO START</h1>
      <ol role="list" class="list-3">
        <li class="how-to-start">Join KIDOU</li>
        <li class="how-to-start">Find the right buyer for you</li>
        <li class="how-to-start">Sell your business</li>
      </ol>
      <div class="w-layout-grid grid-46">
        <div id="w-node-f624a73d-23be-9124-5214-2d626f1920ef-51fd90cb" class="div-block-46">
          <h1 class="sub-heading white">Sell Your Business Now!</h1>
          <a href="{{url('/registersseller')}}" class="button-home-page box white w-button">Create a Profile</a>
        </div>
      </div>
    </div>
  </div>
  <div class="section-21">
    <div class="w-container">
      <div class="w-layout-grid grid-47">
        <div>
          <div class="w-layout-grid grid-48">
            <div class="div-block-47"><img src="{{url('maikai/images/Kidou-Visual-Branding-v1_page-0002-icon.jpg')}}" loading="lazy" width="204" sizes="204px" srcset="images/Kidou-Visual-Branding-v1_page-0002-icon-p-500.jpeg 500w, images/Kidou-Visual-Branding-v1_page-0002-icon-p-800.jpeg 800w, images/Kidou-Visual-Branding-v1_page-0002-icon-p-1080.jpeg 1080w, images/Kidou-Visual-Branding-v1_page-0002-icon.jpg 1200w" alt="" class="image-38"></div>
            <div>
              <div class="w-layout-grid grid-49">
                <div>
                  <p class="para-body-home-page">Terms &amp; Conditions</p>
                </div>
                <div>
                  <p class="para-body-home-page">Privacy &amp; Policy</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="w-node-_600839cd-ed87-37f4-84ce-78fcbebe7feb-51fd90cb">
          <div>
            <div class="w-layout-grid grid-49-copy">
              <div id="w-node-_600839cd-ed87-37f4-84ce-78fcbebe7ff1-51fd90cb">
                <p class="para-body-home-page">Instagram</p>
              </div>
              <div id="w-node-_79d50865-a004-93e5-e522-b0901371288b-51fd90cb">
                <p class="para-body-home-page">Linkedn</p>
              </div>
              <div id="w-node-_600839cd-ed87-37f4-84ce-78fcbebe7ff4-51fd90cb">
                <p class="para-body-home-page">Facebook</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection