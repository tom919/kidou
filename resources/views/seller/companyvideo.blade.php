@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Company Video</h4>
                  <p class="card-category"> company video gallery</p>
        </div>
              
         <div class="card-body">
               
         <div class="row justify-content-center">
                        <a href="{{url('/sellercompanyvideocreate')}}"><button  type="button" class="btn btn-outline-primary pull-right"><i class="material-icons">video_camera_front</i>&nbsp;Add Video</button></a>
        </div>
               
                <div class="card-description">
                        <div class="row video-group">
                        @foreach ($cv as $v)
                                <div class="col-md-4 col-sm-4 col-lg-4 mr-auto video-gallery">

                                            <div class="text-center video-container">
                                                {!! $v->comp_video !!}
                                            </div>
                                            <p class="text-center">
                                                <a href="{{url('/sellercompanyvideodelete')}}/{{$v->id}}" ><button  type="button" class="btn btn-sm btn-outline-primary"><i class="material-icons">delete</i>&nbsp;Delete</button></a>
                                            </p>

                                </div>
                        @endforeach
                        </div>
                </div>
    
        </div>
             
        <div class="row justify-content-center">
                        {!! $cv->links('pagination::bootstrap-4') !!}
                </div>




</div>

<div id="myModal" class="modal fullscreen fade">
    <div class="modal-dialog">        
        <div class="modal-content">
        <div class="close-btn" data-dismiss="modal">×</div>
            <div class="modal-body">
                <!--Overlay iframe inserted here-->
            </div>            
        </div>        
    </div>
</div>

@endsection