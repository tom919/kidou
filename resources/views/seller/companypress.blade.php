@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Company Press</h4>
        </div>
              
         <div class="card-body">
               
         <div class="row justify-content-center">
                        <a href="{{url('/sellercompanypressadd')}}"><button  type="button" class="btn btn-outline-primary pull-right"><i class="material-icons">fact_check</i>&nbsp;Add Press</button></a>
        </div>
               
                 

                <div class="row">

                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>Link Title</th>
                                <th>Link</th>
                                <th>Media</th>
                                <th>Year</th>
                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($cp as $f)
                                <tr>
                                <th>{{$loop->iteration}}</th>
                                <td> {{ $f->link_title }}</td>
                                <td> 
                                    <?php 
                                        $fl = substr($f->link, 0, 50);
                                    ?>
                                    {{ $fl }}
                                </td>
                                <td> {{ $f->media }}</td>
                                <td>{{ $f->year }}</td>
                                <td> <a href="{{url('/sellercompanypressedit')}}/{{$f->id}}" ><i class="material-icons">edit</i></a><a href="{{url('/sellercompanypressdelete')}}/{{$f->id}}" ><i class="material-icons">delete</i></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                            </table>
                        </div>

                        </div>
                   
                </div>

                <div class="row justify-content-center">
                        {!! $cp->links('pagination::bootstrap-4') !!}
                </div>
    

         





</div>

<div id="myModal" class="modal fullscreen fade">
    <div class="modal-dialog">        
        <div class="modal-content">
        <div class="close-btn" data-dismiss="modal">×</div>
            <div class="modal-body">
                <!--Overlay iframe inserted here-->
            </div>            
        </div>        
    </div>
</div>

@endsection