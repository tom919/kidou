@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Add New Company Fact</h4>
        </div>
              
         <div class="card-body">
               
         <form action="{{ url('/sellercompanyfactcreatesave') }}" method="POST" enctype="multipart/form-data" >
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="fact-upload">
            <div class="form-group">
                <input class="form-control" name="title" placeholder="Title...">
            </div>
            <div class="form-group">
                <select class="form-control" name="category">
                    <option selected disable>Choose Category...</option>
                    <option value="finance">Finance</option>
                    <option value="organization">Organization</option>
                    <option value="service">Service</option>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control" name="year" placeholder="Year...">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="data" placeholder="Data...">
                </textarea>
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-outline-primary"><i class="material-icons">save</i>&nbsp;Save Fact</button>
            </div>
        </form>
    
        </div>

</div>

@endsection