@extends('layouts.user')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


                
                @if (isset($errors) && count($errors))

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }} </li>
                    @endforeach
                </ul>

                @endif


                <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Update Company Section</h4>
                </div>
                <div class="card-body">


                <form id="updatedetail" action="{{ url('/sellercompsectionupdate') }}" method="POST" enctype="multipart/form-data" >
            


            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="cid" value="{{$cd[0]->id}}">
            <input type="hidden" name="sid" value="{{$sid}}">
            @switch($sid)
                         @case(1)
                         <p>
                            <label>Company Name:</label> 
                            <input class="form-control" type="text" name="company_name" placeholder="Company Name..." value="{{$cd[0]->company_name}}" >
                        </p>
                             <p><label>About :</label><textarea class="ckeditor form-control" name="about" placeholder="About..." >{{$cd[0]->about}}</textarea></p>

                            @break
                        @case(2)

                        <p>
                            <label>Establish Date :</label>
                            <input id="datepicker" class="form-control" type="date" data-date-format="yyyy-mm-dd" pattern="\d{4}-\d{2}-\d{2}" name="establish" placeholder="Establish Date..." value="{{$cd[0]->establish_date}}">
                        </p>
                        <p>
                            <label>Average Sales : </label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="avg_sales" placeholder="Average Sales..." value="{{$cd[0]->avg_sales}}">
                        </p>
                        <p>
                            <label>Annual Revenue : </label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="annual_revenue" placeholder="Annual Revenue..." value="{{$cd[0]->annual_revenue}}">
                        </p>
                        <p>
                            <label>Asking Price : </label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="asking_price" placeholder="Asking Price..." value="{{$cd[0]->asking_price}}" >
                        </p>
                        <p>
                            <label>Product Price Range Start :</label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="pr_start" placeholder="Product price start from..." value="{{$cd[0]->pr_start}}">
                        </p>
                        <p>
                            <label>Product Price Range End : </label>
                            <input class="form-control" type="number" min="0.00" step="0.01" name="pr_end" placeholder="Product price until..." value="{{$cd[0]->pr_end}}" ></p>
                            @break
                        @case(3)
                        <p><label>Bussines Model :</label><textarea class="ckeditor form-control" name="bussines_model" placeholder="Bussines Model..." >{{$cd[0]->bussines_model}}</textarea></p>

                            @break
                        @case(4)
                        <p>
                            <label>Category</label>
                            @php( $category = \App\Category::all())

                            <select class="form-control selectpicker" id="selCategory" name="category[]" multiple >
                            <option value="{{ $cd[0]->category }}" selected>{{$cd[0]->category}}</option>
                                    @foreach($category as $cat)
                                        <option value="{{ $cat->text }}">{{ $cat->text }}</option>
                                    @endforeach   
                            </select>
                            </p>
                        <p>
                                <label>Industry</label>
                                @php( $industry = \App\Industry::all())
                                <select class="form-control selectpicker" id="selIndustry" name="industry[]" multiple="multiple" >
                                <option value="{{ $cd[0]->industry }}" selected>{{$cd[0]->industry}}</option>
                                        @foreach($industry as $ind)
                                            <option value="{{ $ind->text }}">{{ $ind->text }}</option>
                                        @endforeach  
                                    </select>
                                
                                </p>
                                <p>
                                <label>Market Channel</label>
                                @php( $market = \App\MarketChannel::all())
                                <select class="form-control selectpicker" id="selMarket" name="marketing_channel[]" multiple="multiple" >
                                <option value="{{ $cd[0]->marketing_channel }}" selected>{{$cd[0]->marketing_channel}}</option>
                                @foreach($market as $mark)
                                            <option value="{{ $mark->text }}">{{ $mark->text }}</option>
                                        @endforeach       
                                    </select>

                                </p>
                                <p>
                                <label>Sales Channel</label>
                                @php( $sales = \App\SalesChannel::all())
                                <select class="form-control selectpicker" id="selSales" name="sales_channel[]" multiple="multiple" >
                                <option value="{{ $cd[0]->sales_channel }}" selected>{{$cd[0]->sales_channel}}</option>
                                @foreach($sales as $sls)
                                            <option value="{{ $sls->text }}">{{ $sls->text }}</option>
                                @endforeach       
                                    </select>

                                </p>
                                <p>
                                <label>Bussines Specialities</label>
                                @php( $bsp = \App\BussinesSpeciality::all())
                                <select class="form-control selectpicker" id="selBussinesspec" name="bussines_specialities[]" multiple="multiple" >
                                <option value="{{ $cd[0]->bussines_specialities }}" selected>{{$cd[0]->bussines_specialities}}</option>
                                @foreach($bsp as $bs)
                                            <option value="{{ $bs->text }}">{{ $bs->text }}</option>
                                        @endforeach   
                                    </select>
                                </p>
                                <p>
                                <label>Applicable Assets</label>
                                @php( $appassets = \App\ApplicableAssets::all())
                                <select class="form-control selectpicker" id="selApplicableassets" name="applicable_assets[]" multiple="multiple" >
                                <option value="{{ $cd[0]->applicable_assets }}" selected>{{$cd[0]->applicable_assets}}</option>
                                @foreach($appassets as $ap)
                                            <option value="{{ $ap->text }}">{{ $ap->text }}</option>
                                        @endforeach   
                                    </select>
                                </p>

                            @break
                @case(5)
                <p><label>Owner Introduction :</label><textarea class="ckeditor form-control" name="intro" placeholder="Owner Introduction..." >{{$cd[0]->intro}}</textarea></p>
                            @break
                @case(6)
                <p>
                    <label>Address :</label>
                    <textarea class="form-control" name="address" placeholder="Address..." >{{$cd[0]->address}}</textarea>
                </p>
                <p>
                    <label>City :</label>
                    <input class="form-control" type="text" name="city" placeholder="City..." value="{{$cd[0]->city}}" >
                </p>
                <p>
                    <label>Postal Code :</label>
                    <input class="form-control" type="text" name="postal_code" placeholder="Postal Code..." value="{{$cd[0]->postal_code}}">
                </p>
                <p>
                    <label>Country : </label>    
                    <input id="myCountry" class="form-control" type="text" name="country" placeholder="Country..." value="{{$cd[0]->country}}" autocomplete="off" >
                </p>
                <p>
                    <label>Phone :</label>
                    <input class="form-control" type="text" name="phone" placeholder="Phone..." value="{{$cd[0]->phone}}">
                </p>
                <p>
                    <label>Email :</label>
                    <input class="form-control" type="text" name="email" placeholder="Email..." value="{{$cd[0]->email}}">
                </p>
                <p>
                    <label>Website :</label>
                    <input class="form-control" type="text" name="website" placeholder="Website..." value="{{$cd[0]->website}}" >
                </p>
                <p>
                    <label>LinkedIn :</label>
                    <input class="form-control" type="text" name="linkedin" placeholder="Linkedin..." value="{{$cd[0]->linkedin}}">
                </p>
                <p>
                    <label>Instagram :</label>
                    <input class="form-control" type="text" name="instagram" placeholder="Instagram..." value="{{$cd[0]->instagram}}" >
                </p>
                <p>
                    <label>Facebook : </label>
                    <input class="form-control" type="phone" name="facebook" placeholder="Facebook..." value="{{$cd[0]->facebook}}">
                </p>

                    @break








            @endswitch            
            <p><button  type="submit" name="register" class="btn btn-primary pull-right">Save &nbsp;<i class="material-icons">save</i></button></p>
    






            </form>

            </div>
        </div>
          

    </div>
</div>


@endsection