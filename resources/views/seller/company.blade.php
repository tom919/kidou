@extends('layouts.user')

@section('content')
<div class="container">

<div class="row justify-content-center">
        <div class="col-md-12">

   
        <div class="card" style="background-color: #ffff !important;">
                <div class="card-header card-header-primary">
                  <h2 class="card-title">My Business</h2>
                </div>

        </div>


        @if($cd == null)
            <div class="text-md-center">
            <a href="{{url('/sellercompanycreate')}}"><button class="btn btn-light btn-lg"> <i class="material-icons">add_circle_outline</i> &nbsp; Add Business </button></a>
            </div>
        @else
        <?php 
                  
                    $ap = number_format($cd[0]->asking_price, 0);
                    $avs = number_format($cd[0]->avg_sales, 0);
                    $ar =  number_format($cd[0]->annual_revenue, 0);
                    $pr_start = number_format($cd[0]->pr_start, 0);
                    $pr_end = number_format($cd[0]->pr_end, 0);
            ?>
               <!-- card group -->
      <div class="col-md-12">
                        <div class="row justify-content-center align-self-center">
                          <!-- side 1 -->
                            <div class="col-md-7 mx-1">
                                  <!-- company information -->
                                  <div class="card">
                                    <div class="card-body">
                                  
                                        <div class="row">
                                            <div class="col-md-5 align-self-center">
                                            <h3 class="text-uppercase text-center"> {{$cd[0]->company_name}} </h3>
                                              <div class="card-photo text-center ">
                                            
                                              @if($cd[0]->photo != null)
                                                <img src="{{url('photo/company')}}/{{ $cd[0]->photo}}" style="width: 100%;"  >
                                                @else
                                                <img src="{{url('photo/material/')}}/no_image.jpg" style="width: 100%;" >
                                                @endif
                                              </div>
                                              <a href="{{ url('sellercompidentityedit') }}" style="margin-left: 25%;"><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                            <div class="col-md-6 mt-5">
                                            <div class="card-account-detail">
                                            <label>Company Status </label>
                                            <br>
                                            {{$cd[0]->status}}
                                            </div>
                                            <div class="row">
                                              <div class="card-account-detail col-md-6">
                                              <label>Register Date </label>
                                              <br>
                                              {{$cd[0]->registered_since}}
                                              </div>

                                              <div class="card-account-detail col-md-6">
                                              <label>Establish Date </label>
                                              <br>
                                              {{$cd[0]->establish_date}}
                                              </div>
                                            </div>
                                            
                                            <div class="row">
                                              <div class="card-account-detail col-md-6">
                                              <label>Asking Price </label>
                                              <br>
                                              $ {{$ap}}
                                              </div>
                                              <div class="card-account-detail col-md-6">
                                              <label>Annual Revenue </label>
                                              <br>
                                              $ {{$ar}}
                                              </div>
                                            </div>

                                           
                                                <div class="card-account-detail">
                                                  <label class="mr-3">Product Price Range </label>
                                                <br>
                                                  $ {{ $pr_start }} -  {{ $pr_end }}
                                                </div>
                                                            
                                                <div class="card-account-detail">
                                                  <label class="mr-3">Average Sales / Year </label>
                                                <br>
                                                  $ {{$avs}}
                                                </div>
                                      
                                            <div class="row mt-2 pull-right">
                                            <a href="{{ url('sellercompsectionedit/2') }}"><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- company information -->
                            <!-- owner intro  -->
    
                                <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                          <h5 class="text-center p-3"> Owner Intro</h5>
                                          <div class="p-3">
                                          {!! \Illuminate\Support\Str::limit($cd[0]->intro, 255, $end='') !!}
                                          <span id="dots">...</span>
                                        <span id="more">
                                        <?php
                                            echo substr($cd[0]->intro,  -(( strlen($cd[0]->intro)-1) - 100));
                                          ?>
                                        </span>
                                        <br>
                                        <button onclick="myFunction()" id="myBtn" class="btn btn-primary btn-sm">Read more</button>
                                            <div class="pull-right"> 
                                              <a href="{{ url('sellercompsectionedit/5') }}" ><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                          </div>
                                          </div>
                                        </div>
                              </div>
                            <!-- owner intro  -->
                            <!-- business model  -->
    
                            <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                          <h5 class="text-center p-3"> Business Model</h5>
                                          <div class="p-3">
                                          {!! \Illuminate\Support\Str::limit($cd[0]->bussines_model, 255, $end='') !!}
                                          <span id="dotscmodel">...</span>
                                        <span id="morecmodel">
                                        <?php
                                            echo substr($cd[0]->bussines_model,  -(( strlen($cd[0]->bussines_model)-1) - 100));
                                          ?>
                                        </span>
                                        <br>
                                        <button onclick="myFunctioncmodel()" id="myBtncmodel" class="btn btn-primary btn-sm">Read more</button>
                                            <div class="pull-right"> 
                                              <a href="{{ url('sellercompsectionedit/3') }}" ><i class="material-icons">edit</i> &nbsp;Update</a>
                                            </div>
                                          </div>
                                          </div>
                                        </div>
                              </div>
                            <!-- businees model  -->
                            <!-- contact  -->

                            <div class="card">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="p-1 col-md-12">
                                              <h5 class="text-left p-2"> Contact</h5>
                                                 <div class="row my-3">
                                                        <div class="col-md-6">
                                                          <p><label class="mx-2"><i class="material-icons">location_on</i></label> &nbsp; {{$cd[0]->address}} {{$cd[0]->city}} {{$cd[0]->country}}</p>
                                                          <p><label class="mx-2"><i class="material-icons">mail</i></label>&nbsp;{{$cd[0]->email}}</p> 
                                                          <p><label class="mx-2"><i class="material-icons">phone</i></label>&nbsp;{{$cd[0]->phone}}</p>
                                                        </div>

                                                        <div class="col-md-6">
                                                          <p><label class="mx-1">Website</label> &nbsp;{{$cd[0]->website}}</p>
                                                          <p><label class="mx-1">Linkedin</label> &nbsp;{{$cd[0]->linkedin}}</p>
                                                          <p><label class="mx-1">Instagram</label>&nbsp;{{$cd[0]->instagram}}</p>
                                                          <p><label class="mx-1">Facebook</label>&nbsp;{{$cd[0]->facebook}}</p>
                                                        </div>
                                                    </div>
                                                    <a href="{{ url('sellercompsectionedit/6') }}" class="pull-right" ><i class="material-icons">edit</i> &nbsp;Update</a>
                                              </div>
                                          </div>
                                        </div>
                              </div>
                            <!-- contact  -->
                            </div>
                        <!-- side 2 -->
                            <div class="col-md-4 mx-0">
                              <!-- shared -->
                              <div class="card">
                                    <div class="card-body text-center">
                                    <a href="{{url('sellershared/')}}/{{$cd[0]->id}}" target="_blank"><button class="btn btn-primary btn-sm"><i class="material-icons">share</i> &nbsp;Preview My Business</button></a>
                                    </div>
                              </div>
                              <!-- shared -->
                                  <!-- contact information -->
                                  <div class="card">
                                    <div class="card-body">
                                        <h5>General Information</h5>
                                        <div>
                                        <div class="card-account-detail">
                                          <label>Category</label>
                                          <br>
                                          {{$cd[0]->category}}
                                        </div>
                                        <div class="card-account-detail">
                                        <label>Industry</label>
                                        <br>
                                        {{$cd[0]->industry}}
                                          </div>
                                        <div class="card-account-detail">
                                        <label> Marketing Channel</label>
                                        <br>
                                        {{$cd[0]->marketing_channel}} 
                                          </div>
                                        <div class="card-account-detail">
                                        <label>Business Speciality</label>
                                        <br>
                                        {{$cd[0]->bussines_specialities}} 
                                          </div>
                                          <div class="card-account-detail">
                                        <label>Applicable Assets</label>
                                        <br>
                                        {{$cd[0]->applicable_assets}}
                                          </div>
                                        </div>
                                        <div class="mt-2">
                                        <a href="{{ url('sellercompsectionedit/4') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update</a>
                                        </div>
                                    </div>
                                </div>
                            <!-- contact information -->
                            <!-- main photo  -->
                            <div class="card">
                                    <div class="card-body">
                                        <h5>Main Photo</h5>
                                                @if($cd[0]->photo_main != null)
                                                <img src="{{url('photo/company')}}/{{ $cd[0]->photo_main}}" style="width: 100%;" class="mx-0" >
                                                @else
                                                <img src="{{url('photo/material/')}}/no_image.jpg" style="width: 100%;" >
                                                @endif
                                                <a href="{{ url('sellercompmainphotoedit') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update Main Photo</a>
                                    </div>
                            </div>
                            <!-- main photo -->
                            <!-- slide photo  -->
                            <div class="card">
                            <div class="card-body">
                                <h5>Background Photo</h5>
                                        @if($cd[0]->photo_slide != null)
                                        <img src="{{url('photo/company')}}/{{ $cd[0]->photo_slide}}" style="width: 100%;" class="mx-0" >
                                        @else
                                        <img src="{{url('photo/material/')}}/no_image.jpg" style="width: 100%;" >
                                        @endif
                                        <a href="{{ url('sellercompslidephotoedit') }}" class="pull-right"><i class="material-icons">edit</i> &nbsp;Update Background Photo</a>
                            </div>
                            </div>
                            <!-- slide  photo -->
                            <!-- other feature  -->
                            <div class="card">
                            <div class="card-body">
                                <h5>Other Feature</h5>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompanyphoto')}}" >Photo</a>
                                </div>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompanyvideo')}}">Video</a>
                                </div>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompanypress')}}">Press</a>
                                </a>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompanyfact')}}" >Fact</a>
                                </div>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompanycertificate')}}" >Certificate</a>
                                </div>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompanyawards')}}" >Awards</a>
                                </div>
                                <div class="card-account-detail">
                                  <a href="{{url('/sellercompbo')}}" >Business Oportunities</a>
                                </div>
                            </div>
                            </div>
                            <!-- other feature -->
                            </div>
                        </div>
        </div>
        @endif



            
        </div>

</div>

@endsection