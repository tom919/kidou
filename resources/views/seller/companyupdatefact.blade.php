@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Company Fact Update</h4>
        </div>
              
         <div class="card-body">
               
         <form action="{{ url('sellercompanyupdatefactsave') }}" method="POST" enctype="multipart/form-data" >
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $cfd->id }}">
            <div class="fact-upload">
            <div class="form-group">
                <input class="form-control" name="title" placeholder="Title..." value="{{ $cfd->title }}">
            </div>
            <div class="form-group">
                <select class="form-control" name="category">
                    <option selected value="{{$cfd->category}}">{{ ucfirst(trans($cfd->category)) }}</option>
                    <option value="finance">Finance</option>
                    <option value="organization">Organization</option>
                    <option value="service">Service</option>
                </select>
            </div>
            <div class="form-group">
                <input class="form-control" name="year" placeholder="Year..." value="{{$cfd->year}}">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="data" placeholder="Data...">
                    {{$cfd->data}}
                </textarea>
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-outline-primary"><i class="material-icons">save</i>&nbsp;Save Fact</button>
            </div>
        </form>
    
        </div>

</div>

@endsection