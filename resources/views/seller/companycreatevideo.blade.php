@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Add New Company Video</h4>
        </div>
              
         <div class="card-body text-center">
               
         <form action="{{ url('/sellercompanyvideocreatesave') }}" method="POST" enctype="multipart/form-data" >
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="gallery-upload">
            <div class="form-group">
            <h4>Title</h4>
            <input type="text" name="title" class="form-control my-2" placeholder="Video Title ...">
            </div>
            <div class="form-group">
            <h4>Link</h4>
                <textarea name="comp_video" class="form-control my-2" placeholder="paste your video link here.."></textarea>
            </div>
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-outline-primary"><i class="material-icons">save</i>&nbsp;Save Video</button>
            </div>
        </form>
    
        </div>

</div>

@endsection