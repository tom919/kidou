@extends('layouts.user')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">


                
                @if (isset($errors) && count($errors))

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }} </li>
                    @endforeach
                </ul>

                @endif


                <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Update Company</h4>
                </div>
                <div class="card-body">


                <form id="updatedetail" action="{{ url('/sellercompanyupdatesave') }}" method="POST" enctype="multipart/form-data" >
            


            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="cid" value="{{$cd[0]->id}}">
            <div class="tab">
            <p><input class="form-control" type="text" name="company_name" placeholder="Company Name..." value="{{$cd[0]->company_name}}" ></p>
            <p>
            <label>Category</label>
            @php( $category = \App\Category::all())

            <select class="form-control selectpicker" id="selCategory" name="category[]" multiple >
            <option value="{{ $cd[0]->category }}" selected>{{$cd[0]->category}}</option>
                     @foreach($category as $cat)
                        <option value="{{ $cat->text }}">{{ $cat->text }}</option>
                     @endforeach   
            </select>
            </p>
            <p><input id="datepicker" class="form-control" type="date" data-date-format="yyyy-mm-dd" pattern="\d{4}-\d{2}-\d{2}" name="establish" placeholder="Establish Date..." value="{{$cd[0]->establish_date}}"></p>
            </div>

            <div class="tab">
            <p><textarea class="form-control" name="address" placeholder="Address..." >{{$cd[0]->address}}</textarea></p>
            <p><input class="form-control" type="text" name="city" placeholder="City..." value="{{$cd[0]->city}}" ></p>
            <p><input class="form-control" type="text" name="postal_code" placeholder="Postal Code..." value="{{$cd[0]->postal_code}}"></p>
            <p><input id="myCountry" class="form-control" type="text" name="country" placeholder="Country..." value="{{$cd[0]->country}}" autocomplete="off" ></p>
            <p><input class="form-control" type="text" name="phone" placeholder="Phone..." value="{{$cd[0]->phone}}"></p>
            <p><input class="form-control" type="text" name="email" placeholder="Email..." value="{{$cd[0]->email}}"></p>
            </div>


            <div class="tab">
            <p><label>Owner Introduction :</label><textarea class="ckeditor form-control" name="intro" placeholder="Owner Introduction..." >{{$cd[0]->intro}}</textarea></p>
            <p><label>About :</label><textarea class="ckeditor form-control" name="about" placeholder="About..." >{{$cd[0]->about}}</textarea></p>
            <p><label>Bussines Model :</label><textarea class="ckeditor form-control" name="bussines_model" placeholder="Bussines Model..." >{{$cd[0]->bussines_model}}</textarea></p>
            </div>

            <div class="tab">
            <p><input class="form-control" type="number" min="0.00" step="0.01" name="annual_revenue" placeholder="Annual Revenue..." value="{{$cd[0]->annual_revenue}}"></p>
            <p><input class="form-control" type="number" min="0.00" step="0.01" name="asking_price" placeholder="Asking Price..." value="{{$cd[0]->asking_price}}" ></p>
            <p><input class="form-control" type="number" min="0.00" step="0.01" name="pr_start" placeholder="Product price start from..." value="{{$cd[0]->pr_start}}"></p>
            <p><input class="form-control" type="number" min="0.00" step="0.01" name="pr_end" placeholder="Product price until..." value="{{$cd[0]->pr_end}}" ></p>
            <p>
            <label>Industry</label>
            @php( $industry = \App\Industry::all())
            <select class="form-control selectpicker" id="selIndustry" name="industry[]" multiple="multiple" >
            <option value="{{ $cd[0]->industry }}" selected>{{$cd[0]->industry}}</option>
                     @foreach($industry as $ind)
                        <option value="{{ $ind->text }}">{{ $ind->text }}</option>
                     @endforeach  
                </select>
            
            </p>
            <p>
            <label>Market Channel</label>
            @php( $market = \App\MarketChannel::all())
            <select class="form-control selectpicker" id="selMarket" name="marketing_channel[]" multiple="multiple" >
            <option value="{{ $cd[0]->marketing_channel }}" selected>{{$cd[0]->marketing_channel}}</option>
            @foreach($market as $mark)
                        <option value="{{ $mark->text }}">{{ $mark->text }}</option>
                     @endforeach       
                </select>

            </p>
            <p>
            <label>Sales Channel</label>
            @php( $sales = \App\SalesChannel::all())
            <select class="form-control selectpicker" id="selSales" name="sales_channel[]" multiple="multiple" >
            <option value="{{ $cd[0]->sales_channel }}" selected>{{$cd[0]->sales_channel}}</option>
            @foreach($sales as $sls)
                        <option value="{{ $sls->text }}">{{ $sls->text }}</option>
            @endforeach       
                </select>

            </p>
            <p>
            <label>Bussines Specialities</label>
            @php( $bsp = \App\BussinesSpeciality::all())
            <select class="form-control selectpicker" id="selBussinesspec" name="bussines_specialities[]" multiple="multiple" >
            <option value="{{ $cd[0]->bussines_specialities }}" selected>{{$cd[0]->bussines_specialities}}</option>
            @foreach($bsp as $bs)
                        <option value="{{ $bs->text }}">{{ $bs->text }}</option>
                     @endforeach   
                </select>
            </p>
            <p>
            <label>Applicable Assets</label>
            @php( $appassets = \App\ApplicableAssets::all())
            <select class="form-control selectpicker" id="selApplicableassets" name="applicable_assets[]" multiple="multiple" >
            <option value="{{ $cd[0]->applicable_assets }}" selected>{{$cd[0]->applicable_assets}}</option>
            @foreach($appassets as $ap)
                        <option value="{{ $ap->text }}">{{ $ap->text }}</option>
                     @endforeach   
                </select>
            </p>
            </div>


            <div class="tab">
            <p><input class="form-control" type="text" name="website" placeholder="Website..." value="{{$cd[0]->website}}" ></p>
            <p><input class="form-control" type="text" name="linkedin" placeholder="Linkedin..." value="{{$cd[0]->linkedin}}"></p>
            <p><input class="form-control" type="text" name="instagram" placeholder="Instagram..." value="{{$cd[0]->instagram}}" ></p>
            <p><input class="form-control" type="phone" name="facebook" placeholder="Facebook..." value="{{$cd[0]->facebook}}"></p>
            </div>

            <div class="tab">
            <p>
                <label>Logo</label>
                <br>
                @if($cd[0]->photo != null)
                <img src="{{url('photo/company')}}/{{ $cd[0]->photo}}" style="max-width: 50%;"  class="shadow-lg rounded">
                @else
                <img src="{{url('photo/material/')}}/no_image.jpg" style="max-width: 50%;" class="shadow-lg rounded">
                @endif
                  <input class="form-control" type="file" name="photo" >

            </p>
            <p><button  type="submit" name="register" class="btn btn-outline-primary pull-right">Save &nbsp;<i class="material-icons">save</i></button></p>
            </div>


            <div style="overflow:auto;">
            <div style="float:right;">
            <button type="button" class="btn btn-primary" id="prevBtn" onclick="nextPrev(-1)">&nbsp;Previous</button>
            <button type="button" class="btn btn-primary" id="nextBtn" onclick="nextPrev(1)">Next &nbsp;</button>
            </div>
            </div>


            <div style="text-align:center;margin-top:40px;">
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            <span class="step"></span>
            </div>

            </form>

            </div>
        </div>
          
        </div>
    </div>
</div>






<script>



var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
// This function will display the specified tab of the form ...
var x = document.getElementsByClassName("tab");
x[n].style.display = "block";
// ... and fix the Previous/Next buttons:
if (n == 0) {
document.getElementById("prevBtn").style.display = "none";
} else {
document.getElementById("prevBtn").style.display = "inline";
}
if (n == (x.length - 1)) {

document.getElementById("nextBtn").style.display = "none";;
} else {
document.getElementById("nextBtn").style.display = "inline";
document.getElementById("nextBtn").innerHTML = "Next &nbsp;";
}
// ... and run a function that displays the correct step indicator:
fixStepIndicator(n)
}

function nextPrev(n) {
// This function will figure out which tab to display
var x = document.getElementsByClassName("tab");
// Exit the function if any field in the current tab is invalid:
if (n == 1 && !validateForm()) return false;
// Hide the current tab:
x[currentTab].style.display = "none";
// Increase or decrease the current tab by 1:
currentTab = currentTab + n;
// if you have reached the end of the form... :
if (currentTab >= x.length) {
//...the form gets submitted:
//   console.log('fire');
// document.getElementById("regForm").submit();
return true;
}
// Otherwise, display the correct tab:
showTab(currentTab);
}

function validateForm() {
// This function deals with validation of the form fields
var x, y, i, valid = true;
x = document.getElementsByClassName("tab");
y = x[currentTab].getElementsByTagName("input");
// A loop that checks every input field in the current tab:
for (i = 0; i < y.length; i++) {
// If a field is empty...
if (y[i].value == "") {
// add an "invalid" class to the field:
// alert("Please Fill Column")
// y[i].className += " invalid";
// and set the current valid status to false:
valid = true;
}
}
// If the valid status is true, mark the step as finished and valid:
if (valid) {
document.getElementsByClassName("step")[currentTab].className += " finish";
}
return valid; // return the valid status
}

function fixStepIndicator(n) {
// This function removes the "active" class of all steps...
var i, x = document.getElementsByClassName("step");
for (i = 0; i < x.length; i++) {
x[i].className = x[i].className.replace(" active", "");
}
//... and adds the "active" class to the current step:
x[n].className += " active";
}


var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

autocomplete(document.getElementById("myCountry"), countries);

autocomplete(document.getElementById("myCountry"), countries);

function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
      x[i].parentNode.removeChild(x[i]);
    }
  }
}
/*execute a function when someone clicks in the document:*/
document.addEventListener("click", function (e) {
    closeAllLists(e.target);
});
} 

</script>
@endsection