@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Add New Business Opportunities</h4>
        </div>
              
         <div class="card-body">
               
         <form action="{{ url('/sellercompbosave') }}" method="POST" enctype="multipart/form-data" >
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

            <div class="fact-upload">
            <div class="form-group">
                <input class="form-control" name="title" placeholder="Title...">
            </div>

            <div class="form-group">
                <textarea class="form-control" name="note" placeholder="Note..."></textarea>
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-primary"><i class="material-icons">save</i>&nbsp;Save</button>
            </div>
        </form>
    
        </div>

</div>

@endsection