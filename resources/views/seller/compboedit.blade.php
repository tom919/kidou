@extends('layouts.user')

@section('content')
<div>
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Edit Bussines Opportunities</h4>
        </div>
              
         <div class="card-body">
               
                    <form action="{{ url('sellercompboupdate') }}" method="POST" enctype="multipart/form-data" >
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $cfd->id }}">
                        <div class="fact-upload">
                            <div class="my-2">
                            <label><h4>Title</h4></label>
                                <input class="form-control" name="title" placeholder="Title..." value="{{ $cfd->title }}">
                            </div>

                            <div class="my-4">
                                <label><h4>Note</h4></label>
                                    <textarea class="form-control" name="note" placeholder="Note...">{{$cfd->note}}</textarea>
                        </div>
                            <div class="gallery-submit">
                            <button  type="submit" class="btn btn--primary"><i class="material-icons">save</i>&nbsp;Update</button>
                            </div>
                    </form>
            </div>
        </div>

</div>

@endsection