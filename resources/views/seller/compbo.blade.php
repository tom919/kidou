@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Company Bussines Opportunities</h4>
        </div>
              
         <div class="card-body">
               
         <div class="row justify-content-center">
                        <a href="{{url('/sellercompboadd')}}"><button  type="button" class="btn btn-primary pull-right"><i class="material-icons">add</i>&nbsp;Add Bussines Opportunities</button></a>
        </div>
               
                 
        <div class="row">

        <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                <th></th>
                <th>Title</th>
                <th>Note</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($cp as $f)
                <tr>
                <td class="col-md-1">{{$loop->iteration}}</td>
                <td class="col-md-3"> {{ $f->title }}</td>
                <td class="col-md-6"> 
                    <?php 
                        $fl = substr($f->note, 0,255);
                    ?>
                    {{ $fl }}
                </td>
                <td class="col-md-3"> <a href="{{url('/sellercompboedit')}}/{{$f->id}}" ><i class="material-icons">edit</i></a><a href="{{url('/sellercompbodelete')}}/{{$f->id}}" ><i class="material-icons">delete</i></a></td>
                </tr>
            @endforeach
            </tbody>
            </table>
        </div>

</div>

</div>

<div class="row justify-content-center">
{!! $cp->links('pagination::bootstrap-4') !!}
</div>
    

         





</div>

<div id="myModal" class="modal fullscreen fade">
    <div class="modal-dialog">        
        <div class="modal-content">
        <div class="close-btn" data-dismiss="modal">×</div>
            <div class="modal-body">
                <!--Overlay iframe inserted here-->
            </div>            
        </div>        
    </div>
</div>

@endsection