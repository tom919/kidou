@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Edit Press</h4>
        </div>
              
         <div class="card-body">
               
         <form action="{{ url('/sellercompanypressupdate') }}" method="POST">
            
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{$cp->id}}">
            <div class="fact-upload">
            <div class="form-group">
                <input class="form-control" name="media" value="{{$cp->media}}" placeholder="Media Name...">
            </div>
            <div class="form-group">
                <input class="form-control" name="year" value="{{$cp->year}}" placeholder="Year...">
            </div>
            <div class="form-group">
                 <input class="form-control" name="link_title" value="{{$cp->link_title}}" placeholder="Link Title...">
            </div>
            <div class="form-group">
                 <input class="form-control" name="link" value="{{ $cp->link }}" placeholder="Link...">
            </div>
            <div class="gallery-submit">
            <button  type="submit" class="btn btn-outline-primary"><i class="material-icons">save</i>&nbsp;Press</button>
            </div>
        </form>
    
        </div>

</div>

@endsection