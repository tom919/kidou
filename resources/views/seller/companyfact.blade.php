@extends('layouts.user')

@section('content')
<div>
        <div class="main-container">

        <div class="card card-plain">
                <div class="card-header card-header-primary">
                  <h4 class="card-title mt-0"> Company Fact</h4>
        </div>
              
         <div class="card-body">
               
         <div class="row justify-content-center">
                        <a href="{{url('/sellercompanyfactcreate')}}"><button  type="button" class="btn btn-outline-primary pull-right"><i class="material-icons">fact_check</i>&nbsp;Add Fact</button></a>
        </div>
               
                 

                <div class="row">

                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Category</th>
                                <th scope="col">Year</th>
                                <th scope="col">Data</th>
                                <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($cf as $f)
                                <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td> {{ $f->title }}</td>
                                <td> {{ $f->category }}</td>
                                <td> {{ $f->year }}</td>
                                <td>{{ $f->data }}</td>
                                <td> <a href="{{url('/sellercompanyupdatefact')}}/{{$f->id}}" ><button  type="button" class="btn btn-sm btn-outline-primary"><i class="material-icons">edit</i></button></a><a href="{{url('/sellercompanyfactdelete')}}/{{$f->id}}" ><button  type="button" class="btn btn-sm btn-outline-danger"><i class="material-icons">delete</i></button></a></td>
                                </tr>
                            @endforeach
                            </tbody>
                            </table>
                        </div>

                        </div>
                   
                </div>

                <div class="row justify-content-center">
                        {!! $cf->links('pagination::bootstrap-4') !!}
                </div>
    

         





</div>

<div id="myModal" class="modal fullscreen fade">
    <div class="modal-dialog">        
        <div class="modal-content">
        <div class="close-btn" data-dismiss="modal">×</div>
            <div class="modal-body">
                <!--Overlay iframe inserted here-->
            </div>            
        </div>        
    </div>
</div>

@endsection