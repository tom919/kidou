@extends('layouts.user')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center">

                @if (isset($errors) && count($errors))

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }} </li>
                    @endforeach
                </ul>

                @endif


<!-- test form -->

<div class="card">
    <div class="card-header card-header-primary">
      <h3 class="card-title">Update Credential</h3>
    </div>
    <div class="card-body">
      <form  method="POST" action="{{ url('/sellersavecredential') }}" method="POST">

      @csrf
        <div class="form-group row">
          <h4>
            <span class="material-icons">
            email
            </span>
              Email
          </h4>
          <input type="email" name="email" class="form-control my-2" value="{{$uc->email}}">
        </div>
        
        <div class="form-group row">
          <h4>
          <span class="material-icons">
            vpn_key
            </span>
            Password
          </h4>
          <input type="password" name="password" class="form-control my-2" >
        </div>

        <div class="form-group row">
            <h4>
          <span class="material-icons">
            vpn_key
            </span>
            Confirm Password
          </h4>
          <input type="password" name="repassword" class="form-control my-2">
        </div>
        <div class="row pull-right">
          <div class="col-md-3">
          <button type="submit" class="btn btn-outline-primary"> <i class="material-icons">save</i>&nbsp;Update</button>
          </div>
        </div>
      </form>
  </div>
</div>


         

              
            </div>
        </div>
    </div>
</div>
@endsection