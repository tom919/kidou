@extends('layouts.blank')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Master {{ __('Dashboard') }}</div>

                <div class="card-body">
                  Test Controller

                  <form action="{{url('/testproc')}}" method="post">
                                {{ csrf_field() }}
 
                                <div class="form-group">
                                    <label for="nama">Nama</label>
                                    <input class="form-control" type="text" name="nama" >
                                </div>
                  
                                <div class="form-group">
                                    <input class="btn btn-primary" type="submit" value="Proses">
                                </div>
                            </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection