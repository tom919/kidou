<!DOCTYPE html>
<html data-wf-page="60879b212148c109e1e27079" data-wf-site="604dff40fd2aff5154978dc0">
<head>
  <meta charset="utf-8">
  <title>Kidou Seller</title>
  <meta content="Kidou seller" property="og:title">
  <meta content="Kidou seller" property="twitter:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="{{ url('maikai/css/normalize.css')}}" rel="stylesheet" type="text/css">
  <link href="{{ url('maikai/css/webflow.css')}}" rel="stylesheet" type="text/css">
  <link href="{{ url('maikai/css/maikai.webflow.css')}}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Montserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic","Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="{{ url('photo/material/icon.png')}}" rel="shortcut icon" type="image/x-icon">
  <link href="{{ url('maikai/images/webclip.png')}}" rel="apple-touch-icon">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> 
  <style>
.price{
  font-size: 2rem !important;
}
.modal {
    margin-top: 20%;
}
.modal-content{
  height: 15rem;
}
.modal-content .row{
  margin-top: 2.5%;
}
.cust-pp{
  width: 40rem;
  border-radius: 5%;
  border: 0.1px grey;
}
.logo-avatar{
  border-radius: 50%;
}
.main-photo{
  height: 100%; 
  width: 100%; 
  object-fit: cover;
}
.slide-photo{
  height: 100%; 
  width: 100%; 
  object-fit: cover;
  position: absolute;
  z-index: 0;
}
  </style>
</head>
<body class="body-6">
  <header id="nav" class="sticky-nav-2">
    <nav class="container-8 w-container">
      <ul role="list" class="nav-grid-2 w-list-unstyled">
        <li id="w-node-_9c6e7821-45bd-502b-01b5-a251a5bd99c7-e1e27079" class="list-item-6">
          <a href="#" class="nav-logo-link"><img src="https://uploads-ssl.webflow.com/606293a75c192d03dcd949ff/60731b10c68543ed70a43511_Kidou_Black%201.png" alt="" class="nav-logo"></a>
        </li>
        <li class="listt-item-4-seller">
          <a data-w-id="fb4993fa-3144-974e-2c06-730c3d936f23" href="#" class="l003-button w-inline-block">
            <div data-w-id="fb4993fa-3144-974e-2c06-730c3d936f24" data-animation-type="lottie" data-src="{{ url('maikai')}}/documents/1087-heart.json" data-loop="0" data-direction="1" data-autoplay="0" data-is-ix2-target="1" data-renderer="svg" data-default-duration="3.1" data-duration="0" class="lottie-heart"></div>
          </a>
        </li>
        <li class="list-item-5-seller">
          <a href="#" class="nav-link-2">Sign In</a>
        </li>
        <li class="list-item-3-seller">
          <a href="#" class="nav-link-2">Register</a>
        </li>
      </ul>
    </nav>
  </header>
  <div class="section-nav seller">
    <div class="w-container">
      <div class="div-block-19">
        <div class="w-layout-grid grid-sec-nav">
          <a href="#" class="nav-link-second-nav-bar grey">About<br></a>
          <a href="#" class="nav-link-second-nav-bar">Corporate Services</a>
          <a href="#" class="nav-link-second-nav-bar">Seller</a>
          <a href="#" class="nav-link-second-nav-bar">Buyer</a>
          <a href="#" class="nav-link-second-nav-bar">Franchises</a>
          <a href="#" class="nav-link-second-nav-bar">Location</a>
          <a href="#" class="nav-link-second-nav-bar">Contact Us</a>
          <a href="#" class="button-3 w-button"><strong>Place an Offer</strong></a>
        </div>
        <div data-collapse="medium" data-animation="default" data-duration="400" role="banner" class="navbar tablet w-nav">
          <div class="container-10 w-container">
            <a id="w-node-_1f3ec78f-501c-20e3-b917-517ae7b58058-e1e27079" href="#" class="button-3 menu w-button"><strong>Place an Offer</strong></a>
            <nav role="navigation" class="nav-menu w-nav-menu">
              <div class="w-layout-grid grid-sec-nav-copy munu">
                <a href="#" class="nav-link-second-nav-bar grey phone">About<br></a>
                <a href="#" class="nav-link-second-nav-bar phone">Corporate Services</a>
                <a href="#" class="nav-link-second-nav-bar phone">Seller</a>
                <a href="#" class="nav-link-second-nav-bar phone">Buyer</a>
                <a href="#" class="nav-link-second-nav-bar phone">Franchises</a>
                <a href="#" class="nav-link-second-nav-bar phone">Location</a>
                <a href="#" class="nav-link-second-nav-bar phone">Contact Us</a>
              </div>
            </nav>
            <div id="w-node-_05c004df-ff35-5efc-a05c-e186f06bee01-e1e27079" class="columns-8 w-row">
              <div class="column-17 w-col w-col-6"></div>
              <div class="w-col w-col-6"></div>
            </div>
            <div id="w-node-beca01e2-e814-c5ba-aa7f-ac4062f78e28-e1e27079" class="menu-button w-nav-button">
              <div class="icon-4 w-icon-nav-menu"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @yield('content')



  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=604dff40fd2aff5154978dc0" type="text/javascript" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="{{ url('maikai/js/webflow.js')}}" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>