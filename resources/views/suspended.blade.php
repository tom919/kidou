@extends('layouts.blank')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Account Suspended</div>

                <div class="card-body">
                    Your Account has been suspended, please contact our customer services
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
