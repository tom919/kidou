<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPress extends Model
{
    //
    protected $table = 'comp_press';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'year',
        'media',
        'link_title',
        'link',
        'status',
    ];
}
