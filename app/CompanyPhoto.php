<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPhoto extends Model
{
    //
    protected $table = 'comp_photo';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'comp_photo',
        'upload_date',
        'priority',
    ];
}
