<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAwards extends Model
{
    //
    protected $table = 'awards';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'title',
        'image',
        'upload_date',
        'priority',
    ];
}
