<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BussinesSpeciality extends Model
{
    //
    protected $table = 'bussines_specialities';
    public $timestamps = false;
}
