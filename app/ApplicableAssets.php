<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplicableAssets extends Model
{
    //
    protected $table = 'applicable_assets';
    public $timestamps = false;
}
