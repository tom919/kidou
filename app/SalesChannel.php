<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesChannel extends Model
{
    //
    protected $table = 'sales_channel';
    public $timestamps = false;

    protected $fillable = [
        'id','text', 
    ];
}
