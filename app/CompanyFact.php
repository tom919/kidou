<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyFact extends Model
{
    //
    protected $table = 'comp_fact';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'title',
        'category',
        'year',
        'data',
    ];
}
