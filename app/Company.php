<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $table = 'company';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'air_id',
        'company_name',
        'category',
        'address',
        'registered_since',
        'establish_date',
        'city',
        'country',
        'postal_code',
        'email',
        'phone',
        'photo',
        'photo_slide',
        'photo_main',
        'intro',
        'about',
        'bussines_model',
        'avg_sales',
        'annual_revenue',
        'asking_price',
        'pr_start',
        'pr_end',
        'industry',
        'marketing_channel',
        'sales_channel',
        // buyer
        'b_inv_type',
        'b_mna_exp',
        'b_criteria',
        'b_investment_size_start',
        'b_investment_size_end',
        'b_expertise',
        'b_fund_source',
        // buyer
        'bussines_specialities',
        'applicable_assets',
        'website',
        'linkedin',
        'instagram',
        'facebook',
        'status',
    ];
}
