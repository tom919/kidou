<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreferedTransaction extends Model
{
    //

    protected $table = 'prefered_transaction';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'title',
        'sub_title',
        'note',
    ];
}
