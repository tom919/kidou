<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyOwned extends Model
{
    //
    protected $table = 'company_owned';
    public $timestamps = false;

    protected $fillable = [
        'ownership_id',
        'user_id',
        'comp_id',
        'owner_position',
        'owned_since',
        'status',
    ];
}
