<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketChannel extends Model
{
    //
    protected $table = 'market_channel';
    public $timestamps = false;

    protected $fillable = [
        'id','text', 
    ];
}
