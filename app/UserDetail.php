<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    //
    protected $table = 'user_detail';
    public $timestamps = false;

    protected $fillable = [
        'user_id','first_name', 'last_name',
    ];

}
