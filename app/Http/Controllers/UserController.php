<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserDetail;
use App\Http\Controllers\UtilController;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use DB;
use Mail;

class UserController extends Controller
{
    //

    private $airtable_token = "key8lP4O2mDi0XPei";
    private $airtable_url = "https://api.airtable.com/v0/appmWUP3TQoCNNWdp/";

    public function __construct(UtilController $util_controller)
    {
        $this->util_controller = $util_controller;
 
    }


    public function EmailVerification()
    {
        $ud = Auth::user();
        $userDetail = UserDetail::where('user_id','=',$ud->id)->firstOrFail();
        $data['email'] = $ud->email;
        $data['name'] = $userDetail->first_name.' '.$userDetail->last_name;
        $data['token'] = Str::random(30);

        $qr = User::where('id','=',$ud->id)->firstOrFail();
        $qr->remember_token = $data['token'];
        $qr->save();

        Mail::send('emails.verification', $data, function($message) use ($data) {

            $message->to($data['email']);

            $message->subject('Email Verification');

        });


        // return response()->json($data);
        Session::flash('msg', 'Verification Email already sent' );
        return back();
    }

    public function EmailVerificationProcess($token)

    {

        $check = DB::table('users')->where('remember_token',$token)->first();


        if(!is_null($check)){

            $user = User::find($check->id);


            if($user->email_verified_at != null){
                $msg = 'Already Verified';
                return  view("emails.verificationresponse", compact('msg'));                
                // return response()->json('Email already verified');
            }

            $user->email_verified_at = date("Y-m-d");
            $user->remember_token = null;
            $user->save();


             //DD01 Send Update here
          $updateData =  [
            'fields' => array(
                'Verify Date' =>date("Y-m-d"),
                    ),
            ];
            
        Http::withToken($this->airtable_token)->patch($this->airtable_url.'User/'.$user->air_id,$updateData);

            $msg = 'Verified Successfully';
            return  view('emails.verificationresponse', compact('msg'));
            // return response()->json('successfully');
        }

        $msg = 'Verification Link Expired';
        return  view('emails.verificationresponse', compact('msg'));
        // return response()->json('link expired');
    }


}
