<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MarketChannel;
use App\SalesChannel;
use App\BussinesSpeciality;
use App\ApplicableAssets;
use App\Industry;
use App\Category;
use App\UserDetail;
use App\Visitor;
use App\CompanyOwned;
use Auth;
use Illuminate\Support\Facades\Redis;
use Session;

class UtilController extends Controller
{
    //
    public function suspend(){
        return view('suspended');
      }
      public function unauthorized(){
        return view('403');
      }

    public function getmarketchannel(Request $request){

      $data = MarketChannel::where('text','like', '%' . $request->searchTerm . '%')->get();
      return response()->json($data);
    }

    public function getsaleschannel(Request $request){

      $data = SalesChannel::where('text','like', '%' . $request->searchTerm . '%')->get();
      return response()->json($data);
    }

    public function getbussinesspec(Request $request){

      $data = BussinesSpeciality::where('text','like', '%' . $request->searchTerm . '%')->get();
      return response()->json($data);
    }

    public function getapplicableassets(Request $request){

      $data = ApplicableAssets::where('text','like', '%' . $request->searchTerm . '%')->get();
      return response()->json($data);
    }

    public function getindustry(Request $request){

      $data = Industry::where('text','like', '%' . $request->searchTerm . '%')->get();
      return response()->json($data);
    }

    public function getcategory(Request $request){

      $data = Category::where('text','like', '%' . $request->searchTerm . '%')->get();
      return response()->json($data);
    }

    function calcProfile()
    {
        $maxPoint = 11;
        $point = 0;
        $percentage = 0;

        $ua = Auth::user();
        $ud = UserDetail::where('user_id',$ua->id)->first();

        if($ua->email != null)
        {
            $point += 1;
        }

        if($ud->photo != null)
        {
            $point += 1;
        }

        if($ud->first_name != null)
        {
            $point += 1;
        }

        if($ud->last_name != null)
        {
            $point += 1;
        }

        if($ud->address != null)
        {
            $point += 1;
        }

        if($ud->phone != null)
        {
            $point += 1;
        }

        if($ud->pob != null)
        {
            $point += 1;
        }

        if($ud->dob != null)
        {
            $point += 1;
        }

        if($ud->city != null)
        {
            $point += 1;
        }

        if($ud->country != null)
        {
            $point += 1;
        }

        if($ud->story != null)
        {
            $point += 1;
        }

        $percentage = ($point / $maxPoint) * 100;

        return $percentage;
    }

    public function VisitorCounter($coid)
    {

      // $uid = Auth::user()->id;
      // if($uid == null){
      //   $uid = "user not exists";
      // }
      $sid = session()->getId();
      $ip = request()->ip();
      $datetime = date("Y-m-d H:i:s");
      $os = request()->header('User-Agent');

      $oldSid = Visitor::where('session_id','=',$sid)->first();
      if($oldSid == null){
          Visitor::create([
            'session_id'=> $sid,
            'coid' => $coid,
            'visit_time' => $datetime,
            'ip' => $ip,
            'os' => $os,
        ]);
      }


        
      // return response()->json($oldSid);
    }

    public function VisitorDisplay()
    {
      $uid = Auth::user()->id;
      $co = CompanyOwned::where('user_id',$uid)->first();
      $visitor = Visitor::where('coid','=',$co->comp_id)->get();

      return count($visitor);
    }

}
