<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\CompanyOwned;
use App\CompanyPhoto;
use App\CompanyVideo;
use App\CompanyCertificate;
use App\CompanyAwards;
use App\CompanyFact;
use App\CompanyPress;
use App\BussinesOpportunities;
use App\User;
use App\UserDetail;
use Auth;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\UtilController;

class CompanyController extends Controller
{
    //

    private $airtable_token = "key8lP4O2mDi0XPei";
    private $airtable_url = "https://api.airtable.com/v0/appmWUP3TQoCNNWdp/";

    public function __construct(UtilController $util_controller)
    {
        $this->middleware(['auth']);
        $this->util_controller = $util_controller;
    }

    public function company(){
        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('seller.company',compact('cd'));
    }


    public function companyVideo(){
        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        $cv = CompanyVideo::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
        return view('seller.companyvideo',compact('cv'));
    }

    public function companyCertificate(){
        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        $cc = CompanyCertificate::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
        return view('seller.companycertificate',compact('cc'));
    }

    public function companyCreateCertificate(){
        return view('seller.companycreatecertificate');
    }

    public function companyCreateCertificateSave(Request $request){
        //validation
        $request->validate([
           'image' => 'required',

       ],[
           'image.required'=>'Certificate Image Required',
       ]);


       // company query

       $filepath = 'photo/company_certificates';
       $uid = Auth::user()->id;
       $co = CompanyOwned::where('user_id',$uid)->first();
 
       if($request->image != null){

           $getimageName =  date("Ymdhis").'.'.str_replace(' ', '',$co->comp_id).'.jpg';
           $image = file_get_contents($request->image);
           file_put_contents(public_path($filepath).'/'.$getimageName,$image);

           CompanyCertificate::create([
               'comp_id'=> $co->comp_id,
               'title'=> $request->title,
               'image' => $getimageName,
               'upload_date' => date("Y-m-d"),
           ]);

       }else{
           Session::flash('msg', 'error failed to upload' );

           return redirect()->route('sellercompany');
       }




   Session::flash('msg', 'Company Certificates Saved Successfully' );

   return redirect()->route('sellercompany');
}

public function companyCertificateDelete($id){

    // company query

    $cd = CompanyCertificate::where('id',$id)->first();

    $filepath = 'photo/company_certificates/';
    $oldFile = public_path($filepath).$cd->image;
    unlink($oldFile);

    $cd->delete();

Session::flash('msg', 'Company Certificate Deleted Successfully' );

return redirect()->route('homeseller');
}

// awards
public function companyAwards(){
    $uid = Auth::user()->id;
    $co = CompanyOwned::where('user_id',$uid)->first();
    $cc = CompanyAwards::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
    return view('seller.companyawards',compact('cc'));
}

public function companyCreateAwards(){
    return view('seller.companycreateawards');
}

    public function companyCreateAwardsSave(Request $request){
                //validation
                $request->validate([
                'image' => 'required',

            ],[
                'image.required'=>'Awards Image Required',
            ]);


            // company query

            $filepath = 'photo/company_awards';
            $uid = Auth::user()->id;
            $co = CompanyOwned::where('user_id',$uid)->first();

            if($request->image != null){

                $getimageName =  date("Ymdhis").'.'.str_replace(' ', '',$co->comp_id).'.jpg';
                $image = file_get_contents($request->image);
                file_put_contents(public_path($filepath).'/'.$getimageName,$image);

                CompanyAwards::create([
                    'comp_id'=> $co->comp_id,
                    'image' => $getimageName,
                    'upload_date' => date("Y-m-d"),
                ]);

            }else{
                Session::flash('msg', 'error failed to upload' );

                return redirect()->route('sellercompanyawards');
            }




            Session::flash('msg', 'Company Awards Saved Successfully' );

            return redirect('/sellercompanyawards');
    }

    public function companyAwardsDelete($id){

        
        $cd = CompanyAwards::where('id',$id)->first();

        $filepath = 'photo/company_awards/';
        $oldFile = public_path($filepath).$cd->image;
        unlink($oldFile);

        $cd->delete();

        Session::flash('msg', 'Company Awards Deleted Successfully' );

        return redirect('/sellercompanyawards');
    }

// awards

    public function companyCreateVideo(){
        return view('seller.companycreatevideo');
    }

    public function companyCreateVideoSave(Request $request){
        //validation
        $request->validate([
           'comp_video' => 'required',

       ],[
           'comp_video.required'=>'Video Link Required',
       ]);


       // company query

       $uid = Auth::user()->id;
       $co = CompanyOwned::where('user_id',$uid)->first();
 
           CompanyVideo::create([
               'comp_id'=> $co->comp_id,
               'title'=> $co->title,
               'comp_video' => preg_replace('/\\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\\>/i', '<$1$4$7>', $request->comp_video),
               'upload_date' => date("Y-m-d"),
           ]);

   Session::flash('msg', 'Company Video Saved Successfully' );

   return redirect()->route('homeseller');
}


public function companyVideoDelete($id){

    // company query

    $cv = CompanyVideo::where('id',$id)->first();

    $cv->delete();

Session::flash('msg', 'Company Video Deleted Successfully' );

return redirect()->route('homeseller');
}

    public function companyPhoto(){
        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        $cd = CompanyPhoto::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
        return view('seller.companyphoto',compact('cd'));
    }

    public function companyCreatePhoto(){
        return view('seller.companycreatephoto');
    }

    public function companyCreatePhotoSave(Request $request){
                     //validation
                     $request->validate([
                        'photo' => 'required',
        
                    ],[
                        'photo.required'=>'Photo Required',
                    ]);
        
        
                    // company query
        
                    $filepath = 'photo/company_gallery';
                    $uid = Auth::user()->id;
                    $co = CompanyOwned::where('user_id',$uid)->first();
              
                    if($request->photo != null){

                        $getimageName =  date("Ymdhis").'.'.str_replace(' ', '',$co->comp_id).'.jpg';
                        $image = file_get_contents($request->photo);
                        file_put_contents(public_path($filepath).'/'.$getimageName,$image);

                        CompanyPhoto::create([
                            'comp_id'=> $co->comp_id,
                            'comp_photo' => $getimageName,
                            'upload_date' => date("Y-m-d"),
                        ]);

                    }else{
                        Session::flash('msg', 'error failed to upload' );
        
                        return redirect()->route('homeseller');
                    }
        
            
         
        
                Session::flash('msg', 'Company Photo Saved Successfully' );
        
                return redirect()->route('homeseller');
    }

    
    public function companyPhotoDelete($id){

                // company query

                $cd = CompanyPhoto::where('id',$id)->first();

                $filepath = 'photo/company_gallery/';
                $oldFile = public_path($filepath).$cd->comp_photo;
                unlink($oldFile);

                $cd->delete();

            Session::flash('msg', 'Company Photo Deleted Successfully' );

            return redirect()->route('homeseller');
    }

    public function companyCreate(){
        return view('seller.companycreate');
    }
    

    public function companyCreateSave(Request $request)
    {

             //validation
             $request->validate([
                'company_name' => 'required',
                'category'=> 'required',
                'address'=> 'required',
                'city'=> 'required',
                'country'=> 'required',
                'postal_code' => 'required',
                'email'=> 'required',
                'phone'=> 'required',
                'establish' => 'required',
                'intro'=>'required ',
                'about' => 'required  | min: 100',
                'bussines_model'=> 'required  | min: 100',
                'avg_sales'=>'required',
                'annual_revenue'=> 'required',
                'asking_price'=> 'required',
                'pr_start'=>'required',
                'pr_end'=>'required',
                'industry'=>'required',
                'marketing_channel'=>'required',
                'sales_channel'=>'required',
                'bussines_specialities'=>'required',
                'applicable_assets'=>'required',

            ],[
                'company_name.required'=>'Company Name Required',
                'category.required'=>'Category Required',
                'address.required'=>'Address Required',
                'city.required'=>'City Required',
                'country.required'=>'Country Required',
                'postal_code.required'=>'Postal Code Required',
                'email.required'=>'Company Email Required',
                'phone.required'=>'Company Phone Required',
                'establish.required'=>'Establish Date Required',
                'intro.required'=>'Owner Introduction Required',
                'about.required'=>'About Company Required',
                'bussines_model.required'=>'Bussines Model Required',
                'avg_sales.required'=>'Average Sales Required',
                'annual_revenue.required'=>'Annual Revenue Required',
                'asking_price.required'=>'Asking Price Required',
                'pr_start.required'=>'Price range start required',
                'pr_end.required'=>'Price range end required',
                'industry.required'=>'Industry Required',
                'marketing_channel.required'=>'Marketing Channel Required',
                'sales_channel.required'=>'Sales Channel Required',
                'bussines_specialities.required'=>'Bussines Specialities Required',
                'applicable_assets.required'=>'Applicable Assets Required',
            ]);


            // company query

            $filepath = 'photo/company/';
      
            if($request->photo != null){
                $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                $image = file_get_contents($request->photo);
                file_put_contents(public_path($filepath).'/'.$getimageName,$image);
            }else{
                $getimageName = "";
            }

            // upload slide
            if($request->photo_slide != null){
                $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                $imageSlide = file_get_contents($request->photo_slide);
                file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
            }else{
                $getimageNameSlide = "";
            }

            // upload main
            if($request->photo_main != null){
                $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                $imageMain = file_get_contents($request->photo_main);
                file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
            }else{
                $getimageNameMain = "";
            }


            $comp = Company::create([
                'company_name' => $request->company_name,
                'category' => implode(',',$request->category),
                'address' => $request->address,
                'registered_since' => date("Y-m-d"),
                'establish_date'=>$request->establish,
                'city'=>$request->city,
                'country'=>$request->country,
                'postal_code'=>$request->postal_code,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'photo'=>$getimageName,
                'photo_slide'=>$getimageNameSlide,
                'photo_main'=>$getimageNameMain,
                'intro'=>$request->intro,
                'about'=>$request->about,
                'bussines_model'=>$request->bussines_model,
                'avg_sales'=>$request->avg_sales,
                'annual_revenue'=>$request->annual_revenue,
                'asking_price'=>$request->asking_price,
                'pr_start' => $request->pr_start,
                'pr_end' => $request->pr_end,
                'industry'=>implode(',',$request->industry),
                'marketing_channel'=>implode(',',$request->marketing_channel),
                'sales_channel'=>implode(',',$request->sales_channel),
                'bussines_specialities'=>implode(',',$request->bussines_specialities),
                'applicable_assets'=>implode(',',$request->applicable_assets),
                'website'=>$request->website,
                'linkedin'=>$request->linkedin,
                'instagram'=>$request->instagram,
                'facebook'=>$request->facebook,
                'status' => 'not verified',
            ]);

       

            //company owned query

            CompanyOwned::create([
                'user_id'=> Auth::user()->id,
                'comp_id' => $comp->id,
                'owner_position' => $request->owner_position,
                'owned_since' => date("Y-m-d"),
                'status' => 'suspend',
            ]);

            $user_id = Auth::user()->id;
            $ud = UserDetail::where('user_id','=',Auth::user()->id)->firstOrFail();
            //airtable api
                        // DD01 send company here
                            
                            $postData =  [
                                'fields' => array(
                                    'Company Name'=>$request->company_name,
                                    'CompanyWebId'=> (string) $comp->id,
                                    'Owner WebID'=> (string) Auth::user()->id,
                                    'Owner Status'=> 'Not Verified',
                                    'Owner Name'=> $ud->first_name,
                                    'Category'=> implode(',',$request->category),
                                    'Address'=>$request->address,
                                    'City'=>$request->city,
                                    'Country'=>$request->country,
                                    'Postal Code' =>$request->postal_code,
                                    'Email'=>$request->email,
                                    'Phone'=>$request->phone,
                                    'Register Date' => date("Y-m-d"),
                                    'Establish Date' => $request->establish,
                                    'Intro'=>$request->intro,
                                    'About' => $request->about,
                                    'Bussines Model'=>$request->bussines_model,
                                    'Avg Sales'=>$request->avg_sales,
                                    'Annual Revenue'=>$request->annual_revenue,
                                    'Asking Price'=>$request->asking_price,
                                    'Product Price Range Start' => $request->pr_start,
                                    'Product Price Range End' => $request->pr_end,
                                    'Industry'=>implode(',',$request->industry),
                                    'Marketing Channel'=>implode(',',$request->marketing_channel),
                                    'Sales Channel'=>implode(',',$request->sales_channel),
                                    'Bussines Specialities'=>implode(',',$request->bussines_specialities),
                                    'Applicable Assets'=>implode(',',$request->applicable_assets),
                                    'Website'=>$request->website,
                                    'Linkedin'=>$request->linkedin,
                                    'Instagram'=>$request->instagram,
                                    'Facebook'=>$request->facebook,
                                    'Logo' =>array([
                                        'url'=> url('photo/company/'.$getimageName),
                                    ]),
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameSlide),
                                    ]),
                                    'Photo Main' =>array([
                                        'url'=> url('photo/company/'.$getimageNameMain),
                                    ]),
                                    'Status'=>'not verified'
                                ),
                                'typecast' => true,
                            ];     

        
                            $data = Http::withToken($this->airtable_token)->post($this->airtable_url.'Seller/',$postData);
                                //getid from airtable
                                $air_id = $data['id'];
                                $company_owned = CompanyOwned::where('user_id','=',$user_id)->firstOrFail();
                                $px = Company::where('id','=',$company_owned->comp_id)->firstOrFail();
                             
                                $px->air_id = $air_id;
                                
                            $px->save();

                            //DD01

                            // DD02
                            // $ud = UserDetail::where('user_id','=',Auth::user()->id)->firstOrFail();
                            // $coData =  [
                            //     'fields' => array(
                            //         'Company Name'=>$request->company_name,
                            //         'Company Web Id'=> (string) $comp->id,
                            //         'Seller Name'=>  $ud->first_name,
                            //         'Seller Id'=> (string) Auth::user()->id,
                            //         'Status'=>'not verified'
                            //     ),
                            //     'typecast' => true,
                            // ];
                            
                            // Http::withToken(env('AIRTABLE_TOKEN'))->post(env('AIR_URL').'Company%20Owner/',$coData);


                            // DD02


 

        Session::flash('msg', 'Company Saved Successfully' );

        return redirect('/sellercompany');
    }

    public function companyUpdate(){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('seller.companyupdate',compact('cd'));
    }

    public function companyUpdateSave(Request $request)
    {

             //validation
             $request->validate([
                'company_name' => 'required',
                'category'=> 'required',
                'address'=> 'required',
                'city'=> 'required',
                'country'=> 'required',
                'postal_code' => 'required',
                'email'=> 'required',
                'phone'=> 'required',
                'establish' => 'required',
                'intro'=> 'required | min: 100',
                'about' => 'required  | min: 100',
                'bussines_model'=> 'required  | min: 100',
                'avg_sales'=>'required',
                'annual_revenue'=> 'required',
                'asking_price'=> 'required',
                'pr_start'=>'required',
                'pr_end'=>'required',
                'industry'=>'required',
                'marketing_channel'=>'required',
                'sales_channel'=>'required',
                'bussines_specialities'=>'required',
                'applicable_assets'=>'required',

            ],[
                'company_name.required'=>'Company Name Required',
                'category.required'=>'Category Required',
                'address.required'=>'Address Required',
                'city.required'=>'City Required',
                'country.required'=>'Country Required',
                'postal_code.required'=>'Postal Code Required',
                'email.required'=>'Company Email Required',
                'phone.required'=>'Company Phone Required',
                'establish.required'=>'Establish Date Required',
                'intro.required'=> 'Company Introduction Required',
                'about.required'=>'About Company Required',
                'bussines_model.required'=>'Bussines Model Required',
                'avg_sales.required'=>'Average Sales Required',
                'annual_revenue.required'=>'Annual Revenue Required',
                'asking_price.required'=>'Asking Price Required',
                'pr_start.required'=>'Price range start required',
                'pr_end.required'=>'Price range end required',
                'industry.required'=>'Industry Required',
                'marketing_channel.required'=>'Marketing Channel Required',
                'sales_channel.required'=>'Sales Channel Required',
                'bussines_specialities.required'=>'Bussines Specialities Required',
                'applicable_assets.required'=>'Applicable Assets Required',
       
            ]);


            // company query

            $filepath = 'photo/company';
      
            if($request->photo != null){
                $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                $image = file_get_contents($request->photo);
                file_put_contents(public_path($filepath).'/'.$getimageName,$image);
            }else{
                $getimageName = "";
            }

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

                $comp->company_name = $request->company_name;
                $comp->category = implode(',',$request->category);
                $comp->address  = $request->address;
                $comp->establish_date = $request->establish;
                $comp->city = $request->city;
                $comp->country = $request->country;
                $comp->postal_code = $request->postal_code;
                $comp->email = $request->email;
                $comp->phone = $request->phone;
                $comp->intro = $request->intro;
                $comp->about = $request->about;
                $comp->bussines_model = $request->bussines_model;
                $comp->avg_sales = $request->avg_sales;
                $comp->annual_revenue = $request->annual_revenue;
                $comp->asking_price = $request->asking_price;
                $comp->pr_start = $request->pr_start;
                $comp->pr_end = $request->pr_end;
                $comp->industry = implode(',',$request->industry);
                $comp->marketing_channel = implode(',',$request->marketing_channel);
                $comp->sales_channel = implode(',',$request->sales_channel);
                $comp->bussines_specialities = implode(',',$request->bussines_specialities);
                $comp->applicable_assets = implode(',',$request->applicable_assets);
                $comp->website = $request->website;
                $comp->linkedin = $request->linkedin;
                $comp->instagram = $request->instagram;
                $comp->facebook = $request->facebook;
       

            // upload photo
            if($comp->photo != null){
                if($request->hasFile('photo')){
                    $oldFile = public_path($filepath).$comp->photo;
                    unlink($oldFile);
        
                    $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                    $request->photo->move(public_path($filepath), $getimageName);
                    $comp->photo = $getimageName;
                }

            }else{
                $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                $image = file_get_contents($request->photo);
                file_put_contents(public_path($filepath).'/'.$getimageName,$image);
                $comp->photo = $getimageName;
            }

            // upload slide
            if($comp->photo_slide != null){
                if($request->hasFile('photo_slide')){
                    $oldFileSlide = public_path($filepath).$comp->photo_slide;
                    unlink($oldFileSlide);
        
                    $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                    $request->photo_slide->move(public_path($filepath), $getimageNameSlide);
                    $comp->photo_slide = $getimageNameSlide;
                }

            }else{
                $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                $imageSlide = file_get_contents($request->photo_slide);
                file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
                $comp->photo_slide = $getimageNameSlide;
            }

            // upload main
            if($comp->photo_main != null){
                if($request->hasFile('photo_main')){
                    $oldFileMain = public_path($filepath).$comp->photo_main;
                    unlink($oldFileMain);
        
                    $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                    $request->photo_main->move(public_path($filepath), $getimageNameMain);
                    $comp->photo_main = $getimageNameMain;
                }

            }else{
                $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                $imageMain = file_get_contents($request->photo_main);
                file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
                $comp->photo_main = $getimageNameMain;
            }
            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                            $updateData =  [
                                'fields' => array(
                                    'Company Name'=>$request->company_name,
                                    'Category'=> implode(',',$request->category),
                                    'Address'=>$request->address,
                                    'City'=>$request->city,
                                    'Country'=>$request->country,
                                    'Postal Code' =>$request->postal_code,
                                    'Email'=>$request->email,
                                    'Phone'=>$request->phone,
                                    'Establish Date' => $request->establish,
                                    'Intro'=> $request->intro,
                                    'About' => $request->about,
                                    'Bussines Model'=>$request->bussines_model,
                                    'Avg Sales'=>$request->avg_sales,
                                    'Annual Revenue'=>$request->annual_revenue,
                                    'Asking Price'=>$request->asking_price,
                                    'Product Price Range Start' => $request->pr_start,
                                    'Product Price Range End' => $request->pr_end,
                                    'Industry'=>implode(',',$request->industry),
                                    'Marketing Channel'=>implode(',',$request->marketing_channel),
                                    'Sales Channel'=>implode(',',$request->sales_channel),
                                    'Bussines Specialities'=>implode(',',$request->bussines_specialities),
                                    'Applicable Assets'=>implode(',',$request->applicable_assets),
                                    'Website'=>$request->website,
                                    'Linkedin'=>$request->linkedin,
                                    'Instagram'=>$request->instagram,
                                    'Facebook'=>$request->facebook,
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Logo' =>array([
                                        'url'=> url('photo/company/'.$getimageName),
                                    ]),
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameSlide),
                                    ]),
                                    'Photo Main' =>array([
                                        'url'=> url('photo/company/'.$getimageNameMain),
                                    ]),
                                ),
                                'typecast' => true,
                            ];     
 
                            $data = Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch(env('AIR_URL').'Seller/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Company Updated Successfully' );

        return redirect()->route('homeseller');
    }

    // sub module photo update
    // 1. update logo
    public function compidentityedit(){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('seller.couplogo',compact('cd'));
    }

    public function compslidephotoedit(){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('seller.coupslide',compact('cd'));
    }

    
    public function compmainphotoedit(){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('seller.coupmain',compact('cd'));
    }


    public function compidentityupdate(Request $request)
    {

             //validation
             $request->validate([

                'company_name' => 'required'

            ],[

                'company_name' => 'Company Name Required'
            ]);


            // company query

            $filepath = 'photo/company';
            $getimageName = null;
            $comp = Company::where('id','=',$request->cid)->firstOrFail();
            $comp->company_name = $request->company_name;

                if($comp->photo != null){
                    if($request->hasFile('photo')){
                        $oldFile = public_path($filepath).'/'.$comp->photo;
                      unlink($oldFile);
            
                      $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                        $request->photo->move(public_path($filepath), $getimageName);
                        $comp->photo = $getimageName;
                    }

                }else{
                    if($request->photo != null)
                    {
                        $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                        $image = file_get_contents($request->photo);
                        file_put_contents(public_path($filepath).'/'.$getimageName,$image);
                        $comp->photo = $getimageName;
                    }
                }

            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                        if($getimageName != null)
                        {
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Company Name' => (string) $request->company_name,
                                    'Logo' =>array([
                                        'url'=> url('photo/company/'.$getimageName),
                                    ]),
                                ),
                                'typecast' => true,
                            ]; 
                        }else{
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Company Name' => (string) $request->company_name,
                                ),
                                'typecast' => true,
                            ]; 
                        }    
 
                            $data = Http::withToken($this->airtable_token)->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch($this->airtable_url.'Seller/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Company Identity Update Successfully' );

        return redirect()->route('sellercompany');
    }


    public function compslidephotoupdate(Request $request)
    {

             //validation
             $request->validate([
                'photo_slide' => 'required',

            ],[
                'photo_slide.required'=>'Photo Required',
            ]);


            // company query

            $filepath = 'photo/company';

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

                if($comp->photo_slide != null){
                    if($request->hasFile('photo_slide')){
                        $oldFileSlide = public_path($filepath).'/'.$comp->photo_slide;
                      unlink($oldFileSlide);
            
                      $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                        $request->photo_slide->move(public_path($filepath), $getimageNameSlide);
                        $comp->photo_slide = $getimageNameSlide;
                    }

                }else{
                    $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                    $imageSlide = file_get_contents($request->photo_slide);
                    file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
                    $comp->photo_slide = $getimageNameSlide;
                }

            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameSlide),
                                    ]),
                                ),
                                'typecast' => true,
                            ];     
 
                            $data = Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch(env('AIR_URL').'Company/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Company Photo Background Update Successfully' );

        return redirect()->route('sellercompany');
    }


    public function compmainphotoupdate(Request $request)
    {

             //validation
             $request->validate([
                'photo_main' => 'required',

            ],[
                'photo_main.required'=>'Photo Required',
            ]);


            // company query

            $filepath = 'photo/company';
      

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

                if($comp->photo_main != null){
                    if($request->hasFile('photo_main')){
                        $oldFileMain = public_path($filepath).'/'.$comp->photo_main;
                      unlink($oldFileMain);
            
                      $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                        $request->photo_main->move(public_path($filepath), $getimageNameMain);
                        $comp->photo_main = $getimageNameMain;
                    }

                }else{
                    $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                    $imageMain = file_get_contents($request->photo_main);
                    file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
                    $comp->photo_main = $getimageNameMain;
                }

            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameMain),
                                    ]),
                                ),
                                'typecast' => true,
                            ];     
 
                            $data = Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch(env('AIR_URL').'Company/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Company Photo Background Update Successfully' );

        return redirect()->route('sellercompany');
    }




    // 2. update section 1 company status

    public function compsectionedit($sid){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('seller.coupsection')
        ->with('cd',$cd)
        ->with('sid', $sid);
    }

    public function compsectionupdate(Request $request)
    {

             

            // company query

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

            switch ($request->sid) {
                case 1:
                        // section 1
                    $comp->company_name = $request->company_name;
                    $comp->about = $request->about;
                    break;
                case 2:
                         //section 2
                    $comp->establish_date = $request->establish;
                    $comp->avg_sales = $request->avg_sales;
                    $comp->annual_revenue = $request->annual_revenue;
                    $comp->asking_price = $request->asking_price;
                    $comp->pr_start = $request->pr_start;
                    $comp->pr_end = $request->pr_end;
                    break;
                case 3:
                    //section 3
                    $comp->bussines_model = $request->bussines_model;
                    break;
                case 4:
                     //section 4
                    $comp->category = implode(',',$request->category);
                    $comp->industry = implode(',',$request->industry);
                    $comp->marketing_channel = implode(',',$request->marketing_channel);
                    $comp->sales_channel = implode(',',$request->sales_channel);
                    $comp->bussines_specialities = implode(',',$request->bussines_specialities);
                    $comp->applicable_assets = implode(',',$request->applicable_assets);
                    break;
                case 5:
                      //section 5
                    $comp->intro = $request->intro;
                    break;
                case 6:
                     //section 6
                    $comp->address  = $request->address;
                    $comp->city = $request->city;
                    $comp->country = $request->country;
                    $comp->postal_code = $request->postal_code;
                    $comp->email = $request->email;
                    $comp->phone = $request->phone;
                    $comp->website = $request->website;
                    $comp->linkedin = $request->linkedin;
                    $comp->instagram = $request->instagram;
                    $comp->facebook = $request->facebook;
                    break;
                default:
                 return Redirect::back()->withErrors(['msg', 'Section ID Not Detected']);
                    break;
            }


            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                         
                                    
            switch ($request->sid) {
                case 1:
                    $updateData =  [
                        'fields' => array(
                                            'Company Name'=>$request->company_name,
                                            'About' => $request->about,
                                            'CompanyWebId' => (string) Auth::user()->id,
                                        ),
                                        'typecast' => true,
                                    ];     
                    break;
                case 2:
                    $updateData =  [
                        'fields' => array(
                                    'Establish Date' => $request->establish,
                                    'Avg Sales'=>$request->avg_sales,
                                    'Annual Revenue'=>$request->annual_revenue,
                                    'Asking Price'=>$request->asking_price,
                                    'Product Price Range Start' => $request->pr_start,
                                    'Product Price Range End' => $request->pr_end,
                                    'CompanyWebId' => (string) Auth::user()->id,
                                ),
                                'typecast' => true,
                            ];   
                    break;
                case 3:
                    $updateData =  [
                        'fields' => array(
                                'Bussines Model'=>$request->bussines_model,
                                'CompanyWebId' => (string) Auth::user()->id,
                            ),
                            'typecast' => true,
                        ];   
                    break;
                case 4:
                            $updateData =  [
                                'fields' => array(
                                'Category'=> implode(',',$request->category),
                                'Industry'=>implode(',',$request->industry),
                                'Marketing Channel'=>implode(',',$request->marketing_channel),
                                'Sales Channel'=>implode(',',$request->sales_channel),
                                'Bussines Specialities'=>implode(',',$request->bussines_specialities),
                                'Applicable Assets'=>implode(',',$request->applicable_assets),
                                'CompanyWebId' => (string) Auth::user()->id,
                            ),
                            'typecast' => true,
                        ];   
                    break;
                case 5:
                    $updateData =  [
                        'fields' => array(
                        'Intro'=> $request->intro,
                        'CompanyWebId' => (string) Auth::user()->id,
                    ),
                    'typecast' => true,
                ];
                    break;
                case 6:
                        $updateData =  [
                            'fields' => array(
                            'Address'=>$request->address,
                            'City'=>$request->city,
                            'Country'=>$request->country,
                            'Postal Code' =>$request->postal_code,
                            'Email'=>$request->email,
                            'Phone'=>$request->phone,
                            'Website'=>$request->website,
                            'Linkedin'=>$request->linkedin,
                            'Instagram'=>$request->instagram,
                            'Facebook'=>$request->facebook,
                            'CompanyWebId' => (string) Auth::user()->id,
                        ),
                        'typecast' => true,
                    ];
                    break;
                    default:
                    return Redirect::back()->withErrors(['msg', 'Section ID Airtable Not Detected']);

            }
                                  
                                 

                                  
 
                            Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch(env('AIR_URL').'Seller/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Company Updated Successfully' );

        return redirect()->route('sellercompany');
    }

       
        // company fact 
        public function companyFact(){
            $uid = Auth::user()->id;
            $co = CompanyOwned::where('user_id',$uid)->first();
            $cf = CompanyFact::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
            return view('seller.companyfact',compact('cf'));
        }

        public function companyCreateFact(){
            return view('seller.companycreatefact');
        }


        public function companyCreateFactSave(Request $request){
            //validation
            $request->validate([
               'title' => 'required',
               'category'=>'required',
               'year'=>'required',
               'data'=>'required'
           ],[
               'title.required'=>'Title Required',
               'category.required'=> 'Category Required',
               'year.required'=>'Year Required',
               'data.required'=>'Data Required',
           ]);
        
           // company query
           $uid = Auth::user()->id;
           $co = CompanyOwned::where('user_id',$uid)->first();
        
               CompanyFact::create([
                   'comp_id'=> $co->comp_id,
                   'title' => $request->title,
                   'category' => $request->category,
                   'year'=> $request->year,
                   'data'=>$request->data,
               ]);
        

        Session::flash('msg', 'Company Fact '.$request->title.' Saved Successfully' );
        
        return redirect()->route('sellercompanyfact');
        }


        public function companyUpdateFact($id){
            $cfd = CompanyFact::where('id',$id)->first();
           
            return view('seller.companyupdatefact',compact('cfd'));
        }


        public function companyUpdateFactSave(Request $request)
        {
    
                 //validation
                 $request->validate([
                    'title' => 'required',
                    'category'=>'required',
                    'year'=>'required',
                    'data'=>'required'
                ],[
                    'title.required'=>'Title Required',
                    'category.required'=> 'Category Required',
                    'year.required'=>'Year Required',
                    'data.required'=>'Data Required',
                ]);
    
    
                // company query
    
    
                $fct = CompanyFact::where('id','=',$request->id)->firstOrFail();
    
                    $fct->title = $request->title;
                    $fct->category = $request->category;
                    $fct->year  = $request->year;
                    $fct->data = $request->data;
    
                $fct->save();
    
            Session::flash('msg', 'Company Fact Updated Successfully' );
    
            return redirect()->route('sellercompanyfact');
        }

        public function companyFactDelete($id){

            // company query

            $fd = CompanyFact::where('id',$id)->first();

            $fd->delete();

        Session::flash('msg', 'Company Fact Deleted Successfully' );

        return redirect()->route('sellercompanyfact');
}


        // company fact


       

        public function companypress(){
            $uid = Auth::user()->id;
            $co = CompanyOwned::where('user_id',$uid)->first();
            $cp = CompanyPress::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
            return view('seller.companypress',compact('cp'));
        }

        public function companypressadd(){
            return view('seller.companycreatepress');
        }


        public function companypresssave(Request $request){
            //validation
            $request->validate([
               'year' => 'required',
               'media'=>'required',
               'link_title'=> 'required',
               'link'=>'required'
           ],[
               'year.required'=>'Year Required',
               'media.required'=> 'Media Required',
               'link_title.required' => 'Link Title Required',
               'link.required'=>'Link Required',
           ]);
        
           // company query
           $uid = Auth::user()->id;
           $co = CompanyOwned::where('user_id',$uid)->first();
        
               CompanyPress::create([
                   'comp_id'=> $co->comp_id,
                   'year' => $request->year,
                   'media' => $request->media,
                   'link_title' => $request->link_title,
                   'link'=> $request->link,
                   'status'=> 'publish',
               ]);
        

        Session::flash('msg', 'Company Press '.$request->link_title.' Saved Successfully' );
        
        return redirect()->route('sellercompanypress');
        }

        public function companypressedit($id){
            $cp = CompanyPress::where('id',$id)->first();
           
            return view('seller.companypressedit',compact('cp'));
        }


        public function companypressupdate(Request $request)
        {
    
                 //validation
                 $request->validate([
                    'year' => 'required',
                    'media'=>'required',
                    'link_title'=> 'required',
                    'link'=>'required'
                ],[
                    'year.required'=>'Year Required',
                    'media.required'=> 'Media Required',
                    'link_title.required' => 'Link Title Required',
                    'link.required'=>'Link Required',
                ]);
    
    
                // company query
    
    
                $fp = CompanyPress::where('id','=',$request->id)->firstOrFail();
    
                $fp->year = $request->year;
                $fp->media = $request->media;
                $fp->link_title = $request->link_title;
                $fp->link = $request->link;
                $fp->status = 'publish';
    
                $fp->save();
    
            Session::flash('msg', 'Company Press Updated Successfully' );
    
            return redirect()->route('sellercompanypress');
        }


        public function companypressdelete($id)
        {

            // company query

            $cp = companypress::where('id',$id)->first();

            $cp->delete();

        Session::flash('msg', 'Company Press Deleted Successfully' );

        return redirect()->route('sellercompanypress');
        }

        // bussines opportunities

        
        public function compbo(){
            $uid = Auth::user()->id;
            $co = CompanyOwned::where('user_id',$uid)->first();
            $cp = BussinesOpportunities::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
            return view('seller.compbo',compact('cp'));
     }

        public function compboadd(){
            return view('seller.compboadd');
        }

        public function compbosave(Request $request){
            //validation
            $request->validate([
               'title' => 'required',
               'note'=>'required',
           ],[
               'title.required'=>'Title Required',
               'note.required'=> 'Note Required',
           ]);
        
           // company query
           $uid = Auth::user()->id;
           $co = CompanyOwned::where('user_id',$uid)->first();
        
               BussinesOpportunities::create([
                   'comp_id'=> $co->comp_id,
                   'title' => $request->title,
                   'note' => $request->note,
               ]);
        

        Session::flash('msg', 'Company Opportunities '.$request->title.' Saved Successfully' );
        
        return redirect()->route('sellercompbo');
        }

        public function compboedit($id){
            $cfd = BussinesOpportunities::where('id',$id)->first();
           
            return view('seller.compboedit',compact('cfd'));
        }

        public function compboupdate(Request $request)
        {
    
                 //validation
                 $request->validate([
                    'title' => 'required',
                    'note'=>'required',
                ],[
                    'title.required'=>'Title Required',
                    'note.required'=> 'Note Required',
                ]);
    
    
                // company query
    
    
                $fct = BussinesOpportunities::where('id','=',$request->id)->firstOrFail();
    
                    $fct->title = $request->title;
                    $fct->note = $request->note;
    
                $fct->save();
    
            Session::flash('msg', 'Bussines Opportunity Updated Successfully' );
    
            return redirect()->route('sellercompbo');
        }

        
        public function compbodelete($id)
        {

            // company query

            $cp = BussinesOpportunities::where('id',$id)->first();

            $cp->delete();

            Session::flash('msg', 'Bussines Opportunity Deleted Successfully' );

            return redirect()->route('sellercompbo');
        }



         // API Section

         public function updateCompanyAPI(Request $request){


            //validation
            $request->validate([
                'company_name' => 'required',
                'category'=> 'required',
                'address'=> 'required',
                'city'=> 'required',
                'country'=> 'required',
                'postal_code' => 'required',
                'email'=> 'required',
                'phone'=> 'required',
                'establish' => 'required',
                'intro'=> 'required | min: 100',
                'about' => 'required | min: 100',
                'bussines_model'=> 'required | min: 100',
                'avg_sales'=>'required',
                'annual_revenue'=> 'required',
                'asking_price'=> 'required',
                'pr_start'=>'required',
                'pr_end'=>'required',
                'industry'=>'required',
                'marketing_channel'=>'required',
                'sales_channel'=>'required',
                'bussines_specialities'=>'required',
                'applicable_assets'=>'required',
                // 'status.required'=>'required',
         ],[
            'company_name.required'=>'Company Name Required',
            'category.required'=>'Category Required',
            'address.required'=>'Address Required',
            'city.required'=>'City Required',
            'country.required'=>'Country Required',
            'postal_code.required'=>'Postal Code Required',
            'email.required'=>'Company Email Required',
            'phone.required'=>'Company Phone Required',
            'establish.required'=>'Establish Date Required',
            'intro.required'=>'Owner Introduction Required',
            'about.required'=>'About Company Required',
            'bussines_model.required'=>'Bussines Model Required',
            'avg_sales.required'=>'Average Sales Required',
            'annual_revenue.required'=>'Annual Revenue Required',
            'asking_price.required'=>'Asking Price Required',
            'pr_start.required'=>'Price range start required',
            'pr_end.required'=>'Price range end required',
            'industry.required'=>'Industry Required',
            'marketing_channel.required'=>'Marketing Channel Required',
            'sales_channel.required'=>'Sales Channel Required',
            'bussines_specialities.required'=>'Bussines Specialities Required',
            'applicable_assets.required'=>'Applicable Assets Required',
            // 'status.required'=>'Status Required',
         ]);





        $comp = Company::where('id','=',$request->webId)->firstOrFail();

        $comp->company_name = $request->company_name;
        $comp->category = $request->category;
        $comp->address  = $request->address;
        $comp->registered_since = date("Y-m-d");
        $comp->establish_date = $request->establish;
        $comp->city = $request->city;
        $comp->country = $request->country;
        $comp->postal_code = $request->postal_code;
        $comp->email = $request->email;
        $comp->phone = $request->phone;
        $comp->intro = $request->intro;
        $comp->about = $request->about;
        $comp->bussines_model = $request->bussines_model;
        $comp->avg_sales = $request->avg_sales;
        $comp->annual_revenue = $request->annual_revenue;
        $comp->asking_price = $request->asking_price;
        $comp->pr_start = $request->pr_start;
        $comp->pr_end = $request->pr_end;
        $comp->industry = $request->industry;
        $comp->marketing_channel = $request->marketing_channel;
        $comp->sales_channel = $request->sales_channel;
        $comp->bussines_specialities = $request->bussines_specialities;
        $comp->applicable_assets = $request->applicable_assets;
        $comp->website = $request->website;
        $comp->linkedin = $request->linkedin;
        $comp->instagram = $request->instagram;
        $comp->facebook = $request->facebook;
        $comp->status = $request->status;


        $filepath = 'photo/company/';
        if($comp->photo != null){
            if($request->hasFile('photo')){
                $oldFile = public_path($filepath).$comp->photo;
              unlink($oldFile);
    
              $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                $request->photo->move(public_path($filepath), $getimageName);
                $comp->photo = $getimageName;
            }

        }else{
            $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
            $image = file_get_contents($request->photo);
            file_put_contents(public_path($filepath).'/'.$getimageName,$image);
            $comp->photo = $getimageName;
        }


        // upload slide
        if($comp->photo_slide != null){
        if($request->hasFile('photo_slide')){
            $oldFileSlide = public_path($filepath).$comp->photo_slide;
            unlink($oldFileSlide);

            $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
            $request->photo_slide->move(public_path($filepath), $getimageNameSlide);
            $comp->photo_slide = $getimageNameSlide;
        }

        }else{
            $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
            $imageSlide = file_get_contents($request->photo_slide);
            file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
            $comp->photo_slide = $getimageNameSlide;
        }

        // upload main
        if($comp->photo_main != null){
            if($request->hasFile('photo_main')){
                $oldFileMain = public_path($filepath).$comp->photo_main;
                unlink($oldFileMain);
    
                $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                $request->photo_main->move(public_path($filepath), $getimageNameMain);
                $comp->photo_main = $getimageNameMain;
            }

        }else{
            $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
            $imageMain = file_get_contents($request->photo_main);
            file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
            $comp->photo_main = $getimageNameMain;
        }


        
        $res = $comp->save();



            return response()->json($res);
        }



        public function deleteCompanyAPI(Request $request)
        {


                //validation
                $request->validate([
                    'webId' => 'required',
                ],[
                    'webId.required'=>'Web ID Required',
                ]);

                // should check transaction first !!!


                 // should check transaction first !!!

                $co = CompanyOwned::where('comp_id','=',$request->webId)->firstOrFail();
                                //Delete sub data
                                

                                $cf = CompanyFact::where('comp_id',$co->comp_id)->get();
                                $cf->delete();

                                $cpr = CompanyPress::where('comp_id',$co->comp_id)->get();
                                $cpr->delete();

                                $cph = CompanyPhoto::where('comp_id',$co->comp_id)->get();
                                $cphLength = count($cph);
                                $filepath_gallery = 'photo/company_gallery/';
                                for($i = 0 ; $i<$cphLength;$i++)
                                {
                                    if($cph[$i]->comp_photo != null)
                                    {
                                        unlink(public_path($filepath_gallery).$cph[$i]->comp_photo);
                                    }
                                }
                                $cph->delete();

                                $cvd = CompanyVideo::where('comp_id',$co->comp_id)->get();
                                $cvd->delete();


                                $crt = CompanyCertificate::where('comp_id',$co->comp_id)->get();
                                $crtLength = count($crt);
                                $filepath_certificates = 'photo/company_certificates/';
                                for($i = 0 ; $i<$crtLength;$i++)
                                {
                                    if($crt[$i]->image != null)
                                    {
                                        unlink(public_path($filepath_certificates).$crt[$i]->image);
                                    }
                                }
                                $crt->delete();

                                $caw = CompanyAwards::where('comp_id',$co->comp_id)->get();
                                $cawLength = count($caw);
                                $filepath_awards = 'photo/company_awards/';
                                for($i = 0 ; $i<$cawLength;$i++)
                                {
                                    if($caw[$i]->image != null)
                                    {
                                        unlink(public_path($filepath_awards).$caw[$i]->image);
                                    }
                                }
                                $caw->delete();

                                $cbo = BussinesOpportunities::where('comp_id',$co->comp_id)->get();
                                $cbo->delete();

                                //Delete sub data


                $co->delete();


                $post = Company::where('id','=',$request->webId)->firstOrFail();


                $filepath = 'photo/company/';
                if($post->photo != null){
                        $oldFile = public_path($filepath).$post->photo;
                        unlink($oldFile);
                }

                if($post->photo_slide != null){
                    $oldFileSlide = public_path($filepath).$post->photo_slide;
                    unlink($oldFileSlide);
                }

                if($post->photo_main != null){
                    $oldFileMain = public_path($filepath).$post->photo_main;
                    unlink($oldFileMain);
                }

                $res = $post->delete();


                return response()->json($res);
        }



}
