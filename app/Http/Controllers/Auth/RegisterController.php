<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserDetail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    private $airtable_token = "key8lP4O2mDi0XPei";
    private $airtable_url = "https://api.airtable.com/v0/appmWUP3TQoCNNWdp/";


    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $max_string = "max:255";
        return Validator::make($data, [
            'first_name' => ['required', 'string', $max_string],
            'last_name' => ['required', 'string', $max_string],
            'email' => ['required', 'string', 'email', $max_string, 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'email' => $data['email'],
            'user_type' => $data['user_type'],
            'password' => Hash::make($data['password']),
            'status' => 'not verified',
        ]);



        UserDetail::create([
            'user_id' => $user->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'status' => 'not verified',
        ]);

        // DD01 send user here
        
        switch($data['user_type']){
            case 3: $userType = 'Seller';
                break;
            case 4: $userType = 'Buyer';
                break;
            default: $userType = 'Staff';
                break;
        }
        
        $postData =  [
            'fields' => array(
                'Email'=>$data['email'],
                'Password'=> Hash::make($data['password']),
                'Type'=>$userType,
                'Register Date' => date("Y-m-d"),
                'First Name'=> $data['first_name'],
                'Last Name' => $data['last_name'],
                'UserWebId' => (string) $user->id,
                'Status'=>'not verified'
            ),
            'typecast' => true,
     ];     
 
     
     $data = Http::withToken($this->airtable_token)->post($this->airtable_url.'User',$postData);
    //getid from airtable
    $air_id = $data['id'];
    // dd($air_id);

    $post = User::where('id','=',$user->id)->firstOrFail();
    $post->air_id = $air_id;
    
   $post->save();

        //DD01

        return $user;

    }

    public function showBuyerForm()
    {
        return view('auth/registerbuyer');
    }
    public function showSellerForm()
    {
        return view('auth/registerseller');
    }
}
