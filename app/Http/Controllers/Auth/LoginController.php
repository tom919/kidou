<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;


    public function redirectTo() {
        $role = Auth::user()->user_type; 

        if(Auth::user()->status == 'suspend'){
          return '/suspend';
        }

        switch ($role) {
          case 0:
            return '/home';
            break;
          case 3:
            return '/homeseller';
            break;
        case 4:
            return '/homebuyer';
            break; 
      
          default:
            return '/unauthorized'; 
          break;
        }
      }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)

    {   

        $input = $request->all();

  

        $this->validate($request, [

            'email' => 'required',
            'password' => 'required',
            'secode' => 'required',
            'seccode' => 'required|same:secode',
        ],[
            'email.required' => 'Email Required',
            'password.required' => 'Password Required',
            'seccode.required' => 'Captcha Required',
            'seccode.same' => 'Captcha Not Same'    
        ]);

  

        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'email';

        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'])))

        {

            return redirect()->route('home');

        }else{
            Session::flash('notification', 'Wrong Username Or Password' );
            return redirect()->route('login');

        }

          

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }


}
