<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Hash;
use App\Company;
use App\CompanyOwned;
use App\CompanyPhoto;
use App\CompanyVideo;
use App\CompanyCertificate;
use App\CompanyAwards;
use App\CompanyFact;
use App\CompanyPress;
use App\BussinesOpportunities;
use App\User;
use App\UserDetail;
use Auth;
use Storage;
use Session;

class TestController extends Controller
{


    private $url = 'https://api.airtable.com/v0/appmWUP3TQoCNNWdp/User';
    private $token = 'key8lP4O2mDi0XPei';

    public function testGetList()
    {

        // sample req: http://localhost/kidou/public/testgetlist

        $data = Http::withToken($this->token)->get($this->url);
        return response()->json($data->json());
    }

    public function testGetListDetail($id)
    {
        // sample req : http://localhost/kidou/public/testgetlist?id=recvUob3C6rSErPRF

        $data = Http::withToken($this->token)->get($this->url,['/user/'.$id]);
        return response()->json($data->json());
    }

    public function testPostData(){

        $postData =  [
            'fields' => array(
                'Email'=>'test@email.com',
                'Password'=> Hash::make('qwerty'),
                'Type'=>'Seller',
                'Date Of Birth'=>'1986-05-25',
                'Photo' =>array([
                      'url'=> url('photo/user/20210406023221.thomasbudhi.jpg'),
                ]),
                'Status'=>'Active'
            ),
            'typecast' => true,
     ];     

        $data = Http::withToken($this->token)->post($this->url,$postData);

        return response()->json($data->json());
        // return response()->json($postData);
    }


    public function testUpdateData(){

        $postData =  [
            'fields' => array(
                'Email'=>'test@email.com',
            ),
     ];     


        $data = Http::withToken($this->token)->patch($this->url.'/recvUob3C6rSErPRF',$postData);

        return response()->json($data->json());
        // return response()->json($postData);
    }

    public function testDeleteData(){
        
        $data = Http::withToken($this->token)->delete($this->url.'/rectg0iIFq6gJTVhJ');

        return response()->json($data->json());
        // return response()->json($postData);
    }

    public function home(){
        return response()->json('here is home');
    }


    
    // public function jump()
    // {
    //     // $user = Auth::user();
    //     $co = CompanyOwned::where('user_id', 40)->first();
    //     $cph = CompanyPhoto::where('comp_id',$co->comp_id)->get();
    //     $dataLength = count($cph);
    //     $filepath_gallery = 'photo/company_gallery/';
    //     for($i = 0 ; $i<$dataLength;$i++)
    //     {
    //         if($cph[$i]->comp_photo != null)
    //         {
    //             unlink(public_path($filepath_gallery).$cph[$i]->comp_photo);
    //         }
    //     }
    //     return response()->json('success');
    // }

    public function showLayout(){
        return view('test');
    }
}
