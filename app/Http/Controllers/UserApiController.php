<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use App\UserDetail;
use App\Http\Controllers\UtilController;
use App\User;
use Illuminate\Support\Facades\Hash;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;

class UserApiController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);        $user->save();        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);        
        
        $credentials = request(['email', 'password']);       
         if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);        
            $user = $request->user();        
            $tokenResult = $user->createToken('Personal Access Token');
                 $token = $tokenResult->token;      
          if ($request->remember_me)
            $token->expires_at = Carbon::now()->addDays(1);      
              $token->save();        
              return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
        
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();        
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

        // API Section

        public function updateDetailAPI(Request $requests){


            //validation
            $requests->validate([
             'webId' => 'required',
             'first_name' => 'required',
             'last_name' => 'required',
             'pob' => 'required',
             'dob' => 'required',
             'city' =>'required',
             'country' => 'required',
             'phone'=> 'required',
             'story' => 'required',
             'status'=> 'required'
         ],[
             'uid.required'=>'User ID Required',
             'first_name.required'=>'First Name Required',
             'last_name.required'=>'Last Name Required',
             'pob.required'=>'Place of Birth Required',
             'dob.required'=>'Date of Birth Required',
             'city.required'=>'City Required',
             'country.required'=>'Country Required',
             'phone.required' => 'Phone Required',
             'story.required' => 'Story Required',
             'status.required' => 'Status Required'
         ]);


         $postCredent = User::where('id','=',$requests->webId)->firstOrFail();
         $postCredent->email = $requests->email;
         if($requests->password == $postCredent->password){
             $postCredent->password = $requests->password ;
         } else {
             $postCredent->password = Hash::make($requests->password);
         }
        
         $postCredent->created_at = $requests->register_date;
         $postCredent->email_verified_at = $requests->verify_date;
         $postCredent->status = $requests->status;
        $postCredent->save();


     $post = UserDetail::where('user_id','=',$requests->webId)->firstOrFail();

     $post->first_name = $requests->first_name;
     $post->last_name = $requests->last_name;
     $post->pob = $requests->pob;
     $post->dob = $requests->dob;
     $post->address = $requests->address;
     $post->city = $requests->city;
     $post->country = $requests->country;
     $post->phone = $requests->phone;
     $post->story = $requests->story;

     $filepath = 'photo/user/';
     if($post->photo != null){
         if($requests->hasFile('photo')){
             $oldFile = public_path($filepath).$post->photo;
           unlink($oldFile);
 
             $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $requests->first_name).'.jpg';
             $image = file_get_contents($requests->photo);
             file_put_contents(public_path($filepath).'/'.$getimageName,$image);

             $post->photo = $getimageName;
        }
     }else{
         $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $requests->first_name).'.jpg';
         $image = file_get_contents($requests->photo);
         file_put_contents(public_path($filepath).'/'.$getimageName,$image);

         $post->photo = $getimageName;
     }

    
    $res = $post->save();



    
    //DD01 Send Update Password here
 //    $postData =  [
 //     'fields' => array(
 //         'Password' =>$postCredent->password,
 //             ),
 //     ];     


 //     $data = Http::withToken(env('AIRTABLE_TOKEN', null))->patch(env('USER_URL', null).$postCredent->air_id,$postData);

    //DD01



     return response()->json($res);
 }


 public function deleteUserAPI(Request $requests){


              //validation
             $requests->validate([
                 'webId' => 'required',
             ],[
                 'webId.required'=>'Web ID Required',
             ]);


             $postCredent = User::where('id','=',$requests->webId)->firstOrFail();
             $postCredent->delete();


         $post = UserDetail::where('user_id','=',$requests->webId)->firstOrFail();


         $filepath = 'photo/user/';
         if($post->photo != null){
                 $oldFile = public_path($filepath).$post->photo;
                 unlink($oldFile);
         }


         $res = $post->delete();


         return response()->json($res);
         }
}