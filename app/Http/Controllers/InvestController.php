<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\CompanyOwned;
use App\CompanyPhoto;
use App\CompanyVideo;
use App\CompanyCertificate;
use App\CompanyAwards;
use App\CompanyFact;
use App\CompanyPress;
use App\BussinesOpportunities;
use App\PreferedTransaction;
use App\User;
use App\UserDetail;
use Auth;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;


class InvestController extends Controller
{
    //
    private $airtable_token = "key8lP4O2mDi0XPei";
    private $airtable_url = "https://api.airtable.com/v0/appmWUP3TQoCNNWdp/";

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function company(){
        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        $ud = UserDetail::where('user_id','=', $uid)->firstOrFail();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();

        }else{
            $cd = '';
        }
        
        return view('buyer.invest',compact('cd'))
        ->with(compact('co'))
        ->with(compact('ud'));
    }

    public function companyCreate(){
        return view('buyer.companycreate');
    }

    public function companySave(Request $request)
    {

             //validation
             $request->validate([
                'company_name' => 'required',
                'address'=> 'required',
                'city'=> 'required',
                'country'=> 'required',
                'postal_code' => 'required',
                'email'=> 'required',
                'phone'=> 'required',
                'establish' => 'required',
                'intro'=>'required  | min: 100',
                'about' => 'required  | min: 100',
                'industry'=>'required',
                'b_inv_type' => 'required',
                'b_mna_exp' => 'required',
                'b_criteria' => 'required',
                'b_investment_size_start' => 'required',
                'b_investment_size_end' => 'required',
                'b_expertise' => 'required',
                'b_fund_source' => 'required',

            ],[
                'company_name.required'=>'Company Name Required',
                'address.required'=>'Address Required',
                'city.required'=>'City Required',
                'country.required'=>'Country Required',
                'postal_code.required'=>'Postal Code Required',
                'email.required'=>'Company Email Required',
                'phone.required'=>'Company Phone Required',
                'establish.required'=>'Establish Date Required',
                'intro.required'=>'Owner Introduction Required',
                'about.required'=>'About Company Required',
                'industry.required'=>'Industry Required',
                'b_inv_type' => 'Investment Type Required',
                'b_mna_exp' => 'Merger and Acqusition Experience Required',
                'b_criteria' => 'Criteria Required',
                'b_investment_size_start' => 'Investment Size Required',
                'b_investment_size_end' => 'Investment Size Required',
                'b_expertise' => 'Expertise Required ',
                'b_fund_source' => 'Fund Source Required',
            ]);


            // company query

            $filepath = 'photo/company/';
      
            if($request->photo != null){
                $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                $image = file_get_contents($request->photo);
                file_put_contents(public_path($filepath).'/'.$getimageName,$image);
            }else{
                $getimageName = "";
            }

            // upload slide
            if($request->photo_slide != null){
                $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                $imageSlide = file_get_contents($request->photo_slide);
                file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
            }else{
                $getimageNameSlide = "";
            }

            // upload main
            if($request->photo_main != null){
                $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                $imageMain = file_get_contents($request->photo_main);
                file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
            }else{
                $getimageNameMain = "";
            }


            $comp = Company::create([
                'company_name' => $request->company_name,
                'address' => $request->address,
                'registered_since' => date("Y-m-d"),
                'establish_date'=>$request->establish,
                'city'=>$request->city,
                'country'=>$request->country,
                'postal_code'=>$request->postal_code,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'photo'=>$getimageName,
                'photo_slide'=>$getimageNameSlide,
                'photo_main'=>$getimageNameMain,
                'intro'=>$request->intro,
                'about'=>$request->about,
                'b_inv_type' => $request->b_inv_type,
                'b_mna_exp' => $request->b_mna_exp,
                'b_criteria' => implode(',',$request->b_criteria),
                'b_investment_size_start' => $request->b_investment_size_start,
                'b_investment_size_end' => $request->b_investment_size_end,
                'b_expertise' => implode(',',$request->b_expertise),
                'b_fund_source' => implode(',',$request->b_fund_source),
                'industry'=>implode(',',$request->industry),
                'website'=>$request->website,
                'linkedin'=>$request->linkedin,
                'instagram'=>$request->instagram,
                'facebook'=>$request->facebook,
                'status' => 'not verified',
            ]);

       

            //company owned query

            CompanyOwned::create([
                'user_id'=> Auth::user()->id,
                'comp_id' => $comp->id,
                'owner_position' => $request->owner_position,
                'owned_since' => date("Y-m-d"),
                'status' => 'not verified',
            ]);

            $user_id = Auth::user()->id;
            $ud = UserDetail::where('user_id','=',Auth::user()->id)->firstOrFail();
            //airtable api
                        // DD01 send company here
                            
                            $postData =  [
                                'fields' => array(
                                    'Company Name'=>$request->company_name,
                                    'CompanyWebId'=> (string) $comp->id,
                                    'Owner WebID'=> (string) Auth::user()->id,
                                    'Owner Status'=> 'Not Verified',
                                    'Owner Name'=> $ud->first_name,
                                    'Address'=>$request->address,
                                    'City'=>$request->city,
                                    'Country'=>$request->country,
                                    'Postal Code' =>$request->postal_code,
                                    'Email'=>$request->email,
                                    'Phone'=>$request->phone,
                                    'Register Date' => date("Y-m-d"),
                                    'Establish Date' => $request->establish,
                                    'Intro'=>$request->intro,
                                    'About' => $request->about,
                                    'Industry'=>implode(',',$request->industry),
                                    'Investment Type' => $request->b_inv_type,
                                    'MNA Experience' => $request->b_mna_exp,
                                    'Criteria' => implode(',',$request->b_criteria),
                                    'Investment Size Start' => $request->b_investment_size_start,
                                    'Investment Size End' => $request->b_investment_size_end,
                                    'Expertise' => implode(',',$request->b_expertise),
                                    'Fund Source' => implode(',',$request->b_fund_source),
                                    'Website'=>$request->website,
                                    'Linkedin'=>$request->linkedin,
                                    'Instagram'=>$request->instagram,
                                    'Facebook'=>$request->facebook,
                                    'Logo' =>array([
                                        'url'=> url('photo/company/'.$getimageName),
                                    ]),
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameSlide),
                                    ]),
                                    'Photo Main' =>array([
                                        'url'=> url('photo/company/'.$getimageNameMain),
                                    ]),
                                    'Status'=>'not verified'
                                ),
                                'typecast' => true,
                            ];     

        
                            $data = Http::withToken($this->airtable_token)->post($this->airtable_url.'Buyer/',$postData);
                            // dd($data);
                                //getid from airtable
                                $air_id = $data['id'];

                                $company_owned = CompanyOwned::where('user_id','=',$user_id)->get();

                                $px = Company::where('id','=',$company_owned[0]->comp_id)->firstOrFail();
                                $px->air_id = $air_id;
                                
                            $px->save();

                            //DD01

                            // DD02
                     
                            // $coData =  [
                            //     'fields' => array(
                            //         'Company Name'=>$request->company_name,
                            //         'Company Web Id'=> (string) $comp->id,
                            //         'Seller Name'=>  $ud->first_name,
                            //         'Seller Id'=> (string) Auth::user()->id,
                            //         'Status'=>'not verified'
                            //     ),
                            //     'typecast' => true,
                            // ];
                            
                            // Http::withToken(env('AIRTABLE_TOKEN'))->post(env('AIR_URL').'Company%20Owner/',$coData);


                            // DD02


 

        Session::flash('msg', 'Company Saved Successfully' );

        return redirect()->route('homeseller');
    }


    // sub module 
    public function compidentityedit(){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('buyer.couplogo',compact('cd'));
    }

    public function compidentityupdate(Request $request)
    {

             //validation
             $request->validate([
                'company_name' => 'required',

            ],[
                'company_name.required'=>'Investment Name Required',
            ]);


            // company query

            $filepath = 'photo/company';
            $getimageName = null;
            $comp = Company::where('id','=',$request->cid)->firstOrFail();
            $comp->company_name = $request->company_name;
                if($comp->photo != null){
                    if($request->hasFile('photo')){
                        $oldFile = public_path($filepath).'/'.$comp->photo;
                      unlink($oldFile);
            
                      $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                        $request->photo->move(public_path($filepath), $getimageName);
                        $comp->photo = $getimageName;
                    }

                }else{
                    if($request->photo != null)
                    {
                        $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
                        $image = file_get_contents($request->photo);
                        file_put_contents(public_path($filepath).'/'.$getimageName,$image);
                        $comp->photo = $getimageName;
                    }
                }

            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                        if($getimageName != null)
                        {
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Company Name' => (string) $request->company_name,
                                    'Logo' =>array([
                                        'url'=> url('photo/company/'.$getimageName),
                                    ]),
                                ),
                                'typecast' => true,
                            ]; 
                        }else{
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Company Name' => (string) $request->company_name,
                                ),
                                'typecast' => true,
                            ]; 
                        }    
 
                            $data = Http::withToken($this->airtable_token)->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch($this->airtable_url.'Buyer/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Investment Identity Update Successfully' );

        return redirect('buyercompany');
    }

    // 2. update section 1 company status

    public function compsectionedit($sid){

        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        if($co != null){
            $cd =  Company::where('id',$co->comp_id)->get();
        }else{
            $cd = '';
        }
        
        return view('buyer.coupsection')
        ->with('cd',$cd)
        ->with('sid', $sid);
    }

    public function compsectionupdate(Request $request)
    {

             

            // company query

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

            switch ($request->sid) {
                case 1:
                        // section 1
                    $comp->company_name = $request->company_name;
                    $comp->about = $request->about;
                    break;
                case 2:
                         //section 2
                    $comp->establish_date = $request->establish;
                    $comp->b_inv_type = $request->b_inv_type;
                    $comp->b_mna_exp = $request->b_mna_exp;
                    $comp->b_investment_size_start = $request->b_investment_size_start;
                    $comp->b_investment_size_end = $request->b_investment_size_end;
                    break;
                case 4:
                     //section 4
                    $comp->industry = implode(',',$request->industry);
                    $comp->b_criteria = implode(',',$request->b_criteria);
                    $comp->b_expertise = implode(',',$request->b_expertise);
                    $comp->b_fund_source = implode(',',$request->b_fund_source);
                    break;
                case 5:
                      //section 5
                    $comp->intro = $request->intro;
                    break;
                case 6:
                     //section 6
                    $comp->address  = $request->address;
                    $comp->city = $request->city;
                    $comp->country = $request->country;
                    $comp->postal_code = $request->postal_code;
                    $comp->email = $request->email;
                    $comp->phone = $request->phone;
                    $comp->website = $request->website;
                    $comp->linkedin = $request->linkedin;
                    $comp->instagram = $request->instagram;
                    $comp->facebook = $request->facebook;
                    break;
                default:
                 return Redirect::back()->withErrors(['msg', 'Section ID Not Detected']);
                    break;
            }


            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                         
                                    
            switch ($request->sid) {
                case 1:
                    $updateData =  [
                        'fields' => array(
                                            'Company Name'=>$request->company_name,
                                            'About' => $request->about,
                                            'CompanyWebId' => (string) Auth::user()->id,
                                        ),
                                        'typecast' => true,
                                    ];     
                    break;
                case 2:
                    $updateData =  [
                        'fields' => array(
                                    'Establish Date' => $request->establish,
                                    'Investment Type' => $request->b_inv_type,
                                    'MNA Experience' => $request->b_mna_exp,
                                    'Investment Size Start' => $request->b_investment_size_start,
                                    'Investment Size End' => $request->b_investment_size_end,
                                    'CompanyWebId' => (string) Auth::user()->id,
                                ),
                                'typecast' => true,
                            ];   
                    break;
                case 4:
                            $updateData =  [
                                'fields' => array(
                                'Industry'=>implode(',',$request->industry),
                                'Criteria' => implode(',',$request->b_criteria),
                                'Expertise' => implode(',',$request->b_expertise),
                                'Fund Source' => implode(',',$request->b_fund_source),
                                'CompanyWebId' => (string) Auth::user()->id,
                            ),
                            'typecast' => true,
                        ];   
                    break;
                case 5:
                    $updateData =  [
                        'fields' => array(
                        'Intro'=> $request->intro,
                        'CompanyWebId' => (string) Auth::user()->id,
                    ),
                    'typecast' => true,
                ];
                    break;
                case 6:
                        $updateData =  [
                            'fields' => array(
                            'Address'=>$request->address,
                            'City'=>$request->city,
                            'Country'=>$request->country,
                            'Postal Code' =>$request->postal_code,
                            'Email'=>$request->email,
                            'Phone'=>$request->phone,
                            'Website'=>$request->website,
                            'Linkedin'=>$request->linkedin,
                            'Instagram'=>$request->instagram,
                            'Facebook'=>$request->facebook,
                            'CompanyWebId' => (string) Auth::user()->id,
                        ),
                        'typecast' => true,
                    ];
                    break;
                    default:
                    return Redirect::back()->withErrors(['msg', 'Section ID Airtable Not Detected']);

            }
            
                        Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json'
                        ])->patch(env('AIR_URL').'Buyer/'.$comp->air_id,$updateData);

                        //DD01

            Session::flash('msg', 'Investment Updated Successfully' );

            return redirect()->route('buyercompany');

    

    }

        public function compslidephotoedit(){

            $uid = Auth::user()->id;
            $co = CompanyOwned::where('user_id',$uid)->first();
            if($co != null){
                $cd =  Company::where('id',$co->comp_id)->get();
            }else{
                $cd = '';
            }
            
            return view('buyer.coupslide',compact('cd'));
        }
    
        
        public function compmainphotoedit(){
    
            $uid = Auth::user()->id;
            $co = CompanyOwned::where('user_id',$uid)->first();
            if($co != null){
                $cd =  Company::where('id',$co->comp_id)->get();
            }else{
                $cd = '';
            }
            
            return view('buyer.coupmain',compact('cd'));
        }

        public function compslidephotoupdate(Request $request)
    {

             //validation
             $request->validate([
                'photo_slide' => 'required',

            ],[
                'photo_slide.required'=>'Photo Required',
            ]);


            // company query

            $filepath = 'photo/company';

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

                if($comp->photo_slide != null){
                    if($request->hasFile('photo_slide')){
                        $oldFileSlide = public_path($filepath).'/'.$comp->photo_slide;
                      unlink($oldFileSlide);
            
                      $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                        $request->photo_slide->move(public_path($filepath), $getimageNameSlide);
                        $comp->photo_slide = $getimageNameSlide;
                    }

                }else{
                    $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
                    $imageSlide = file_get_contents($request->photo_slide);
                    file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
                    $comp->photo_slide = $getimageNameSlide;
                }

            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameSlide),
                                    ]),
                                ),
                                'typecast' => true,
                            ];     
 
                            $data = Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch(env('AIR_URL').'Buyer/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Investment Photo Background Update Successfully' );

        return redirect('buyercompany');
    }


    public function compmainphotoupdate(Request $request)
    {

             //validation
             $request->validate([
                'photo_main' => 'required',

            ],[
                'photo_main.required'=>'Photo Required',
            ]);


            // company query

            $filepath = 'photo/company';
      

            $comp = Company::where('id','=',$request->cid)->firstOrFail();

                if($comp->photo_main != null){
                    if($request->hasFile('photo_main')){
                        $oldFileMain = public_path($filepath).'/'.$comp->photo_main;
                      unlink($oldFileMain);
            
                      $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                        $request->photo_main->move(public_path($filepath), $getimageNameMain);
                        $comp->photo_main = $getimageNameMain;
                    }

                }else{
                    $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
                    $imageMain = file_get_contents($request->photo_main);
                    file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
                    $comp->photo_main = $getimageNameMain;
                }

            $comp->save();


            //airtable api
                        // DD01 update company here
                            
                            $updateData =  [
                                'fields' => array(
                                   
                                    'CompanyWebId' => (string) Auth::user()->id,
                                    'Photo Background' =>array([
                                        'url'=> url('photo/company/'.$getimageNameMain),
                                    ]),
                                ),
                                'typecast' => true,
                            ];     
 
                            $data = Http::withToken(env('AIRTABLE_TOKEN'))->withHeaders([
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json'
                            ])->patch(env('AIR_URL').'Buyer/'.$comp->air_id,$updateData);
     
                            //DD01

        Session::flash('msg', 'Investment Photo Background Update Successfully' );

        return redirect('buyercompany');
    }
     
    public function preftrans(){
        $uid = Auth::user()->id;
        $co = CompanyOwned::where('user_id',$uid)->first();
        $cp = PreferedTransaction::where('comp_id',$co->comp_id)->orderBy('id', 'ASC')->paginate(6);
        return view('buyer.preftrans',compact('cp'));
 }

    public function preftransadd(){
        return view('buyer.preftransadd');
    }

    public function preftranssave(Request $request){
        //validation
        $request->validate([
           'title' => 'required',
           'note'=>'required',
       ],[
           'title.required'=>'Title Required',
           'note.required'=> 'Note Required',
       ]);
    
       // company query
       $uid = Auth::user()->id;
       $co = CompanyOwned::where('user_id',$uid)->first();
    
           PreferedTransaction::create([
               'comp_id'=> $co->comp_id,
               'title' => $request->title,
               'sub_title' => $request->sub_title,
               'note' => $request->note,
           ]);
    

    Session::flash('msg', 'Prefered Transaction '.$request->title.' Saved Successfully' );
    
    return redirect()->route('buyerpreftrans');
    }

    public function preftransedit($id){
        $cfd = PreferedTransaction::where('id',$id)->first();
       
        return view('buyer.preftransedit',compact('cfd'));
    }

    public function preftransupdate(Request $request)
    {

             //validation
             $request->validate([
                'title' => 'required',
                'note'=>'required',
            ],[
                'title.required'=>'Title Required',
                'note.required'=> 'Note Required',
            ]);


            // company query


            $fct = PreferedTransaction::where('id','=',$request->id)->firstOrFail();

                $fct->title = $request->title;
                $fct->sub_title = $request->sub_title;
                $fct->note = $request->note;

            $fct->save();

        Session::flash('msg', 'Prefered Transaction Updated Successfully' );

        return redirect()->route('buyerpreftrans');
    }

    
    public function preftransdelete($id)
    {

        // company query

        $cp = PreferedTransaction::where('id',$id)->first();

        $cp->delete();

        Session::flash('msg', 'Perfered Transaction Deleted Successfully' );

        return redirect()->route('buyerpreftrans');
    }

      // API Section

      public function updateCompanyAPI(Request $request){


        //validation
        $request->validate([
            'company_name' => 'required',
            'category'=> 'required',
            'address'=> 'required',
            'city'=> 'required',
            'country'=> 'required',
            'postal_code' => 'required',
            'email'=> 'required',
            'phone'=> 'required',
            'establish' => 'required',
            'intro'=> 'required  | min: 100',
            'about' => 'required  | min: 100',
            'industry'=>'required',
            'b_inv_type' => 'required',
            'b_mna_exp' => 'required',
            'b_criteria' => 'required',
            'b_investment_size_start' => 'required',
            'b_investment_size_end' => 'required',
            'b_expertise' => 'required',
            'status'=>'required',
     ],[
        'company_name.required'=>'Company Name Required',
        'category.required'=>'Category Required',
        'address.required'=>'Address Required',
        'city.required'=>'City Required',
        'country.required'=>'Country Required',
        'postal_code.required'=>'Postal Code Required',
        'email.required'=>'Company Email Required',
        'phone.required'=>'Company Phone Required',
        'establish.required'=>'Establish Date Required',
        'intro.required'=>'Owner Introduction Required',
        'about.required'=>'About Company Required',
        'industry.required'=>'Industry Required',
        'b_inv_type.required' => 'Investment Size Required',
        'b_mna_exp.required' => 'MNA Experience Required',
        'b_criteria.required' => 'Criteria Required',
        'b_investment_size_start.required' => 'Investment Size Required',
        'b_investment_size_end.required' => 'Investment Size Required',
        'b_expertise.required' => 'required',
        'status.required'=>'Status Required',
     ]);





    $comp = Company::where('id','=',$request->webId)->firstOrFail();

    $comp->company_name = $request->company_name;
    $comp->category = $request->category;
    $comp->address  = $request->address;
    $comp->registered_since = date("Y-m-d");
    $comp->establish_date = $request->establish;
    $comp->city = $request->city;
    $comp->country = $request->country;
    $comp->postal_code = $request->postal_code;
    $comp->email = $request->email;
    $comp->phone = $request->phone;
    $comp->intro = $request->intro;
    $comp->about = $request->about;
    $comp->industry = $request->industry;
    $comp->b_inv_type = $request->b_inv_type;
    $comp->b_mna_exp = $request->b_mna_exp;
    $comp->b_criteria = $request->b_criteria;
    $comp->b_investment_size_start = $request->b_investment_size_start;
    $comp->b_investment_size_end = $request->b_investment_size_end;
    $comp->website = $request->website;
    $comp->linkedin = $request->linkedin;
    $comp->instagram = $request->instagram;
    $comp->facebook = $request->facebook;
    $comp->status = $request->status;


    $filepath = 'photo/company/';
    if($comp->photo != null){
        if($request->hasFile('photo')){
            $oldFile = public_path($filepath).$comp->photo;
          unlink($oldFile);

          $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
            $request->photo->move(public_path($filepath), $getimageName);
            $comp->photo = $getimageName;
        }

    }else{
        $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'.jpg';
        $image = file_get_contents($request->photo);
        file_put_contents(public_path($filepath).'/'.$getimageName,$image);
        $comp->photo = $getimageName;
    }


    // upload slide
    if($comp->photo_slide != null){
    if($request->hasFile('photo_slide')){
        $oldFileSlide = public_path($filepath).$comp->photo_slide;
        unlink($oldFileSlide);

        $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
        $request->photo_slide->move(public_path($filepath), $getimageNameSlide);
        $comp->photo_slide = $getimageNameSlide;
    }

    }else{
        $getimageNameSlide =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_slide.jpg';
        $imageSlide = file_get_contents($request->photo_slide);
        file_put_contents(public_path($filepath).'/'.$getimageNameSlide,$imageSlide);
        $comp->photo_slide = $getimageNameSlide;
    }

    // upload main
    if($comp->photo_main != null){
        if($request->hasFile('photo_main')){
            $oldFileMain = public_path($filepath).$comp->photo_main;
            unlink($oldFileMain);

            $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
            $request->photo_main->move(public_path($filepath), $getimageNameMain);
            $comp->photo_main = $getimageNameMain;
        }

    }else{
        $getimageNameMain =  date("Ymdhis").'.'.str_replace(' ', '', $request->company_name).'_main.jpg';
        $imageMain = file_get_contents($request->photo_main);
        file_put_contents(public_path($filepath).'/'.$getimageNameMain,$imageMain);
        $comp->photo_main = $getimageNameMain;
    }


    
    $res = $comp->save();



        return response()->json($res);
    }


    public function deleteCompanyAPI(Request $request)
    {


            //validation
            $request->validate([
                'webId' => 'required',
            ],[
                'webId.required'=>'Web ID Required',
            ]);

            // should check transaction first !!!


             // should check transaction first !!!

            $company_owned = CompanyOwned::where('comp_id','=',$request->webId)->firstOrFail();
            $company_owned->delete();


            $post = Company::where('id','=',$request->webId)->firstOrFail();

            //Delete Sub Data Here


            //Delete Sub Data Here

            $filepath = 'photo/company/';
            if($post->photo != null){
                    $oldFile = public_path($filepath).$post->photo;
                    unlink($oldFile);
            }

            if($post->photo_slide != null){
                $oldFileSlide = public_path($filepath).$post->photo_slide;
                unlink($oldFileSlide);
            }

            if($post->photo_main != null){
                $oldFileMain = public_path($filepath).$post->photo_main;
                unlink($oldFileMain);
            }

            $res = $post->delete();


            return response()->json($res);
    }



}
