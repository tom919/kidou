<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\CompanyOwned;
use App\CompanyPhoto;
use App\CompanyVideo;
use App\CompanyCertificate;
use App\CompanyAwards;
use App\CompanyFact;
use App\CompanyPress;
use App\BussinesOpportunities;
use App\User;
use App\UserDetail;
use Auth;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\UtilController;

class CompanySharedController extends Controller
{
    //

    public function __construct(UtilController $util_controller)
    {
        $this->util_controller = $util_controller;
    }



        // company fact


        public function sharedSeller($id)
        {
            $UtilController = new UtilController;

            $UtilController->VisitorCounter($id);
            
            $co = Company::where('id','=',$id)->firstOrFail();
            $finFact = CompanyFact::where('comp_id','=',$id)
            ->where('category','=','finance')
            ->get();


            $cown = CompanyOwned::where('comp_id',$id)->first();
            $ud = UserDetail::where('user_id','=',$cown->user_id)->firstOrFail();
            $this->util_controller->VisitorCounter('seller', $cown->user_id);

            $servFact = CompanyFact::where('comp_id','=',$id)
            ->where('category','=','service')
            ->get();

            $orgFact = CompanyFact::where('comp_id','=',$id)
            ->where('category','=','organization')
            ->get();

            $CC = CompanyCertificate::where('comp_id','=',$id)->get();

            $cv = CompanyVideo::where('comp_id',$co->id)->orderBy('id', 'ASC')->get();
            $cp = CompanyPress::where('comp_id',$co->id)->orderBy('id', 'ASC')->get();
            $bo = BussinesOpportunities::where('comp_id',$co->id)->orderBy('id', 'ASC')->get();
            $aw = CompanyAwards::where('comp_id',$co->id)->orderBy('id', 'ASC')->get();
         

            return view('seller.sellershared',compact('co'))
            ->with(compact('ud'))
            ->with(compact('cown'))
            ->with(compact('cv'))
            ->with(compact('cp'))
            ->with(compact('bo'))
            ->with(compact('aw'))
            ->with(compact('finFact'))
            ->with(compact('servFact'))
            ->with(compact('orgFact'))
            ->with(compact('CC'));
        }



}
