<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserDetail;
use App\Http\Controllers\UtilController;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;

class SellerController extends Controller
{
    private $airtable_token = "key8lP4O2mDi0XPei";
    private $airtable_url = "https://api.airtable.com/v0/appmWUP3TQoCNNWdp/";
    //
    public function __construct(UtilController $util_controller)
    {
        $this->middleware(['auth']);
        $this->util_controller = $util_controller;
    }

    

    public function index()
    {
        $percentage = $this->util_controller->calcProfile();
        $VisitorAmount = $this->util_controller->VisitorDisplay();
        
        return view('seller.home')
        ->with('percentage',$percentage)
        ->with('visitoramount',$VisitorAmount);
    }

    public function profile()
    {
        $ua = Auth::user();
        $ud = UserDetail::where('user_id',$ua->id)->first();
        return view('seller.profile',compact('ud'))
        ->with('ua',$ua);
    }

    public function UpdateDetail()
    {
        // get id first
        $uid = Auth::user()->id;
        $ud = UserDetail::where('user_id',$uid)->first();
        return view('seller.updatedetail',compact('ud'));
    }

    public function savedetail(Request $requests)
    {
         //validation
         $requests->validate([
            'uid' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'pob' => 'required',
            'dob' => 'required',
            'city' =>'required',
            'country' => 'required',
            'phone'=> 'required',
            'story' => 'required | min: 100',
        ],[
            'uid.required'=>'User ID Required',
            'first_name.required'=>'First Name Required',
            'last_name.required'=>'Last Name Required',
            'pob.required'=>'Place of Birth Required',
            'dob.required'=>'Date of Birth Required',
            'city.required'=>'City required',
            'country.required'=>'Country required',
            'phone.required' => 'Phone required',
            'story.required' => 'Story Required',
        ]);




        $uid = $requests->uid;

        $filepath = 'photo/user/';
        $postMain = User::where('id','=',$uid)->firstOrFail();
        $post = UserDetail::where('user_id','=',$uid)->firstOrFail();

        $post->first_name = $requests->first_name;
        $post->last_name = $requests->last_name;
        $post->pob = $requests->pob;
        $post->dob = $requests->dob;
        $post->address = $requests->address;
        $post->city = $requests->city;
        $post->country = $requests->country;
        $post->phone = $requests->phone;
        $post->story = $requests->story;

        if($post->photo != null){
            if($requests->hasFile('photo')){
                $oldFile = public_path($filepath).$post->photo;
              unlink($oldFile);
    
                $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $requests->first_name).'.'.$requests->photo->getClientOriginalExtension();
                $requests->photo->move(public_path($filepath), $getimageName);
                $post->photo = $getimageName;
           }
        }else{
            $getimageName =  date("Ymdhis").'.'.str_replace(' ', '', $requests->first_name).'.'.$requests->photo->getClientOriginalExtension();
            $requests->photo->move(public_path($filepath), $getimageName);
            $post->photo = $getimageName;
        }

       
       $post->save();

          //DD01 Send Update here
          $postData =  [
            'fields' => array(
                'First Name'=>  $requests->first_name,
                'Last Name' =>  $requests->last_name,
                'Place Of Birth' => $requests->pob,
                'Date Of Birth' => $requests->dob,
                'Address' => $requests->address,
                'City' => $requests->city,
                'Country'=>$requests->country,
                'Phone'=>$requests->phone,
                'Story' => $requests->story,
                'Photo' =>array([
                    'url'=> url('photo/user/'.$post->photo),
                ]),
                // 'Verify Date' => $postMain->email_verified_at->format('Y-m-d'),
                    ),
            ];
            
    
    
        $data = Http::withToken($this->airtable_token)->patch($this->airtable_url.'User/'.$postMain->air_id,$postData);

        // dd($data->json());
       
     Session::flash('msg', 'Profile Detail updated Successfully' );

       return redirect()->route('homeseller');

    }

    
    public function updatecredential()
    {
        $uid = Auth::user()->id;
        $uc = User::where('id',$uid)->first();
        return view('seller.updatecredential', compact('uc'));
    }
    public function saveupdatecredential(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required|min:6',
            'repassword' => 'same:password'
        ],[
            'email.required'=>'Email Required',
            'password.required'=>'Password Required',
            'repassword.required'=> 'Retype Password',
            'repassword.same'=>'Password confirm must same',
        ]);

        $uid = Auth::user()->id;
        $post = User::where('id','=',$uid)->firstOrFail();
        $post->email = $request->email;
        $post->password = Hash::make($request->password);
        
       $post->save();


       //DD01 Send Update here
       $postData =  [
        'fields' => array(
            'Email'=>$request->email,
            'Password' => Hash::make($request->password),
                ),
        ];     


        $data = Http::withToken($this->airtable_token)->patch($this->airtable_url.'User/'.$post->air_id,$postData);

       //DD01

       Session::flash('msg', 'Credential updated Successfully' );

       return redirect()->route('homeseller');
    }

   

    

}
