<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\CompanyOwned;
use App\CompanyPhoto;
use App\CompanyVideo;
use App\CompanyCertificate;
use App\CompanyAwards;
use App\CompanyFact;
use App\CompanyPress;
use App\BussinesOpportunities;
use App\PreferedTransaction;
use App\User;
use App\UserDetail;
use Auth;
use Storage;
use Session;
use Illuminate\Support\Facades\Http;


class InvestSharedController extends Controller
{
    //

    public function sharedBuyer($id)
    {
        $co = Company::where('id','=',$id)->firstOrFail();

        $cown = CompanyOwned::where('comp_id',$id)->first();
        $ud = UserDetail::where('user_id','=',$cown->user_id)->firstOrFail();
        $pft = PreferedTransaction::where('comp_id',$co->id)->orderBy('id', 'ASC')->get();


        return view('buyer.buyershared',compact('co'))
        ->with(compact('ud'))
        ->with(compact('cown'))
        ->with(compact('pft'));
    }
    


}
