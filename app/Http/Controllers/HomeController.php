<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\UtilController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware(['auth', 'verified']);
    // }
    public function __construct(UtilController $util_controller)
    {
        $this->util_controller = $util_controller;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/register');
    }

    public function indexseller()
    {
        // $this->util_controller->VisitorCounter('seller');
        return view('seller.homepage');
    }

    public function indexbuyer()
    {
        return view('buyer.homepage');
    }

    public function test()
    {
        return view('test');
    }
    public function testProc(Request $request)
    {
        $data = $request->nama;
        // return response()->json($data);
        return redirect()->route('home');
    }


}
