<?php

namespace App\Http\Middleware;
use Illuminate\Auth\Middleware\Role as Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, String $role)
    {
       
        // if (!Auth::check()) // This isnt necessary, it should be part of your 'auth' middleware
        // {
        //     return redirect('/home');
        // }
  
      $user = Auth::user();
       
      if(Auth::user()->status == "suspend"){
        return redirect('/suspend');
      }

      if($user->user_type == $role)
      {
        return $next($request);
      }
  
      return redirect('/login');
    }
}
