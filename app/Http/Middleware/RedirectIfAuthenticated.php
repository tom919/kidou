<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $role = Auth::user()->user_type;
            
            if(Auth::user()->status == 'suspend'){
              return redirect('/suspend');
            }

            switch ($role) {
              case 0:
                return redirect('/home');
                break;
              case 3:
                return redirect('/homeseller');
                break;
            case 4:
                return redirect('/homebuyer');
                break; 
              default:
                return redirect('/unauthorized'); 
              break;
            }
  
        }
        // else{
        //     $request->session()->flash('error', 'Access Denied');
        //     return back();
        // }


        

        return $next($request);
    }
}
