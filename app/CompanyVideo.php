<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyVideo extends Model
{
    //
    protected $table = 'comp_video';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'title',
        'comp_video',
        'upload_date',
        'priority',
    ];
}
