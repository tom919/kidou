<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCertificate extends Model
{
    //
    protected $table = 'certificates';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'title',
        'image',
        'upload_date',
        'priority',
    ];
}
