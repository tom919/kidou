<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BussinesOpportunities extends Model
{
    //

    protected $table = 'bussines_opportunities';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'comp_id',
        'title',
        'note',
    ];
}
