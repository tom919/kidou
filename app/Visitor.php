<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    //
    protected $table = 'visitor';
    public $timestamps = false;

    protected $fillable = [
        'session_id','coid', 'visit_time','ip','os'
    ];
}
