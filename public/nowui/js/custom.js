
 transparent = true;
 transparentDemo = true;
 fixedTop = false;
 
 navbar_initialized = false;
 backgroundOrange = false;
 sidebar_mini_active = true;
 toggle_initialized = false;
 
 var is_iPad = navigator.userAgent.match(/iPad/i) != null;
 var scrollElement = navigator.platform.indexOf('Win') > -1 ? $(".main-panel") : $(window);
 
 seq = 0, delays = 80, durations = 500;
 seq2 = 0, delays2 = 80, durations2 = 500;
 
 // Returns a function, that, as long as it continues to be invoked, will not
 // be triggered. The function will be called after it stops being called for
 // N milliseconds. If `immediate` is passed, trigger the function on the
 // leading edge, instead of the trailing.
 
 function debounce(func, wait, immediate) {
     var timeout;
     return function() {
         var context = this,
             args = arguments;
         clearTimeout(timeout);
         timeout = setTimeout(function() {
             timeout = null;
             if (!immediate) func.apply(context, args);
         }, wait);
         if (immediate && !timeout) func.apply(context, args);
     };
 };
 
 (function() {
     let isWindows = navigator.platform.indexOf('Win') > -1;
     if (isWindows) {
         if ($('.sidebar-wrapper').length) {
             new PerfectScrollbar('.sidebar-wrapper');
         }
         if ($('.main-panel').length) {
             new PerfectScrollbar('.main-panel');
         }
         $('html').addClass('perfect-scrollbar-on');
     } else {
         $('html').addClass('perfect-scrollbar-off');
     }
 })();
 
 $(document).ready(function() {
     $navbar = $('.navbar[color-on-scroll]');
     scroll_distance = $navbar.attr('color-on-scroll') || 500;
 
     //  Activate the Tooltips
     $('[data-toggle="tooltip"], [rel="tooltip"]').tooltip();
 
     // Activate Popovers and set color for popovers
     $('[data-toggle="popover"]').each(function() {
         color_class = $(this).data('color');
         $(this).popover({
             template: '<div class="popover popover-' + color_class + '" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
         });
     });
 
     var tagClass = $('.tagsinput').data('color');
 
     if ($(".tagsinput").length != 0) {
         $('.tagsinput').tagsinput();
     }
 
     $('.bootstrap-tagsinput').addClass('' + tagClass + '-badge');
 
     //    Activate bootstrap-select
     if ($(".selectpicker").length != 0) {
         $(".selectpicker").selectpicker({
             iconBase: "now-ui-icons",
             tickIcon: "ui-1_check"
         });
     }
 
     if ($('body').hasClass('sidebar-mini')) {
         sidebar_mini_active = true
     }
 
     var isWindows = navigator.platform.startsWith('Win');
     if (isWindows) {
         $('.modal').on('show.bs.modal', function() {
             var ps1 = new PerfectScrollbar('#myModal');
             var ps2 = new PerfectScrollbar('#noticeModal');
             var ps3 = new PerfectScrollbar('#myModal10');
 
         }).on('hide.bs.modal', function() {
             var ps1 = new PerfectScrollbar('#myModal');
             var ps2 = new PerfectScrollbar('#noticeModal');
             var ps3 = new PerfectScrollbar('#myModal10');
 
             ps1.destroy();
             ps2.destroy();
             ps3.destroy();
         });
     }
 
     if ($('.full-screen-map').length == 0 && $('.bd-docs').length == 0) {
         // On click navbar-collapse the menu will be white not transparent
         $('.collapse').on('show.bs.collapse', function() {
             $(this).closest('.navbar').removeClass('navbar-transparent').addClass('bg-white');
         }).on('hide.bs.collapse', function() {
             if ($(document).scrollTop() <= scroll_distance) {
                 $(this).closest('.navbar').addClass('navbar-transparent').removeClass('bg-white');
             }
         });
     }
 
 
     // FileInput
     $('.form-file-simple .inputFileVisible').click(function() {
         $(this).siblings('.inputFileHidden').trigger('click');
     });
 
     $('.form-file-simple .inputFileHidden').change(function() {
         var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
         $(this).siblings('.inputFileVisible').val(filename);
     });
     $('.form-file-simple .inputFileHidden, .form-file-multiple .inputFileHidden').css('z-index', '-1');
 
     $('.form-file-multiple .inputFileVisible, .form-file-multiple .input-group-btn').click(function() {
         $(this).siblings('.inputFileHidden').trigger('click');
     });
 
     $('.form-file-multiple .inputFileHidden').change(function() {
         var names = '';
         for (var i = 0; i < $(this).get(0).files.length; ++i) {
             if (i < $(this).get(0).files.length - 1) {
                 names += $(this).get(0).files.item(i).name + ',';
             } else {
                 names += $(this).get(0).files.item(i).name;
             }
         }
         $(this).siblings('.inputFileVisible').val(names);
     });
 
     nowuiDashboard.initMinimizeSidebar();
 
     // Check if we have the attr "color-on-scroll" then add the function to remove the class "navbar-transparent" so it will transform to a plain color.
 
 
     if ($('.navbar[color-on-scroll]').length != 0) {
         nowuiDashboard.checkScrollForTransparentNavbar();
         scrollElement.on('scroll', nowuiDashboard.checkScrollForTransparentNavbar)
     }
 
     $('.form-control').on("focus", function() {
         $(this).parent('.input-group').addClass("input-group-focus");
     }).on("blur", function() {
         $(this).parent(".input-group").removeClass("input-group-focus");
     });
 
     // Activate bootstrapSwitch
     $('.bootstrap-switch').each(function() {
         $this = $(this);
         data_on_label = $this.data('on-label') || '';
         data_off_label = $this.data('off-label') || '';
 
         $this.bootstrapSwitch({
             onText: data_on_label,
             offText: data_off_label
         });
     });
 
     if (is_iPad) {
         $('body').removeClass('sidebar-mini');
     }
 });
 
 $(document).on('click', '.navbar-toggle', function() {
     $toggle = $(this);
 
     if (nowuiDashboard.misc.navbar_menu_visible == 1) {
         $('html').removeClass('nav-open');
         nowuiDashboard.misc.navbar_menu_visible = 0;
         setTimeout(function() {
             $toggle.removeClass('toggled');
             $('#bodyClick').remove();
         }, 550);
 
     } else {
         setTimeout(function() {
             $toggle.addClass('toggled');
         }, 580);
 
         div = '<div id="bodyClick"></div>';
         $(div).appendTo('body').click(function() {
             $('html').removeClass('nav-open');
             nowuiDashboard.misc.navbar_menu_visible = 0;
             setTimeout(function() {
                 $toggle.removeClass('toggled');
                 $('#bodyClick').remove();
             }, 550);
         });
 
         $('html').addClass('nav-open');
         nowuiDashboard.misc.navbar_menu_visible = 1;
     }
 });
 
 $(window).resize(function() {
     // reset the seq for charts drawing animations
     seq = seq2 = 0;
 
     if ($('.full-screen-map').length == 0 && $('.bd-docs').length == 0) {
 
         $navbar = $('.navbar');
         isExpanded = $('.navbar').find('[data-toggle="collapse"]').attr("aria-expanded");
         if ($navbar.hasClass('bg-white') && $(window).width() > 991) {
             if (scrollElement.scrollTop() == 0) {
                 $navbar.removeClass('bg-white').addClass('navbar-transparent');
             }
         } else if ($navbar.hasClass('navbar-transparent') && $(window).width() < 991 && isExpanded != "false") {
             $navbar.addClass('bg-white').removeClass('navbar-transparent');
         }
     }
     if (is_iPad) {
         $('body').removeClass('sidebar-mini');
     }
 });
 
 nowuiDashboard = {
     misc: {
         navbar_menu_visible: 0
     },
 
     checkScrollForTransparentNavbar: debounce(function() {
         if (scrollElement.scrollTop() >= scroll_distance) {
             if (transparent) {
                 transparent = false;
                 $('.navbar[color-on-scroll]').removeClass('navbar-transparent').addClass('bg-white');
             }
         } else {
             if (!transparent) {
                 transparent = true;
                 if ($(".navbar.fixed-top .navbar-toggler[aria-expanded='false']").length !== 0 || $(window).width() > 991) {
                     $('.navbar[color-on-scroll]').addClass('navbar-transparent').removeClass('bg-white');
                 }
             }
         }
     }, 17),
 
     checkSidebarImage: function() {
         $sidebar = $('.sidebar');
         image_src = $sidebar.data('image');
 
         if (image_src !== undefined) {
             sidebar_container = '<div class="sidebar-background" style="background-image: url(' + image_src + ') "/>'
             $sidebar.append(sidebar_container);
         }
     },
 
     initMinimizeSidebar: function() {
         $('#minimizeSidebar').click(function() {
             var $btn = $(this);
 
 
             if (sidebar_mini_active == true) {
                 $('body').removeClass('sidebar-mini');
                 sidebar_mini_active = false;
                 nowuiDashboard.showSidebarMessage('Sidebar mini deactivated...');
             } else {
                 $('body').addClass('sidebar-mini');
                 sidebar_mini_active = true;
                 nowuiDashboard.showSidebarMessage('Sidebar mini activated...');
             }
 
             // we simulate the window Resize so the charts will get updated in realtime.
             var simulateWindowResize = setInterval(function() {
                 window.dispatchEvent(new Event('resize'));
             }, 180);
 
             // we stop the simulation of Window Resize after the animations are completed
             setTimeout(function() {
                 clearInterval(simulateWindowResize);
             }, 1000);
         });
     },
 
     startAnimationForLineChart: function(chart) {
 
         chart.on('draw', function(data) {
             if (data.type === 'line' || data.type === 'area') {
                 data.element.animate({
                     d: {
                         begin: 600,
                         dur: 700,
                         from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                         to: data.path.clone().stringify(),
                         easing: Chartist.Svg.Easing.easeOutQuint
                     }
                 });
             } else if (data.type === 'point') {
                 seq++;
                 data.element.animate({
                     opacity: {
                         begin: seq * delays,
                         dur: durations,
                         from: 0,
                         to: 1,
                         easing: 'ease'
                     }
                 });
             }
         });
 
         seq = 0;
     },
     startAnimationForBarChart: function(chart) {
 
         chart.on('draw', function(data) {
             if (data.type === 'bar') {
                 seq2++;
                 data.element.animate({
                     opacity: {
                         begin: seq2 * delays2,
                         dur: durations2,
                         from: 0,
                         to: 1,
                         easing: 'ease'
                     }
                 });
             }
         });
 
         seq2 = 0;
     },
     showSidebarMessage: function(message) {
         try {
             $.notify({
                 icon: "now-ui-icons ui-1_bell-53",
                 message: message
             }, {
                 type: 'info',
                 timer: 4000,
                 placement: {
                     from: 'top',
                     align: 'right'
                 }
             });
         } catch (e) {
             console.log('Notify library is missing, please make sure you have the notifications library added.');
         }
 
     }
 };
 
 function hexToRGB(hex, alpha) {
     var r = parseInt(hex.slice(1, 3), 16),
         g = parseInt(hex.slice(3, 5), 16),
         b = parseInt(hex.slice(5, 7), 16);
 
     if (alpha) {
         return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
     } else {
         return "rgb(" + r + ", " + g + ", " + b + ")";
     }
 }

var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

autocomplete(document.getElementById("myCountry"), countries);
 
function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
  } 