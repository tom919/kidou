<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$masterRole ='role:0';
$buyerRole = 'role:4';
$sellerRole = 'role:3';



Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'UserApiController@login');
    Route::post('register', 'UserApiController@register');
});


// util route

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::post('logout', 'UserApiController@logout');
        Route::get('user', 'UserApiController@user');
 

        // master group
                Route::group([
                    'middleware' => 'roleapi:0'
                ], function(){
                        Route::get('homemaster', 'TestController@home');

                        Route::post('air/updatedetail', 'UserApiController@updateDetailAPI');
                        Route::post('air/deleteuser', 'UserApiController@deleteUserAPI');


                        Route::post('air/updatecompany', 'CompanyController@updateCompanyAPI');
                        Route::post('air/deletecompany', 'CompanyController@deleteCompanyAPI');

                        Route::post('air/updateinvestment', 'InvestController@updateCompanyAPI');
                        Route::post('air/deleteinvestment', 'InvestController@deleteCompanyAPI');
                });
        // eof master group
    });




  