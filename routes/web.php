<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$masterRole='role:0';
$buyerRole = 'role:4';
$sellerRole = 'role:3';




Route::get('/', 'HomeController@index');
Route::get('verifyemail/{token}', 'UserController@EmailVerificationProcess');

Route::get('registersbuyer', 'Auth\RegisterController@showBuyerForm')->name('registersbuyer');
Route::get('registersseller', 'Auth\RegisterController@showSellerForm')->name('registersseller');


Route::get('testlayout', 'TestController@showLayout');
// util route
Route::get('/getmarketchannel', 'UtilController@getmarketchannel');
Route::get('/getsaleschannel', 'UtilController@getsaleschannel');
Route::get('/getbussinesspec', 'UtilController@getbussinesspec');
Route::get('/getapplicableassets', 'UtilController@getapplicableassets');
Route::get('/getindustry', 'UtilController@getindustry');
Route::get('/getcategory', 'UtilController@getcategory');


// Route::get('registers', 'Auth\RegisterController@showRegistrationForm');
//util routes

Route::get('/suspend', 'UtilController@suspend')->name('suspend');
Route::get('/unauthorized', 'UtilController@unauthorized')->name('unauthorized');
Route::get('/visitorcounter', 'UtilController@VisitorCounter');
Route::get('/visitordisplay', 'UtilController@VisitorDisplay');

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home')->middleware($masterRole);

//testapi
Route::get('/testgetlist', 'TestController@testGetList');
Route::get('/testgetlist/{id}', 'TestController@testGetListDetail');
Route::get('/testpostdata', 'TestController@testPostData');
Route::get('/testupdatedata', 'TestController@testUpdateData');
Route::get('/testdeletedata', 'TestController@testDeleteData');






// seller routes

Route::get('/homeseller', 'SellerController@index')->name('homeseller')->middleware($sellerRole);
Route::get('/visitor', 'UtilController@VisitorCounter');
Route::get('/visitordisplay', 'UtilController@VisitorDisplay');
Route::get('/sellerprofile', 'SellerController@profile')->middleware($sellerRole);
// email verification
Route::get('/selleremailverify', 'UserController@EmailVerification')->middleware($sellerRole);
// email verification
Route::post('/sellersavedetail', 'SellerController@savedetail')->middleware($sellerRole);
Route::get('/sellerupdatedetail', 'SellerController@updateDetail')->middleware($sellerRole);
Route::post('/sellersavecredential', 'SellerController@saveupdatecredential')->middleware($sellerRole);
Route::get('/sellerupdatecredential', 'SellerController@updateCredential')->middleware($sellerRole);


Route::get('/sellercompany', 'CompanyController@company')->middleware($sellerRole)->name('sellercompany');

Route::get('/sellercompanyphoto', 'CompanyController@companyphoto')->middleware($sellerRole);
Route::get('/sellercompanyphotocreate', 'CompanyController@companycreatephoto')->middleware($sellerRole);
Route::post('/sellercompanyphotocreatesave', 'CompanyController@companycreatephotosave')->middleware($sellerRole);
Route::get('/sellercompanyphotodelete/{id}', 'CompanyController@companyphotodelete')->middleware($sellerRole);

Route::get('/sellercompanyvideo', 'CompanyController@companyvideo')->middleware($sellerRole);
Route::get('/sellercompanyvideocreate', 'CompanyController@companycreatevideo')->middleware($sellerRole);
Route::post('/sellercompanyvideocreatesave', 'CompanyController@companycreatevideosave')->middleware($sellerRole);
Route::get('/sellercompanyvideodelete/{id}', 'CompanyController@companyvideodelete')->middleware($sellerRole);

Route::get('/sellercompanycreate', 'CompanyController@companycreate')->middleware($sellerRole);
Route::post('/sellercompanycreatesave', 'CompanyController@companyCreateSave')->middleware($sellerRole);
Route::get('/sellercompanyupdate', 'CompanyController@companyupdate')->middleware($sellerRole);
Route::post('/sellercompanyupdatesave', 'CompanyController@companyUpdateSave')->middleware($sellerRole);

// sub module update
Route::get('/sellercompidentityedit', 'CompanyController@compidentityedit')->middleware($sellerRole);
Route::post('/sellercompidentityupdate', 'CompanyController@compidentityupdate')->middleware($sellerRole);

Route::get('/sellercompslidephotoedit', 'CompanyController@compslidephotoedit')->middleware($sellerRole);
Route::post('/sellercompslidephotoupdate', 'CompanyController@compslidephotoupdate')->middleware($sellerRole);

Route::get('/sellercompmainphotoedit', 'CompanyController@compmainphotoedit')->middleware($sellerRole);
Route::post('/sellercompmainphotoupdate', 'CompanyController@compmainphotoupdate')->middleware($sellerRole);

Route::get('/sellercompsectionedit/{sid}', 'CompanyController@compsectionedit')->middleware($sellerRole);
Route::post('/sellercompsectionupdate', 'CompanyController@compsectionupdate')->middleware($sellerRole);
// sub module update

Route::get('/sellercompanycertificate', 'CompanyController@companyCertificate')->middleware($sellerRole)->name('sellercompanycertificate');
Route::get('/sellercompanycertificatecreate', 'CompanyController@companycreatecertificate')->middleware($sellerRole);
Route::post('/sellercompanycertificatecreatesave', 'CompanyController@companycreatecertificatesave')->middleware($sellerRole);
Route::get('/sellercompanycertificatedelete/{id}', 'CompanyController@companycertificatedelete')->middleware($sellerRole);

Route::get('/sellercompanyawards', 'CompanyController@companyAwards')->middleware($sellerRole);
Route::get('/sellercompanyawardscreate', 'CompanyController@companycreateawards')->middleware($sellerRole);
Route::post('/sellercompanyawardscreatesave', 'CompanyController@companycreateawardssave')->middleware($sellerRole);
Route::get('/sellercompanyawardsdelete/{id}', 'CompanyController@companyawardsdelete')->middleware($sellerRole);


Route::get('/sellercompanyfact', 'CompanyController@companyFact')->middleware($sellerRole)->name('sellercompanyfact');
Route::get('/sellercompanyfactcreate', 'CompanyController@companycreatefact')->middleware($sellerRole);
Route::post('/sellercompanyfactcreatesave', 'CompanyController@companycreatefactsave')->middleware($sellerRole);
Route::get('/sellercompanyupdatefact/{id}', 'CompanyController@companyUpdateFact')->middleware($sellerRole);
Route::post('/sellercompanyupdatefactsave', 'CompanyController@companyUpdateFactSave')->middleware($sellerRole);
Route::get('/sellercompanyfactdelete/{id}', 'CompanyController@companyFactDelete')->middleware($sellerRole);


Route::get('/sellercompanypress', 'CompanyController@companypress')->middleware($sellerRole)->name('sellercompanypress');
Route::get('/sellercompanypressadd', 'CompanyController@companypressadd')->middleware($sellerRole);
Route::post('/sellercompanypresssave', 'CompanyController@companypresssave')->middleware($sellerRole);
Route::get('/sellercompanypressedit/{id}', 'CompanyController@companypressedit')->middleware($sellerRole);
Route::post('/sellercompanypressupdate', 'CompanyController@companypressupdate')->middleware($sellerRole);
Route::get('/sellercompanypressdelete/{id}', 'CompanyController@companypressdelete')->middleware($sellerRole);


Route::get('/sellercompbo', 'CompanyController@compbo')->middleware($sellerRole)->name('sellercompbo');
Route::get('/sellercompboadd', 'CompanyController@compboadd')->middleware($sellerRole);
Route::post('/sellercompbosave', 'CompanyController@compbosave')->middleware($sellerRole);
Route::get('/sellercompboedit/{id}', 'CompanyController@compboedit')->middleware($sellerRole);
Route::post('/sellercompboupdate', 'CompanyController@compboupdate')->middleware($sellerRole);
Route::get('/sellercompbodelete/{id}', 'CompanyController@compbodelete')->middleware($sellerRole);




Route::get('/sellershared/{id}', 'CompanySharedController@sharedSeller');


// buyer side 
Route::get('/homebuyer', 'BuyerController@index')->name('homebuyer')->middleware($buyerRole);
    // basic settings
    Route::get('/buyerprofile', 'BuyerController@profile')->middleware($buyerRole);
    // email verification
    Route::get('/buyeremailverify', 'UserController@EmailVerification')->middleware($buyerRole);
// email verification
    Route::post('/buyersavedetail', 'BuyerController@savedetail')->middleware($buyerRole);
    Route::get('/buyerupdatedetail', 'BuyerController@updateDetail')->middleware($buyerRole);
    Route::post('/buyersavecredential', 'BuyerController@saveupdatecredential')->middleware($buyerRole);
    Route::get('/buyerupdatecredential', 'BuyerController@updateCredential')->middleware($buyerRole);
    // basic settings

    // buyer company 
    Route::get('/buyercompany', 'InvestController@company')->middleware($buyerRole)->name('buyercompany');
    Route::get('/buyercompanycreate', 'InvestController@companycreate')->middleware($buyerRole);
    Route::post('/buyercompanysave', 'InvestController@companySave')->middleware($buyerRole);
    // buyer company

    // sub module update
    Route::get('/buyercompidentityedit', 'InvestController@compidentityedit')->middleware($buyerRole);
    Route::post('/buyercompidentityupdate', 'InvestController@compidentityupdate')->middleware($buyerRole);

    Route::get('/buyercompslidephotoedit', 'InvestController@compslidephotoedit')->middleware($buyerRole);
    Route::post('/buyercompslidephotoupdate', 'InvestController@compslidephotoupdate')->middleware($buyerRole);

    Route::get('/buyercompmainphotoedit', 'InvestController@compmainphotoedit')->middleware($buyerRole);
    Route::post('/buyercompmainphotoupdate', 'InvestController@compmainphotoupdate')->middleware($buyerRole);

    Route::get('/buyercompsectionedit/{sid}', 'InvestController@compsectionedit')->middleware($buyerRole);
    Route::post('/buyercompsectionupdate', 'InvestController@compsectionupdate')->middleware($buyerRole);
    // sub module update

    // prefered transaction
        
        Route::get('/buyerpreftrans', 'InvestController@preftrans')->middleware($buyerRole)->name('buyerpreftrans');
        Route::get('/buyerpreftransadd', 'InvestController@preftransadd')->middleware($buyerRole);
        Route::post('/buyerpreftranssave', 'InvestController@preftranssave')->middleware($buyerRole);
        Route::get('/buyerpreftransedit/{id}', 'InvestController@preftransedit')->middleware($buyerRole);
        Route::post('/buyerpreftransupdate', 'InvestController@preftransupdate')->middleware($buyerRole);
        Route::get('/buyerpreftransdelete/{id}', 'InvestController@preftransdelete')->middleware($buyerRole);
    // prefered transaction
        
    Route::get('/buyershared/{id}', 'InvestSharedController@sharedBuyer');
// buyer side 


